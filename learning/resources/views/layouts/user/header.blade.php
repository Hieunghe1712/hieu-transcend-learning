<header id="header">
  <div class="logo">
    @if(Auth::check())
    <a href="{{route('user.index')}}"><img src="{!! asset('user/dist/images/logo.png') !!}" alt="Transcend-Learning"></a>
    @else
    <a href="{{route('user.index_login')}}"><img src="{!! asset('user/dist/images/logo.png') !!}" alt="Transcend-Learning"></a>
    @endif
  </div>
  <div style="display: flex; align-items: center;">
    <select style="padding: 0 !important; margin-right: 5px; height: 29px;" name="select" onchange="window.location=this.value">
      <option value="">Japanese</option>
      <option @if ( Config::get('app.locale') == 'vi') selected @endif value="{!! route('change-language', ['vi']) !!}">VietNamese</option>
      <option @if ( Config::get('app.locale') == 'jp') selected @endif value="{!! route('change-language', ['jp']) !!}">Japanese</option>
    </select>
    @if (Auth::check())
    <div class="text-header" style="display:flex;">
      <div class="nav-item pr-2" style="border-right: 1px solid #dee2e6 !important;">
          <a href="{{ route('user.profile.index') }}"><span class="header-name" >{{  Auth::user()->name }}</span></a>
      </div>
      <div class="nav-item pl-2">
        <a href="{{ route('user.logout') }}" class="text-logout" style="color: #fff!important;">ログアウト</a>
      </div>
    </div>
    @else
    <div class="btn">
        <a href="{{ route('user.login') }}">ログイン</a>
    </div>
    @endif
  </div>
</header>
