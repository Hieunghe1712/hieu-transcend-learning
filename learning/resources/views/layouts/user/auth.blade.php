<!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
  <title>@yield('title')</title>
  <link rel="stylesheet" type="text/css" media="all" href="{!! asset('user/dist/css/reset.css') !!}" />
  <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="all" href="{!! asset('user/dist/css/style.css') !!}" />
  <link rel="stylesheet" type="text/css" media="all" href="{!! asset('user/dist/css/brite.common.css') !!}" />
</head>
<body>
<div id="wrapper">
<!--#include virtual="/ssi/header.html" -->
<header id="header">
    <div class="logo">
      <a href="/"><img src="{!! asset('user/dist/images/logo.png') !!}" alt="Transcend-Learning"></a>
    </div>
    <div style="display: flex; align-items: center;">
      <select style="padding: 0 !important; margin-right: 20px; height: 29px; font-family: auto;" name="select" onchange="window.location=this.value">
        {{-- <option value="">Japanese</option> --}}
        <option @if ( Config::get('app.locale') == 'vi') selected @endif value="{!! route('change-language', ['vi']) !!}">VietNamese</option>
        <option @if ( Config::get('app.locale') == 'jp') selected @endif value="{!! route('change-language', ['jp']) !!}">Japanese</option>
      </select>
      <div class="btn">
        <a href="{{ route('user.login') }}">ログイン</a>
      </div>
    </div>
</header>
{{-- content --}}
@yield('content')
<!--#include virtual="/ssi/footer.html" -->
<footer id="footer">
    <div class="section">
      <div class="logo">
        <a href="/"><img src="{!! asset('user/dist/images/logo.png') !!}" alt="Transcend-Learning"></a>
      </div>
      <ul class="menu">
        <li><a href="/about.html">運営団体</a></li>
        <li><a href="/corporation_group.html">法人・団体の方へ</a></li>
        <li><a href="/privacy_policy.html">プライバシーポリシー</a></li>
        <li><a href="/contact.html">お問合せ</a></li>
        <li><a href="{{ route('user.login') }}">ログイン</a></li>
      </ul>
      <span class="copy">&copy;Transcend-Learning</span>
    </div>
</footer>
</div>
<script src="https://kit.fontawesome.com/3fb84fb33c.js" crossorigin="anonymous"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
<script src="{!! asset('user/dist/js/app.js') !!}"></script>
<script src="{!! asset('user/dist/js/common.js') !!}"></script>
</body>
</html>
