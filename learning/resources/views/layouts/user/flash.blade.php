@if (session('success'))
    <div class="custom-alert rounded-0 alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session('success') }}</strong>
    </div>
@elseif(session('error'))
    <div class="custom-alert alert alert-danger alert-dismissible fade show" role="alert">
        <strong>{{ session('error') }}</strong>
    </div>
@endif
