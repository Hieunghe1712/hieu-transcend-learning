<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-admin-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.index') }}" class="brand-link text-admin-primary">
        <span class="brand-text font-weight-light">Transcend-Learningシステム</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-1">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat nav-collapse-hide-child" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('admin.index') }}" class="nav-link ">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li class="nav-item {{ (controllerName() == 'StudentManagerController' || controllerName() == 'AccountManagerController' ? 'menu-open' : '') }} ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>学生アカウント管理<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.student_list') }}" class="nav-link {{ (controllerName() == 'StudentManagerController' && methodName() == 'list') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p>学生一覧</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.create_student') }}" class="nav-link {{ (controllerName() == 'StudentManagerController' && (methodName() == 'createStudent'|| methodName() == 'confirmStudentAccount' || methodName() == 'detail' || methodName() == 'editStudent') || methodName() == 'editConfirmStudent') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p>学生アカウント新規追加</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.account_manager_list') }}" class="nav-link {{ (controllerName() == 'AccountManagerController' && methodName() == 'index') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p>運営アカウント管理</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.createForm') }}" class="nav-link {{ (controllerName() == 'AccountManagerController' && methodName() == 'createForm') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p>運営アカウント新規追加</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.lecture_list') }}" class="nav-link {{ (controllerName() == 'LectureManagerController' && methodName() == 'list') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-search"></i>
                        <p>講義コンテンツ検索</p>
                    </a>
                </li>
                <li class="nav-item {{ (controllerName() == 'LectureManagerController' && methodName() == 'myList' || controllerName() == 'LectureManagerController' && methodName() == 'createForm') ? 'menu-open' : '' }} ">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book-open"></i>
                        <p>マイ講義コンテンツ管理<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.my_lecture_list') }}" class="nav-link {{ (controllerName() == 'LectureManagerController' && methodName() == 'myList') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p class="d-inline-block">マイ講義コンテンツ一覧</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.create_lecture_form') }}" class="nav-link {{ (controllerName() == 'LectureManagerController' && methodName() == 'createForm') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p class="d-inline-block">マイ講義コンテンツ</br>新規追加</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa fa-graduation-cap"></i>
                        <p>シリーズ管理<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon d-inline-block"></i>
                                <p>シリーズ新規追加</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>機関管理<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon d-inline-block"></i>
                                <p>タグ管理</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon d-inline-block"></i>
                                <p>権限管理</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-clock"></i>
                        <p>履歴</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
