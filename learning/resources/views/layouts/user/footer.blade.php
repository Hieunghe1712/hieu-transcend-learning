<footer id="footer">
  <div class="section">
    <div class="logo">
        @if(Auth::check())
            <a href="{{route('user.index')}}"><img src="{!! asset('user/dist/images/logo.png') !!}" alt="Transcend-Learning"></a>
        @else
            <a href="{{route('user.index_login')}}"><img src="{!! asset('user/dist/images/logo.png') !!}" alt="Transcend-Learning"></a>
        @endif
    </div>
    <ul class="menu">
      <li><a href="/about.html">{{__('about')}}</a></li>
      <li><a href="/corporation_group.html">{{__('corporationgroup')}}</a></li>
      <li><a href="/privacy_policy.html">{{__('security')}}</a></li>
      <li><a href="/contact.html">{{__('request')}}</a></li>
      <li><a href="{{ route('user.login') }}">{{__('login')}}</a></li>
    </ul>
    <span class="copy">&copy;Transcend-Learning</span>
  </div>
</footer>
