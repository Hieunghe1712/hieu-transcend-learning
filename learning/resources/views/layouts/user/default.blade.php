<!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
  <title>@yield('title')</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" media="all" href="{!! asset('user/dist/css/reset.css') !!}" />
  <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="all" href="{!! asset('user/dist/css/style.css') !!}" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{!! asset('assets/plugins/fontawesome-free/css/all.min.css') !!}">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{!! asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') !!}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{!! asset('assets/dist/css/adminlte.min.css') !!}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{!! asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') !!}">
  <!-- common css -->
  <link rel="stylesheet" href="{!! asset('assets/dist/css/brite.common.css') !!}">
  <link rel="stylesheet" type="text/css" media="all" href="{!! asset('user/dist/css/brite.common.css') !!}" />
</head>
<style>
  .cmthover,.quiz_modalhover:hover {
    cursor: pointer;
  }
</style>
<body>
  <div id="wrapper">
    @include('layouts.user.header')
    @yield('content')
    @include('layouts.user.footer')
  </div>
  <script type="text/javascript" id="www-widgetapi-script" src="https://s.ytimg.com/yts/jsbin/www-widgetapi-vflS50iB-/www-widgetapi.js" async=""></script>
  <script src="https://www.youtube.com/player_api"></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
  <script src="{!! asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
  <script src="{!! asset('user/dist/js/app.js') !!}"></script>
  <script src="{!! asset('user/dist/js/common.js') !!}"></script>
  <script src="https://player.vimeo.com/api/player.js"></script>
  <script src="https://www.youtube.com/iframe_api"></script>
  <script>
    $(".custom-alert").fadeTo(2000, 500).slideUp(500, function() {
        $(".custom-alert").slideUp(500);
    });
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.js"></script>
  @yield('custom_script_ifrem')
  @yield('custom_script')
</body>
</html>
