<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-admin">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <select class="mr-2" name="select" onchange="window.location=this.value" >
            <option value="">Japanese</option>
            <option @if ( Config::get('app.locale') == 'vi') selected @endif value="{!! route('change-changeLanguageadmin', ['vi']) !!}">VietNamese</option>
            <option @if ( Config::get('app.locale') == 'jp') selected @endif value="{!! route('change-changeLanguageadmin', ['jp']) !!}">Japanese</option>
        </select>
        <!-- user name -->
        <li class="nav-item border-right">
            <span class="d-block text-truncate text-white pr-2">
                @php
                    $userLogin = '';
                    if (Auth::guard('admin')->check()) {
                        $userLogin = Auth::guard('admin')->user()->name;
                    }
                @endphp
                {{ $userLogin }}
            </span>
        </li>
        <li class="nav-item pl-2">
            <a href="{{ route('admin.logout') }}" class="text-white">{{__('logout')}}</a>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
