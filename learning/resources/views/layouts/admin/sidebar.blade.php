<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-admin-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link text-admin-primary">
        <span class="brand-text font-weight-light">{{__('system')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-1">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat nav-collapse-hide-child" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('admin.index') }}" class="nav-link ">
                        <i class="nav-icon fas fa-home"></i>
                        <p>{{__('home')}}</p>
                    </a>
                </li>
                <li class="nav-item {{ (controllerName() == 'StudentManagerController' || controllerName() == 'AccountManagerController' ? 'menu-open' : '') }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>{{__('studentmanagement')}}<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.student_list') }}" class="nav-link {{ (controllerName() == 'StudentManagerController' && methodName() == 'index') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p>{{__('liststudent')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.create_student') }}" class="nav-link {{ (controllerName() == 'StudentManagerController' && (methodName() == 'createStudent'|| methodName() == 'confirmStudentAccount' || methodName() == 'getDataAccountById' || methodName() == 'editStudent') || methodName() == 'editConfirmStudent') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p>{{__('addacstudent')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.account_manager_list')}}" class="nav-link {{ (controllerName() == 'AccountManagerController' && methodName() == 'index') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p>{{__('listacadmin')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.createForm') }}" class="nav-link {{ (controllerName() == 'AccountManagerController' && methodName() == 'createForm') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p>{{__('addacadmin')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.lecture_list') }}" class="nav-link {{ (controllerName() == 'LectureManagerController' && methodName() == 'list') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-search"></i>
                        <p>{{__('searchforlectures')}}</p>
                    </a>
                </li>
                <li class="nav-item {{ (controllerName() == 'LectureManagerController' && (methodName() == 'myList' || methodName() == 'createForm' || methodName() == 'createConfirmLectures' || methodName() == 'editLectureForm' || methodName() == 'editConfirmLecture')) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book-open"></i>
                        <p>{{__('managerlectures')}}<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.my_lecture_list') }}" class="nav-link {{ (controllerName() == 'LectureManagerController' && methodName() == 'myList') ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p class="d-inline-block">{{__('listlecture')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.create_lecture_form') }}" class="nav-link {{ (controllerName() == 'LectureManagerController' && (methodName() == 'createForm' || methodName() == 'createConfirmLectures' || methodName() == 'editLectureForm' || methodName() == 'editConfirmLecture')) ? 'active' : '' }}">
                                <i class="nav-icon d-inline-block"></i>
                                <p class="d-inline-block">{{__('contentlecture')}}</br>{{__('additional')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                {{-- <li class="nav-item d-none">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa fa-graduation-cap"></i>
                        <p>シリーズ管理<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon d-inline-block"></i>
                                <p>シリーズ新規追加</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item d-none">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>機関管理<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon d-inline-block"></i>
                                <p>タグ管理</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon d-inline-block"></i>
                                <p>権限管理</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item d-none">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-clock"></i>
                        <p>履歴</p>
                    </a>
                </li> --}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
