<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title')</title>
  <!-- Google Font: Source Sans Pro -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700&display=swap" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{!! asset('assets/plugins/fontawesome-free/css/all.min.css') !!}">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{!! asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') !!}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{!! asset('assets/dist/css/adminlte.min.css') !!}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{!! asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') !!}">
  <!-- common css -->
  <link rel="stylesheet" href="{!! asset('assets/dist/css/brite.common.css') !!}">
</head>

<body class="hold-transition sidebar-mini layout-fixed text-sm">
  <div class="wrapper">
    <!-- Navbar -->
    @include('layouts.admin.header')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('layouts.admin.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      {{-- @include('layouts.admin.flash') --}}
      @yield('content')
    </div>
    <!-- /.content-wrapper -->
  </div>
  <!-- ./wrapper -->

  <a href="#" id="back-to-top" class="rounded text-center"><i class="fas fa-chevron-up"></i></a>
  <!-- ./back to top -->

  <!-- jQuery -->
  <script src="{!! asset('assets/plugins/jquery/jquery.min.js') !!}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{!! asset('assets/plugins/jquery-ui/jquery-ui.min.js') !!}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 4 -->
  <script src="{!! asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
  <!-- Locales -->
  <script src="{!! asset('assets/plugins/moment/moment-with-locales.min.js') !!}"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="{!! asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') !!}"></script>
  <!-- overlayScrollbars -->
  <script src="{!! asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') !!}"></script>
  <!-- AdminLTE App -->
  <script src="{!! asset('assets/dist/js/adminlte.min.js') !!}"></script>
  <!-- inputmask -->
  {{-- <script src="{!! asset('assets/plugins/inputmask/jquery.inputmask.min.js') !!}"></script>/ --}}
  <!-- common js -->
  <script src="{!! asset('assets/dist/js/brite.common.js') !!}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/jquery.inputmask.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
  <script>
    $(".custom-alert").fadeTo(2000, 500).slideUp(500, function() {
      $(".custom-alert").slideUp(500);
    });
  </script>
  @yield('custom_script')
</body>

</html>
