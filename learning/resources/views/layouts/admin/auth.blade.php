<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
        <title>@yield('title')</title>
        <!-- Google Font: Source Sans Pro -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700&display=swap" rel="stylesheet">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{!! asset('assets/plugins/fontawesome-free/css/all.min.css') !!}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{!! asset('assets/dist/css/adminlte.min.css') !!}">
        <!-- common css -->
        <link rel="stylesheet" href="{!! asset('assets/dist/css/brite.common.css') !!}">
    </head>
    <body class="hold-transition text-sm">
        <div class="bg-admin-primary text-left mb-0 fs-6 p-3">
            <div class="text-admin-primary">
                <b>Transcend-Learning</b>
                <select class="mr-2 float-right" name="select" onchange="window.location=this.value" >
                    <option value="">Japanese</option>
                    <option @if ( Config::get('app.locale') == 'vi') selected @endif value="{!! route('change-changeLanguageadmin', ['vi']) !!}">VietNamese</option>
                    <option @if ( Config::get('app.locale') == 'jp') selected @endif value="{!! route('change-changeLanguageadmin', ['jp']) !!}">Japanese</option>
                </select>
            </div>
        </div>
        @yield('content')

        <!-- jQuery -->
        <script src="{!! asset('assets/plugins/jquery/jquery.min.js') !!}"></script>
        <!-- Bootstrap 4 -->
        <script src="{!! asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
        <!-- AdminLTE App -->
        <script src="{!! asset('assets/dist/js/adminlte.min.js') !!}"></script>
    </body>
</html>
