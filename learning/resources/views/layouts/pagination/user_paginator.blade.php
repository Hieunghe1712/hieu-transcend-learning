<!--user pagination-->
<nav class="page_feed">
  <ul>
    @if ($paginator->hasPages())
        @if (!$paginator->onFirstPage())
            <li><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="fas fa-arrow-left"></i></a></li>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <li><a href="javascript:void(0)">{{ $element }}</a></li>
            @endif

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li><a class="active" href="{{ $url }}">{{ $page }}</a></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="fas fa-arrow-right"></i></a></li>
        @endif
    @endif
  </ul>
</nav>
