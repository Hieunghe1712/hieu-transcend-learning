@extends('layouts.admin.auth')
@section('title', 'パスワードを忘れた方はこちら｜関大ウェブ会議ツール開発')
@section('content')
<div class="login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                <form action="{{ route('admin.postRecoverPassword') }}" method="post">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input type="hidden" name="email" value="{{ $email }}">
                    <div class="mb-3 m-3">
                        <label for="pwd">{{__('newpassword')}}</label>
                        <input id="password" type="password" class="form-control rounded-10px @error('password') is-invalid @enderror" name="password" aria-describedby="passwordFeedback">
                        @error('password')
                            <div id="passwordFeedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-3 m-3">
                        <label for="cfpw">{{__('confirmpassword')}}</label>
                        <input id="password-confirm" type="password" class="form-control rounded-10px" name="password_confirm" aria-describedby="passwordConfirmFeedback">
                        @error('password_confirm')
                            <div id="passwordConfirmFeedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="row mt-4 mb-5">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-admin-primary w-50">{{__('update')}}</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <a href="{{ route('admin.login') }}"><p class="text-center mt-3 mb-5"><b>{{__('returnlogin')}}</b> </p></a>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
</div>
@endsection
