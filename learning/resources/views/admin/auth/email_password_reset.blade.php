<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forgot Password</title>
</head>
<body>
    <div style="width: 670px; background-color: #f2f3f8; boder: 1px solid #ccc; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);">
        <h1 style="color:#1e1e2d; text-align: center; font-weight:500; font-size:32px;font-family:Rubik,sans-serif;">
            You have requested to reset your password
        </h1>
        <p style="color:#455056; font-size:15px; line-height:24px; margin-left:10px;">
            We cannot simply send you your old password. A unique link to reset your password has been generated for you. To reset your password, click the following link and follow the instructions
        </p>
        <center>
            <a href="{{ route('password.reset', [ 'token' => $token ]) }}?email={{ $admin->email }}" style="line-height:24px;background:#20e277; text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff; text-transform:uppercase; font-size:14px; padding:10px 24px; display:inline-block; border-radius:50px; margin-bottom:35px;">
                Reset Password
            </a>
        </center>
        <p style="color:#455056; font-size:15px; line-height:24px; margin-left:10px;">
            If you did not initiate this request, please contact us immediately at abc@gmail.com
        </p>
        <ol style="color:#455056; font-size:15px; line-height:24px; margin:0;">
            <li>Trancend_learning Team</li>
            <li>abc@gmail.com</li>
            <li>1-3-3 Uchikanda, Chiyoda</li>
            <li>Tokyo, 101-0047, Japan</li>
            <li>Sent to: {{ $admin->email }}</li>
        </ol>
        <p style="color:#455056; font-size:15px; line-height:24px; margin-bottom:35px; margin-left:10px;">
            If you would not like to receive any solicitation from Trancend Learning, contact abc@gmail.com to let us unsubscribe you.
        </p>
    </div>
</body>
</html>
