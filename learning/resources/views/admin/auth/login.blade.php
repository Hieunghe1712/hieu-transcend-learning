@extends('layouts.admin.auth')
@section('title', 'ログイン｜関大ウェブ会議ツール開発')
@section('content')
<div class="login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                @if (Session::has('success'))
                    <div class="alert alert-success custom-alert">
                        {!! Session::get('success') !!}
                    </div>
                @endif
                <form action="{{ route('admin.login') }}" method="post">
                    @csrf
                    <div class="mb-3 mt-5 m-3">
                        <label for="uname">{{__('emailaddress')}}</label>
                        <input type="text" name="email" id="name" class="form-control email @error('email') is-invalid @enderror" value="{{ old('email') }}" aria-describedby="emailFeedback">
                        @error('email')
                            <div id="emailFeedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-3 m-3">
                        <label for="psw">{{__('password')}}</label>
                        <input id="password" type="password" class="form-control pwd @error('password') is-invalid @enderror" name="password" aria-describedby="passwordFeedback">
                        @error('password')
                            <div id="passwordFeedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-admin-primary w-50">{{__('login')}}</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <a href="{{ route('admin.forgotPassword') }}"><p class="text-center mt-3 mb-5 forgot-text">{{__('forgetpass')}}</p></a>
                </form>
            </div>
        <!-- /.login-card-body -->
        </div>
    </div>
<!-- /.login-box -->
</div>
@endsection
