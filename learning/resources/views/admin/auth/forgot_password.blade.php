@extends('layouts.admin.auth')
@section('title', 'パスワードを忘れた方はこちら｜関大ウェブ会議ツール開発')
@section('content')
<div class="login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{ route('admin.postForgotPassword') }}" method="post">
                    @csrf
                    <div class="mb-3 mt-5 m-3">
                        <label for="uname">{{__('emailaddress')}}</label>
                        <input id="email" type="text" class="form-control rounded-10px @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" aria-describedby="emailFeedback">
                        @error('email')
                            <div id="emailFeedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="row mt-4 mb-5">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-admin-primary w-50">{{__('submit')}}</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
</div>
@endsection
