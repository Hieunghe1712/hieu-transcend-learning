 {{-- Quiz Script --}}
 <script>
     $(function() {
        function setInputMask() {
            $(".form-create-edit-lecture").find(".input-mask-time").inputmask("99:99:99", {
                placeholder: "HH:MM:SS",
                insertMode: false,
                showMaskOnHover: false,
            });
        }
        var numOfQuiz = $(".number_of_quiz").val();
        $("#add-quiz").on("click", function() {
            var formData = new FormData()
            formData.append('num', numOfQuiz);
            $.ajax({
            type: "POST",
            url: "{{ route('admin.lectures.getFormAddQuiz') }}",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'html',
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            success: function(res) {
                if (res) {
                    let data = JSON.parse(res)
                    if (data.html.length == 0) {
                        return
                    } else {
                        $("#all-quizz").append(data.html)
                        setInputMask();
                        numOfQuiz++;
                        $(".number_of_quiz").val(numOfQuiz)
                    }
                }
            },
            error: function(res) {
                alert('Error!')
            }
            })
        })

        // ==================== Quiz script ====================
        //  Quiz options
        $("#all-quizz").on('change', 'input[type=radio].quiz-option-id', function() {
            checkDisplayQuizValues(this)
        });

        function checkDisplayQuizValues(e) {
            var haveChoice = $(e).attr("data-choice");
            if (haveChoice == "{{ QUIZ_HAVE_CHOICE_0 }}") {
                $(e).closest(".quiz-choices").next('.quiz-values').hide()
            } else {
                $(e).closest(".quiz-choices").next('.quiz-values').show()
            }
        }

        //Quiz values
        $("#all-quizz").on("click", ".add-choice", function() { //add choice
            var currentNumChoice = $(this).closest(".quiz-values").find(".quiz-value").length
            var nextNum = currentNumChoice + 1;
            var indexNum = $(this).closest(".form-quiz").find(".quiz-index").val()
            var html = '<div class="row mb-2 choice-value">'
            html += '<div class="col col-sm-4">'
            html += '<input type="text" class="form-control text-right quiz-value name_quiz_value-' +
                indexNum +
                '-' +
                nextNum + '" data-class="name_quiz_value-' + indexNum + '-' + nextNum +
                '" data-index-value="' +
                nextNum +
                '" name="name_quiz_value-' + indexNum + '-' + nextNum + '">'
            html += '<div id="name_quiz_value-' + indexNum + '-' + nextNum +
                '-error" class="invalid-feedback"></div>'
            html += '</div>'
            html += '<div class="col-auto text-center">'
            html += '<button type="button" class="btn btn-danger remove-choice">{{__('delete')}}</button>'
            html += '</div>'
            html += '</div>'

            $(this).closest(".quiz-values").find(".all-choice").append(html)
        })
        $("#all-quizz").on("click", ".remove-choice", function() { //remove choice
            var currentClass = $(this).closest(".choice-value").find(".quiz-value").attr("data-class")
            var noOfInput = 1;
            $(this).closest(".quiz-values").find(".quiz-value").each(function(index, e) {
                var indexNum = $(e).closest(".form-quiz").find(".quiz-index").val()
                var dataClass = $(e).attr("data-class")
                var currentName = $(e).attr("name")
                if (currentClass != dataClass) {
                    var newClass = "quiz_value-" + indexNum + "-" + noOfInput
                    var newName = "name_quiz_value-" + indexNum + "-" + noOfInput

                    $(e).removeClass(dataClass)
                    $(e).attr("data-class", newClass)
                    $(e).attr("data-index-value", noOfInput)
                    $(e).attr("name", newName)
                    $(e).next("#" + currentName + "-error").attr("id", newName)
                    $(e).addClass(newClass)
                    noOfInput++;
                }
            })
            $(this).closest(".choice-value").remove()
        })
        // ======================================================

    });
 </script>

 {{-- Explanation Script --}}
 <script>
    $(function() {
        var numOfExplanation = $(".number_of_explanation").val()
        $("#add-explanation").on("click", function() {
            var formData = new FormData()
            formData.append('num', numOfExplanation);
            $.ajax({
                type: "POST",
                url: "{{ route('admin.lectures.getFormExplanation') }}",
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'html',
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(res) {
                    if (res) {
                        console.log(res);
                        let data = JSON.parse(res)

                        if (data.html.length == 0) {
                            return
                        } else {
                            $("#all-explanation").append(data.html)
                            numOfExplanation++;
                            $(".number_of_explanation").val(numOfExplanation)
                        }
                    }
                },
                error: function(res) {
                    alert('Error!')
                }
            })
        })

        $("#all-explanation").on('click', '.upload-file', function(e) {
            $(this).next(".explanation-file").click();
        });
        $("#all-explanation").on('change', '.explanation-file', function(e) {
            uploadExplanationFile(e.target.files, this);
        });

        function uploadExplanationFile(files, element) {
            var formData = new FormData();
            formData.append('file', files[0]);
            var urlUp = "{{ Route('admin.lectures.uploadExplanationFile') }}";
            $.ajax({
                type: 'POST',
                url: urlUp,
                data: formData,
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(response) {
                    if (response.success == '{{ CODE_AJAX_SUCCESS }}') {
                        $(element).parent(".upload-file-explanation").find(".file-name").html(response.name)
                        $(element).parent(".upload-file-explanation").find(".file-name-upload").val(response.name)
                        $(element).parent(".upload-file-explanation").find(".dir-file-upload").val(response.dirFile)
                        $(element).parent(".upload-file-explanation").find(".dir-file-name-upload").val(response.fileName)
                    } else {
                        alert(response.msg);
                        $('.explanation-file').val('');
                    }

                },
                error: function(error) {
                    alert("error");
                    $('.explanation-file').val('');
                }
            });
        }
    });
 </script>

 {{-- Headings Script --}}
 <script>
     $(function() {
        function setInputMask() {
        $(".form-create-edit-lecture").find(".input-mask-time").inputmask("99:99:99", {
            placeholder: "HH:MM:SS",
            insertMode: false,
            showMaskOnHover: false,
        });
        }
        var numOfHeadings = $(".number_of_headings").val();
        $("#add-headings").on("click", function() {
            var formData = new FormData()
            formData.append('num', numOfHeadings);
            $.ajax({
                type: "POST",
                url: "{{ route('admin.lectures.getFormHeadings') }}",
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'html',
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(res) {
                    if (res) {
                        let data = JSON.parse(res)
                        if (data.html.length == 0) {
                            return
                        } else {
                            $("#all-headings").append(data.html)
                            setInputMask();
                            numOfHeadings++;
                            $(".number_of_headings").val(numOfHeadings)
                        }
                    }
                },
                error: function(res) {
                    alert('Error!')
                }
            })
        })
    });
 </script>

 {{-- Documents Script --}}
 <script>
    $(function() {
        var numOfDocuments = $(".number_of_documents").val()
        $("#add-documents").on("click", function() {
            var formData = new FormData()
            formData.append('num', numOfDocuments);
            $.ajax({
                type: "POST",
                url: "{{ route('admin.lectures.getFormDocuments') }}",
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'html',
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(res) {
                    if (res) {
                        let data = JSON.parse(res)
                        console.log(data);
                        if (data.html.length == 0) {
                            return
                        } else {
                            if ($(".memo-documents").is(":hidden")) {
                                $(".memo-documents").show()
                            }
                            $("#all-documents").append(data.html)
                            numOfDocuments++;
                            $(".number_of_documents").val(numOfDocuments)
                        }
                    }
                },
                error: function(res) {
                    alert('Error!')
                }
            })
        })

        //upload file
        $("#all-documents").on('click', '.documents-upload-file', function(e) {
            $(this).next(".documents-file").click();
        });
        $("#all-documents").on('change', '.documents-file', function(e) {
            uploadDocumentFile(e.target.files, this);
        });

        function uploadDocumentFile(files, element) {
            var formData = new FormData();
            formData.append('file', files[0]);
            var urlUp = "{{ Route('admin.lectures.uploadDocumentsFile') }}";
            $.ajax({
                type: 'POST',
                url: urlUp,
                data: formData,
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                success: function(response) {
                    if (response.success == '{{ CODE_AJAX_SUCCESS }}') {
                        $(element).parent(".upload-file-documents").find(".file-name").html(response.name)
                        $(element).parent(".upload-file-documents").find(".file-name-upload").val(response.name)
                        $(element).parent(".upload-file-documents").find(".dir-file-upload").val(response.dirFile)
                        $(element).parent(".upload-file-documents").find(".dir-file-name-upload").val(response.fileName)
                        $(element).parent(".upload-file-documents").find(".file_created_at").val(response.createdAt)
                    } else {
                        alert(response.msg);
                        $('.documents-file').val('');
                    }
                },
                error: function(error) {
                    alert("error");
                    $('.documents-file').val('');
                }
            });
        }
    });
 </script>

 {{-- Create lecture script --}}
 <script>
    $(function() {
        $(".form-create-edit-lecture").find(".input-mask-time").inputmask("99:99:99", {
            placeholder: "HH:MM:SS",
            insertMode: false,
            showMaskOnHover: false,
        });
        var errorEmbedCode = false;
        var durationEmbed = 0;
        $("#embed-code").on("input", function() {
            checkEmbedCode();
        })

        function checkIfVideoVimeoExists(videoID, callback) {
            $.ajax({
                type: 'GET',
                url: 'https://vimeo.com/api/oembed.json?url=' + encodeURIComponent(videoID),
                dataType: 'json',
                complete: function(xhr) {
                    callback(xhr);
                }
            });
        }

        function checkIfVideoYoutubeExists(videoID, callback) {
            $.getJSON('https://www.googleapis.com/youtube/v3/videos?id=' + videoID + "&key=AIzaSyDnhPVbIYjfTjDEtM7BCLF5lCFMF45rEBs&part=snippet", 
            function (data, status, xhr) { 
                callback(data, status, xhr);       
            })
        }

        checkEmbedCode()
        
        var radiourl = $("#Radiovimeo").val();

        $("#Radiovimeo").click(function(){
            radiourl = $("#Radiovimeo").val();
        });

        $("#Radioyt").click(function(){
            radiourl = $("#Radioyt").val();
        });

        function checkEmbedCode() {
            $("#video-iframe").html("");
            $("#embed_code-error").text("");
            $("#thumbnail_url").val("");
            $("#embed-code").removeClass('is-invalid');
            durationEmbed = 0
            var erMsg = "埋め込みコード" + "{{ Config::get('message.validation_common_msg.format_err') }}"
            var embedCode = $("#embed-code").val();
            if (embedCode != "") {
                $("#video-iframe").append(embedCode)
                setTimeout(() => {
                    var iframe = $("#video-iframe").find("iframe");
                    if (iframe.length > 0) {
                        var src = iframe.attr("src");
                        
                        if(radiourl == '0') {
                            checkIfVideoVimeoExists(src, function(xhr) {
                                if (xhr.status != 200) {
                                    $("#embed-code").addClass('is-invalid');
                                    $("#embed_code-error").text(erMsg);
                                    errorEmbedCode = true;
                                } else {
                                    console.log('ok');
                                    $("#embed_code-error").text("");
                                    $("#embed-code").removeClass('is-invalid');
                                    errorEmbedCode = false;
                                    var response = xhr.responseJSON
                                    durationEmbed = response.duration
                                    console.log(response);
                                    $("#video-iframe").html("");
                                    $("#thumbnail_url").val("");
                                    $("#video-iframe").append(response.html);
                                    $("#embed-code").val(response.html);
                                    $("#thumbnail_url").val(response.thumbnail_url_with_play_button);
                                }
                            });
                        }else {
                            url = src.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);

                            checkIfVideoYoutubeExists((url[2] !== undefined) ? url[2].split(/[^0-9a-z_\-]/i)[0] : url[0], function(data, status, xhr) {
                                if (data.items.length <= 0) {
                                    $("#embed-code").addClass('is-invalid');
                                    $("#embed_code-error").text(erMsg);
                                    errorEmbedCode = true;
                                } else {
                                    $("#embed_code-error").text("");
                                    $("#embed-code").removeClass('is-invalid');
                                    errorEmbedCode = false;
                                    var response = $("#embed-code").val();
                                    $("#video-iframe").html("");
                                    $("#thumbnail_url").val("");
                                    $("#video-iframe").append(response);
                                    $("#embed-code").val(response);
                                    $("#thumbnail_url").val(data.items[0].snippet.thumbnails.default.url);
                                }
                            });
                        }
                    } else {
                        $("#embed-code").addClass('is-invalid')
                        $("#embed_code-error").text(erMsg);
                        errorEmbedCode = true;
                    }
                }, 200);

            }
        }
        $("#submit-form").on("click", function() {
            $(this).attr('disabled', 'disabled');
            var formData = getFormData();
            var url = "";
            var urlRedirect = "";
            if ($("#id_lecture").val()) {
                formData.append('id', $("#id_lecture").val());
                url = "{{ route('admin.lectures.editLecture', '') }}" + "/" + $("#id_lecture").val()
                urlRedirect = "{{ route('admin.lectures.editConfirmLecture', '') }}" + "/" + $(
                    "#id_lecture").val()
            } else {
                url = "{{ route('admin.lectures.createLectures') }}"
                urlRedirect = "{{ route('admin.lectures.createConfirmLectures', '') }}"
            }

            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                dataType: 'json',
                success: function(response) {
                    $("#confirm-key").val(response.key)
                    $("input").removeClass('is-invalid')
                    $("textarea").removeClass('is-invalid')
                    $(".invalid-feedback").text("")
                    checkEmbedCode();
                    if (response.result == false) {
                        $("#submit-form").attr('disabled', false)

                        displayErrorMessage(response.errMes)
                    } else {
                        if (errorEmbedCode == false) {
                            var redirect = urlRedirect + "/" + response.key
                            window.location.replace(redirect);
                        } else {
                            $("#submit-form").attr('disabled', false);
                            $('html, body').animate({
                                scrollTop: $("#embed-code").offset().top - 50
                            }, 200);
                        }

                    }
                },
                error: function(response) {
                    alert('error')
                }
            })
        })

        function displayErrorMessage(errMes) {

            $("input").removeClass('is-invalid')
            $(".invalid-feedback").text("")
            $.each(errMes, function(fieldName, mess) {
                var errElem = "#" + fieldName + "-error"
                $(errElem).text(mess)
                $("input[name='" + fieldName + "']").addClass('is-invalid')
                $("textarea[name='" + fieldName + "']").addClass('is-invalid')
                $("." + fieldName).addClass('is-invalid')
            })

            var firstFieldErr = Object.keys(errMes)[0]
            $('html, body').animate({
                scrollTop: $("[name='" + firstFieldErr + "']").offset().top - 50
            }, 200);
        }

        function getFormData() {
            var formData = new FormData();
            formData.append('name', $("#name").val());
            formData.append('question_url', $("#question_url").val());
            formData.append('test_url', $("#test_url").val());
            formData.append('homework_url', $("#homework_url").val());
            formData.append('thumbnail_url', $("#thumbnail_url").val());
            formData.append('embed_code', $("#embed-code").val());
            formData.append('ifrem_code', radiourl);
            formData.append('key', $("#confirm-key").val());
            formData.append('explanatory_text', $("#explanatory-text").val());
            formData.append('duration_embed', durationEmbed);


            var quizzes = JSON.stringify(getQuizData());
            formData.append('quizzes', quizzes);

            var explanations = JSON.stringify(getExplanationData())
            formData.append('explanations', explanations);

            var headings = JSON.stringify(getHeadingsData())
            formData.append('headings', headings);

            var documents = JSON.stringify(getDocumentsData())
            formData.append('documents', documents);

            return formData;
        }

        function getQuizData() {
            var quizzes = [];
            $("#all-quizz").find(".form-quiz").each(function() {
                var indexQuiz = $(this).find(".quiz-index").val()
                var title = $(this).find(".quiz-title").val()
                var questionSentence = $(this).find(".question-sentence").val()
                var quizOptionId = $(this).find(".quiz-option-id:checked").val()
                var quizRemark = $(this).find(".quiz-remark").val()
                var quizAppearanceTime = $(this).find(".quiz-appearance-time").val()
                if (typeof quizOptionId == "undefined") {
                    quizOptionId = "";
                }

                var choices = [];
                $(this).find(".quiz-value").each(function(index, e) {
                    var choice = {
                        "name": $(e).val(),
                        "index_value": $(e).attr("data-index-value"),
                        "index_quiz": indexQuiz
                    }
                    choices.push(choice)
                })
                var data = {
                    'index_quiz': indexQuiz,
                    'title': title,
                    'question_sentence': questionSentence,
                    'quiz_option_id': quizOptionId,
                    'remark': quizRemark,
                    'appearance_time': quizAppearanceTime,
                    'choices': choices
                }
                quizzes.push(data)
            })
            return quizzes;
        }

        function getExplanationData() {
            var explanations = [];
            $("#all-explanation").find(".form-explanation").each(function() {
                var indexExplanation = $(this).find(".explanation-index").val()
                var title = $(this).find(".explanation-title").val()
                var content = $(this).find(".explanation-content").val()
                var fileName = $(this).find(".dir-file-name-upload").val()
                var originalFileName = $(this).find(".file-name-upload").val()
                var dirFile = $(this).find(".dir-file-upload").val()
                var data = {
                    "index_explanation": indexExplanation,
                    'title': title,
                    'content': content,
                    'file_name': fileName,
                    'original_file_name': originalFileName,
                    'dir_file': dirFile,
                }
                explanations.push(data)
            })
            return explanations
        }

        function getHeadingsData() {
            var headings = [];
            $("#all-headings").find(".form-headings").each(function() {
                var indexHeadings = $(this).find(".heading-index").val();
                var title = $(this).find(".heading-title").val();
                var time = $(this).find(".heading-time").val();
                var data = {
                    "index_headings": indexHeadings,
                    'title': title,
                    'time': time
                }
                headings.push(data)
            })
            return headings
        }

        $(document).ready(function() {
            getHeadingsData();
        });

        function getDocumentsData() {
            var documents = [];
            $("#all-documents").find(".form-documents").each(function() {
                var indexDocuments = $(this).find(".documents-index").val()
                var title = $(this).find(".documents-title").val()
                var fileName = $(this).find(".dir-file-name-upload").val()
                var originalFileName = $(this).find(".file-name-upload").val()
                var dirFile = $(this).find(".dir-file-upload").val()
                var createdAt = $(this).find(".file_created_at").val()
                var data = {
                    "index_documents": indexDocuments,
                    'title': title,
                    'file_name': fileName,
                    'original_file_name': originalFileName,
                    'dir_file': dirFile,
                    'created_at': createdAt,
                }
                documents.push(data)
            })
            return documents
        }
    })
 </script>
