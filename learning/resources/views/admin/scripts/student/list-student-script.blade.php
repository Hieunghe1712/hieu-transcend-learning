<script>
    $(function () {
        $(".pagination a").on("click", function(e) {
            e.preventDefault();
            let url = $(this).attr('href');
            if (paramUrl = getParameterUrlSearch()) {
                url += paramUrl;
            }
            window.location.href = url;
        });
    });

    function getParameterUrlSearch() {
        let urlSearch = '';
        $.urlParam = function(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
            if (results == null){
               return null;
            }
            else {
               return decodeURI(results[1]) || 0;
            }
        };
        if (searchName = $.urlParam('name')) {
            urlSearch += '&name=' + encodeURIComponent(searchName);
        }
        return urlSearch;
    }
</script>
