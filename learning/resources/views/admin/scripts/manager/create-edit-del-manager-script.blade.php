<script>
    $(document).ready(function(){
        $('.randpass').click(function(){
            var randomstring = Math.random().toString(36).slice(-8);
            console.log(randomstring);
            $('.ippass').val(randomstring);
        });

        $('.upload-avatar-manager').click(function(){
            $('#profile-img').click();
        });

        $('#profile-img').change(function(e) {
            let formData = new FormData();
            let file = e.target.files[0];
            formData.append('file', file);
            $.ajax({
                url: '{{ route('admin.manager.uploadAvatar') }}',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',   
                contentType: false,
                processData: false,   
                cache: false,        
                data: formData,
                success: function(response) {
                    console.log('ok');
                    var data = response.data;
                    if (data) {
                        $('span.file-name').text(data);
                        $('#avatar-account').val(data);
                        var src = "{{ asset('storage/images') }}" + '/' + data;
                        $("#avatar-manager").attr("src", src);
                    }
                },
                error: function(response) {
                    console.log('error');
                }
            });
        });
        
        $('.btnDeleteAccount').on('click', function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var url = '{{ Route("admin.deleteAccount", "") }}' + '/' + id;
            var confirmStr = "アカウント" + "{{ Config::get('message.common_msg.ask_before_delete') }}";
            if (confirm(confirmStr)) {
                window.location = url;
            }
            return false;
        });
    });
</script>