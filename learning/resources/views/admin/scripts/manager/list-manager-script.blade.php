<script>
    $(function() {
      $(".pagination a").on("click", function(e) {
        e.preventDefault();
        let url = $(this).attr('href');
        url = getParameterUrlSearch(url)
        window.location.href = url;
      });
  
      $(".search-manager").on("click", function(e) {
        let url = $(this).attr('data-href');
        var searchParam = new URLSearchParams(window.location.search);
        var page = 1;
        url += '?page=' + page;
        url = getParameterUrlSearch(url)
        window.location.href = url;
      });
  
      $('#form-search-manager').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
          e.preventDefault();
          let url = $(".search-manager").attr('data-href');
          var searchParam = new URLSearchParams(window.location.search);
          var page = 1;
          url += '?page=' + page;
          url = getParameterUrlSearch(url)
          window.location.href = url;
          return false;
        }
      });
  
  
      function getParameterUrlSearch(urlSearch) {
        var urlPermissions = new URLSearchParams(window.location.search);
        var name = '';
        urlSearch += '&name=' + $('.manager-name').val();
        var permissions = [];
        $('.permission').each(function() {
          if ($(this).is(':checked')) {
            permissions.push($(this).val());
          }
        });
        urlSearch += '&permission=' + JSON.stringify(permissions);
        return urlSearch;
      }
    });
  
  </script>