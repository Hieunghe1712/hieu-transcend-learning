@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 学生アカウント確認')
@section('content')
<!-- Main content -->
<section class="content pt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card rounded-0">
                    <div class="card-header rounded-0 border-bottom-0">
                        <h3 class="card-title font-weight-normal">{{__('accountverification')}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pt-1">
                        <form action="{{ route('admin.saveCreateStudent') }}" method="post">
                            @csrf
                            <input type="hidden" name="key" value="{{ $key }}">
                            <div class="form-group">
                                <h6>{{__('image')}}</h6>
                                <div class="upload-image-preview h-auto bg-transparent col-md-3">
                                    @php
                                    $profile_img = 'storage/images/noimage.jpg';
                                    if (isset($data['profile_img']) && $data['profile_img'] != '') {
                                        if (@file_exists(AVATAR_STUDENT_SAVE . $data['profile_img'])) {
                                            $profile_img = AVATAR_STUDENT_SAVE_JS . $data['profile_img'];
                                        } elseif (@file_exists(AVATAR_STUDENT_SAVE_TMP . $data['profile_img'])) {
                                            $profile_img = AVATAR_STUDENT_SAVE_TMP_JS . $data['profile_img'];
                                        }
                                    }
                                    @endphp
                                    <img src="{{ isset($data['profile_img']) ? asset('storage/images/' . $data['profile_img']) : url($profile_img)  }}" alt="Avatar student" class="avatar" width="100%" style="height: 65%;" id="avatar-manager">
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-5 col-xl-3">
                                <div class="form-group">
                                    <h6>{{__('name')}}</h6>
                                    <input type="text" class="form-control" placeholder="{{__('content')}}" name="name" value="{{ $data['name'] }}" disabled>
                                </div>
                                <div class="form-group">
                                    <h6>{{__('emailaddress')}}</h6>
                                    <input type="text" class="form-control" placeholder="example@example.com" name="email" value="{{ $data['email'] }}" disabled>
                                </div>
                                <div class="form-group">
                                    <h6>{{__('nameunv')}}</h6>
                                    <input type="text" class="form-control" placeholder="{{__('content')}}" name="university" value="{{ $data['university'] }}" disabled>
                                </div>
                                <div class="form-group">
                                    <h6>{{__('majors')}}</h6>
                                    <input type="text" class="form-control" placeholder="{{__('content')}}" name="faculty" value="{{ $data['faculty'] }}" disabled>
                                </div>
                                <div class="form-group">
                                    <h6>{{__('yearAdmission')}}</h6>
                                    <input type="text" class="form-control" placeholder="{{__('content')}}" name="school_year" value="{{ $data['school_year'] }}" disabled>
                                </div>
	                            <div class="form-group">
	                                <h6>{{__('password')}}</h6>
                                    <input type="password" class="form-control" placeholder="{{__('content')}}" name="password" value="{{ $data['password'] }}" disabled>
	                            </div>
                            </div>
                            <div class="row mt-3 ml-1">
                                <button type="submit" class="btn btn-admin-primary ml-2 w-100px">{{__('submit')}}</button>
                                <a class="btn btn-secondary ml-3 w-100px" href="{{ route('admin.create_student', ['key' => $key]) }}">{{__('return')}}</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- ./col -->
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
