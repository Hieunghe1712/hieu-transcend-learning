@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 学生アカウント編集')
@section('content')
    <!-- Main content -->
    <section class="content pt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card rounded-0">
                        <div class="card-header rounded-0 border-bottom-0">
                            <h3 class="card-title font-weight-normal">{{__('editacstudent')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        @php
                            if (!empty(Session::get('student'))) {
                                $student = Session::get('student');
                            }
                        @endphp
                        <div class="card-body pt-1 pb-5">
                            <form action="{{ route('admin.postEditStudent', ['id' => $student['id']] ) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $student['id'] }}">
                                <div class="form-group">
                                    <h6>{{__('image')}}</h6>
                                    <div class="upload-image-preview upload-area col-md-3 mb-3 px-0">
                                        @php
                                            $profile_img = 'storage/images/noimage.jpg';
                                            if (isset($student['profile_img']) && $student['profile_img'] != '') {
                                                if (@file_exists(AVATAR_STUDENT_SAVE . $student['profile_img'])) {
                                                    $profile_img = AVATAR_STUDENT_SAVE_JS . $student['profile_img'];
                                                } elseif (@file_exists(AVATAR_STUDENT_SAVE_TMP . $student['profile_img'])) {
                                                    $profile_img = AVATAR_STUDENT_SAVE_TMP_JS . $student['profile_img'];
                                                }
                                            }
                                        @endphp
                                        <img src="{{ isset($student['profile_img']) ? asset('storage/images/' . $student['profile_img']) : url($profile_img)  }}" alt="Avatar student" class="avatar" width="100%" style="height: 65%;" id="avatar-student">
                                        <input type="hidden" name="profile_img" id="student-image" value="{{ isset($student['profile_img']) ? $student['profile_img'] : '' }}">
                                    </div>
                                    <button type="button" class="btn btn-default px-2 upload-avatar-student">{{__('uploadfile')}}</button>
                                    <input type="file" class="form-control-file d-none" id="profile-img" value="{{ isset($student['profile_img']) ? $student['profile_img'] : '' }}">
                                    <span class="ml-2 file-name">{{__('noimage')}}</span>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-5 col-xl-3">
                                        <div class="form-group">
                                            <h6>{{__('name')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="{{__('content')}}" name="name" value="{{ isset($student['name']) ? $student['name'] : old('name') }}" aria-describedby="nameFeedback">
                                            @error('name')
                                                <div id="nameFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <h6>{{__('emailaddress')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="example@example.com" name="email" value="{{ isset($student['email']) ? $student['email'] : old('email') }}" aria-describedby="emailFeedback">
                                            @error('email')
                                                <div id="emailFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <h6>{{__('nameunv')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('university') ? 'is-invalid' : '' }}" placeholder="{{__('content')}}" name="university" value="{{ isset($student['university']) ? $student['university'] : old('university') }}" aria-describedby="universityFeedback">
                                            @error('university')
                                                <div id="universityFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <h6>{{__('majors')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('faculty') ? 'is-invalid' : '' }}" placeholder="{{__('content')}}" name="faculty" value="{{ isset($student['faculty']) ? $student['faculty'] : old('faculty') }}" aria-describedby="facultyFeedback">
                                            @error('faculty')
                                                <div id="facultyFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <h6>{{__('yearAdmission')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('school_year') ? 'is-invalid' : '' }}" placeholder="{{__('content')}}" name="school_year" value="{{ isset($student['school_year']) ? $student['school_year'] : old('school_year') }}" aria-describedby="schoolYearFeedback">
                                            @error('school_year')
                                                <div id="schoolYearFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-lg-6 col-xl-8 col-md-6 col-sm-10 col-12">
                                        <button type="submit" class="btn btn-admin-primary w-100px">{{__('update')}}</button>
                                        <button type="button" class="btn btn-danger ml-3 w-100px btn-delete-student" data-id="{{ $student['id'] }}">{{__('delete')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('custom_script')
    @include('admin.scripts.student.create-edit-del-student-script')
@endsection
