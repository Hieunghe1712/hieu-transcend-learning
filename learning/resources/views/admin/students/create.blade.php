@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 学生アカウント新規追加')
@section('content')
    <!-- Main content -->
    <section class="content pt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card rounded-0">
                        <div class="card-header rounded-0 border-bottom-0">
                            <h3 class="card-title font-weight-normal">{{__('addstudentac')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        @php
                            if (!empty(Session::get('data'))) {
                                $data = Session::get('data');
                            }
                        @endphp
                        <div class="card-body pt-1 pb-5">
                            <form action="{{ route('admin.postCreateStudent') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <h6>{{__('image')}}</h6>
                                    <div class="upload-image-preview upload-area col-md-3 mb-3 px-0">
                                        @php
                                            $profile_img = 'storage/images/noimage.jpg';
                                            if (isset($data['profile_img']) && $data['profile_img'] != '') {
                                                if (@file_exists(AVATAR_STUDENT_SAVE . $data['profile_img'])) {
                                                    $profile_img = AVATAR_STUDENT_SAVE_JS . $data['profile_img'];
                                                } elseif (@file_exists(AVATAR_STUDENT_SAVE_TMP . $data['profile_img'])) {
                                                    $profile_img = AVATAR_STUDENT_SAVE_TMP_JS . $data['profile_img'];
                                                }
                                            }
                                        @endphp
                                        <img src="{{ url($profile_img) }}" alt="Avatar student" class="avatar" width="100%" style="height: 65%;" id="avatar-student">
                                        <input type="hidden" name="profile_img" id="student-image" value="{{ isset($data['profile_img']) ? $data['profile_img'] : '' }}">
                                    </div>
                                    <button type="button" class="btn btn-default px-2 upload-avatar-student">{{__('uploadfile')}}</button>
                                    <input type="file" class="form-control-file d-none" id="profile-img" value="{{ isset($data['profile_img']) ? $data['profile_img'] : '' }}">
                                    <span class="ml-2 file-name">{{__('noimage')}}</span>
                                    <div id="imgerr" class="invalid-feedback" style="display: none"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-5 col-xl-3">
                                        <div class="form-group">
                                            <h6>{{__('name')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="{{__('content')}}" name="name" value="{{ isset($data['name']) ? $data['name'] : old('name') }}" aria-describedby="nameFeedback">
                                            @error('name')
                                                <div id="nameFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <h6>{{__('emailaddress')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="example@example.com" name="email" value="{{ isset($data['email']) ? $data['email'] : old('email') }}" aria-describedby="emailFeedback">
                                            @error('email')
                                                <div id="emailFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <h6>{{__('nameunv')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('university') ? 'is-invalid' : '' }}" placeholder="{{__('content')}}" name="university" value="{{ isset($data['university']) ? $data['university'] : old('university') }}" aria-describedby="universityFeedback">
                                            @error('university')
                                                <div id="universityFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <h6>{{__('majors')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('faculty') ? 'is-invalid' : '' }}" placeholder="{{__('content')}}" name="faculty" value="{{ isset($data['faculty']) ? $data['faculty'] : old('faculty') }}" aria-describedby="facultyFeedback">
                                            @error('faculty')
                                                <div id="facultyFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <h6>{{__('yearAdmission')}}</h6>
                                            <input type="text" class="form-control {{ $errors->has('school_year') ? 'is-invalid' : '' }}" placeholder="{{__('content')}}" name="school_year" value="{{ isset($data['school_year']) ? $data['school_year'] : old('school_year') }}" aria-describedby="schoolYearFeedback">
                                            @error('school_year')
                                                <div id="schoolYearFeedback" class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-10 col-xl-6">
                                        <div class="form-group">
                                            <h6>{{__('password')}}</h6>
                                            <div class="row">
                                                <div class="col-6">
                                                    <input type="password" class="form-control ippass {{ $errors->has('password') ? 'is-invalid' : '' }} password" placeholder="{{__('content')}}" name="password" value="{{ isset($data['password']) ? $data['password'] : old('password') }}" aria-describedby="passwordFeedback">
                                                    @error('password')
                                                        <div id="passwordFeedback" class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-6">
                                                    <button type="button" class="btn btn-admin-primary genpass randpass" id="btn-random-password">{{__('randompass')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <button type="submit" class="btn btn-admin-primary ml-2 px-4">{{__('submit')}}</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('custom_script')
    @include('admin.scripts.student.create-edit-del-student-script')
@endsection
