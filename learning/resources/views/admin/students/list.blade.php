@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 学生アカウント管理')
@section('content')
    <!-- Main content -->
    <section class="content pt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card rounded-0">
                        @if (Session::has('success'))
                            <div class="alert alert-success custom-alert">
                                {!! Session::get('success') !!}
                            </div>
                        @endif
                        <div class="card-header rounded-0 border-bottom-0">
                            <h3 class="card-title font-weight-normal">{{__('managerstudent')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body pt-1">
                            <a href="{{ route('admin.create_student') }}" class="btn btn-admin-primary px-4 mb-3">{{__('add')}}</a>
                            <div class="row">
                                <div class="col-md-8">
                                    <form action="{{ route('admin.student_list') }}" method="GET">
                                        <div class="form-group">
                                            <label for="name" class="font-weight-normal">{{__('search')}}</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="{{__('searchcontent')}}" name="name" value="{{ app('request')->input('name') }}">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-admin-primary px-3">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="table-responsive px-0 my-3" >
                                <table class="table table-striped text-nowrap table-head-fixed table-valign-middle">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{__('name')}}</th>
                                            <th class="text-center">{{__('nameunv')}}</th>
                                            <th class="text-center">{{__('majors')}}</th>
                                            <th class="text-center">{{__('yearAdmission')}}</th>
                                            <th class="text-center">{{__('lastlogin')}}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @if(count($students))
                                            @foreach($students as $student)
                                                <tr>
                                                    <td class="text-wrap">{{ $student->name }}</td>
                                                    <td class="text-wrap">{{ $student->university }}</td>
                                                    <td class="text-wrap">{{ $student->faculty }}</td>
                                                    <td>
                                                        @if ( Config::get('app.locale') == 'vi') 
                                                            {{__('year')}} {{ $student->school_year }} 
                                                        @elseif(Config::get('app.locale') == 'jp')
                                                            {{ $student->school_year }}{{__('year')}}  
                                                        @endif
                                                        
                                                    </td>
                                                    <td>{{ $student->last_login ? date("Y-m-d H:i", strtotime($student->last_login)) : '' }}</td>
                                                    <td><a href="{{ route('admin.student_detail', ['id' => $student->id]) }}" class="btn btn-admin-primary px-3">{{__('details')}}</a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            <!--pagination-->
                            {{ $students->links('layouts.pagination.paginator') }}
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('custom_script')
    @include('admin.scripts.student.list-student-script')
@endsection
