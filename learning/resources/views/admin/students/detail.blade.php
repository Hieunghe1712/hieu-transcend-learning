@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 学生アカウント詳細')
@section('content')
    <!-- Main content -->
    <section class="content pt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card rounded-0">
                        <div class="card-header rounded-0 border-bottom-0">
                            <h3 class="card-title font-weight-normal">{{__('studentdetals')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body pt-1">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-5 col-xl-3">
                                    @php
                                        $profile_img = 'storage/images/noimage.jpg';
                                        if (isset($student['profile_img']) && $student['profile_img'] != '') {
                                            if (@file_exists(AVATAR_STUDENT_SAVE . $student['profile_img'])) {
                                                $profile_img = AVATAR_STUDENT_SAVE_JS . $student['profile_img'];
                                            } elseif (@file_exists(AVATAR_STUDENT_SAVE_TMP . $student['profile_img'])) {
                                                $profile_img = AVATAR_STUDENT_SAVE_TMP_JS . $student['profile_img'];
                                            }
                                        }
                                    @endphp
                                    <img src="{{ isset($student['profile_img']) ? asset('storage/images/' . $student['profile_img']) : url($profile_img) }}" alt="Avatar student" class="avatar">
                                    <div class="bg-admin-primary mt-3 p-1">{{__('name')}}</div>
                                    <div class="mt-1">{{ $student['name'] }}</div>
                                    <div class="bg-admin-primary mt-1 p-1">{{__('emailaddress')}}</div>
                                    <div class="mt-1">{{ $student['email'] }}</div>
                                    <div class="bg-admin-primary mt-1 p-1">{{__('nameunv')}}</div>
                                    <div class="mt-1">{{ $student['university'] }}</div>
                                    <div class="bg-admin-primary mt-1 p-1">{{__('majors')}}</div>
                                    <div class="mt-1">{{ $student['faculty'] }}</div>
                                    <div class="bg-admin-primary mt-1 p-1">{{__('yearAdmission')}}</div>
                                    <div class="mt-1">{{ $student['school_year'] }}</div>
                                    <div class="row mt-3">
                                        <div class="col-6">
                                            <a href="{{ route('admin.editStudent', ['id' => $student['id']]) }}" class="btn btn-admin-primary w-100">{{__('edit')}}</a>
                                        </div>
                                        <div class="col-6">
                                            <a href="{{ route('admin.student_list') }}" class="btn btn-admin-primary w-100">{{__('return')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
