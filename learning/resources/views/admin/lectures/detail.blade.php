@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 講義コンテンツ詳細')
@section('content')
  <!-- Main content -->
  <section class="content pt-3">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card rounded-0">
            <div class="card-header rounded-0 border-bottom-0">
              <h3 class="card-title font-weight-normal">{{__('lecturecontentdetail')}}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row detail-lectures">
                <div class="col-md-6">
                  <div class="pl-3 pr-4">
                    <div id="video" class="video-lecture">
                      @php
                        echo $lecture->embed_code;
                      @endphp
                    </div>

                    <div class="text-color-green py-3">
                      <h6 class="font-weight-bold">{{ $lecture->name }}</h6>
                    </div>
                    @if (count($quizzes))
                      <div class="custom-nav-tab quiz-content">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          @foreach ($quizzes as $quiz)
                            <li class="nav-item">
                              <a class="nav-link px-3 txt-truncate w-100px {{ $quiz->sort_number == 1 ? 'active' : '' }}"
                                data-toggle="tab" href="#quiz{{ $quiz->sort_number }}">{{ $quiz->title }}</a>
                            </li>
                          @endforeach
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content mb-3">
                          @foreach ($quizzes as $quiz)
                            <div id="quiz{{ $quiz->sort_number }}"
                              class="tab-pane border rounded p-3 {{ $quiz->sort_number == 1 ? 'active' : '' }}">
                              <div class="py-1">
                                ー {{__('question')}}
                                <p class="pl-3 mb-1">{{ $quiz->title }}</p>
                              </div>
                              <div class="py-1">
                                ー {{__('comment')}}
                                <p class="pl-3 mb-1">@php echo nl2br($quiz->question_sentence) @endphp</p>
                              </div>
                              <div class="py-1">
                                ー {{__('option')}}
                                @php
                                  $previous = 0;
                                @endphp
                                @foreach ($optionValues as $optionValue)
                                  @if ($optionValue->quiz_id == $quiz->id)
                                    @php
                                      $current = $optionValue->quiz_id;
                                    @endphp
                                    @if ($current != $previous)
                                      <p class="pl-3 mb-1 option-name-{{ $optionValue->optionId }}">
                                        {{ $optionValue->optionName }}<br></p>
                                    @endif
                                    <p class="pl-3 mb-1">{{ $optionValue->name }}<br></p>
                                    @php
                                      $previous = $current;
                                    @endphp
                                  @endif
                                @endforeach
                              </div>
                              <div class="py-1">
                                ー {{__('ideally')}}
                                <p class="pl-3 mb-1">@php echo nl2br($quiz->remark) @endphp</p>
                              </div>
                              {{-- <div class="py-1">
                                                    ー 解説タイトル
                                                    <p class="pl-3 mb-1">テキストテキストテキストテキスト</p>
                                                </div>
                                                <div class="py-1">
                                                    ー 解説文
                                                    <p class="pl-3 mb-1">テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                                                </div> --}}
                              <div class="py-1">
                                ー {{__('starttime')}}
                                <p class="pl-3 mb-1">{{ $quiz->appearance_time }}</p>
                              </div>
                            </div>
                          @endforeach
                        </div>
                      </div>
                    @endif

                    @if (count($explanations))
                      <div class="custom-nav-tab explanation-content mt-4">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                          @foreach ($explanations as $explanation)
                            <li class="nav-item">
                              <a class="nav-link px-4 txt-truncate w-100px {{ $explanation->sort_number == 1 ? 'active' : '' }}"
                                data-toggle="tab"
                                href="#explanation{{ $explanation->sort_number }}">{{ $explanation->title }}</a>
                            </li>
                          @endforeach
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content mb-3">
                          @foreach ($explanations as $explanation)
                            <div id="explanation{{ $explanation->sort_number }}"
                              class="tab-pane border rounded p-3 {{ $explanation->sort_number == 1 ? 'active' : '' }}">
                              <div class="py-1">
                                ー {{__('titlecmt')}}
                                <p class="pl-3 mb-1">{{ $explanation->title }}</p>
                              </div>
                              <div class="py-1">
                                ー {{__('comment')}}
                                <p class="pl-3 mb-1">@php echo nl2br($explanation->content) @endphp</p>
                              </div>
                              <div class="py-1">
                                ー {{__('image')}}
                                <div class="bg-secondary text-center py-5">
                                  <img src="{{ $explanation->uploaded_path }}" width=100%>
                                </div>
                              </div>
                            </div>
                          @endforeach
                        </div>
                      </div>
                    @endif

                    @if (count($headings))
                      <div class="border rounded p-3 mt-4">
                        <div class="py-1">
                          ー {{__('title')}}
                          <p class="pl-3 mb-1">
                            @foreach ($headings as $heading)
                              {{ $heading->heading_time . ' ' . $heading->title }}<br>
                            @endforeach
                          </p>
                        </div>
                      </div>
                    @endif
                    <div class="mt-4 mb-4">
                      <a href="{{ route('admin.lecture_list') }}" class="btn btn-admin-primary px-3">{{__('return')}}</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="row px-4">
                    <div class="col-sm-8 bg-admin-primary py-1 pl-3">{{__('lecturename')}}</div>
                    <div class="col-md-8 py-1 px-3">{{ $lecture->name }}</div>
                    <div class="col-sm-8 bg-admin-primary py-1 pl-3">{{__('comment')}}</div>
                    <div class="col-md-8 py-1 px-3">@php echo nl2br($lecture->explanatory_text) @endphp</div>
                    <div class="col-sm-8 bg-admin-primary py-1 pl-3">{{__('deliverydate')}}</div>
                    <div class="col-md-8 py-1 px-3">
                      {{ $lecture['created_at'] ? date('Y.m.d', strtotime($lecture['created_at'])) : '' }}</div>
                    <div class="col-sm-8 bg-admin-primary py-1 pl-3">{{__('teacher')}}</div>
                    <div class="col-md-8 py-1 px-3">{{ $lecture->lecturerName }}</div>
                    <div class="col-sm-8 bg-admin-primary py-1 pl-3">ID</div>
                    <div class="col-md-8 py-1 px-3">{{ str_pad($lecture->id, 6, '0', STR_PAD_LEFT) }}</div>
                    <div class="col-sm-8 bg-admin-primary py-1 pl-3">{{__('countlike')}}</div>
                    <div class="col-md-8 py-1 px-3">{{ $lecture->liked_number ?? 0 }}</div>
                    <div class="col-sm-8 bg-admin-primary py-1 pl-3">{{__('listnumber')}}</div>
                    <div class="col-md-8 py-1 px-3">{{ str_pad(count($myLectures), 3, '0', STR_PAD_LEFT) }}</div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- ./col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection
@section('custom_script')
  @include('admin.scripts.lecture.create_lecture_script')
@endsection
