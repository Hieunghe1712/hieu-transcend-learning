@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 講義コンテンツ検索')
@section('content')
    <!-- Main content -->
    <section class="content pt-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card rounded-0">
                        @if (Session::has('success'))
                            <div class="alert alert-success custom-alert">
                                {!! Session::get('success') !!}
                            </div>
                        @endif
                        <div class="card-header rounded-0 border-bottom-0">
                            <h3 class="card-title font-weight-normal">{{__('searchlecture')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <form method="get" action="{{ route('admin.lecture_list') }}" accept-charset="UTF-8">
                                        <div class="form-group">
                                            <label for="name" class="font-weight-normal">{{__('lecturenamect')}}</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control fs-7" placeholder="{{__('content')}}" id="name" name="name" value="{{ app('request')->input('name') }}">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-admin-primary px-3">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="table-responsive px-0 my-3">
                                <table class="table table-striped text-nowrap table-head-fixed table-valign-middle">
                                    <thead>
                                        <tr>
                                            <th class="text-center">{{__('lecturenamect')}}</th>
                                            <th class="text-center">{{__('startday')}}</th>
                                            <th class="text-center">{{__('teacher')}}</th>
                                            <th class="text-center">ID</th>
                                            <th class="text-center">{{__('countlike')}}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @if(count($lectures))
                                            @foreach($lectures as $lecture)
                                                <tr>
                                                    <td class="text-wrap">{{ $lecture->name }}</td>
                                                    <td>{{ $lecture['created_at']?date("Y.m.d", strtotime($lecture['created_at'])):'' }}</td>
                                                    <td class="text-wrap">{{ $lecture->lecturerName }}</td>
                                                    <td>{{ str_pad($lecture->id, 6, '0', STR_PAD_LEFT) }}</td>
                                                    <td>{{ $lecture->liked_number??0 }}</td>
                                                    <td><a href="{{ route('admin.lecture_detail', [ 'id' => $lecture->id ]) }}" class="btn btn-admin-primary px-3">{{__('details')}}</a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            <!--pagination-->
                            {{ $lectures->links('layouts.pagination.paginator') }}
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- ./col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
{{-- @section('custom_script')
    @include('admin.scripts.lecture.list_lecture_script')
@endsection --}}
