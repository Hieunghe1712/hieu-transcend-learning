@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | マイ講義コンテンツ編集')
@section('content')
  @php
  $confirm = false;
  @endphp
  <!-- Main content -->
  <section class="content pt-3">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card rounded-0">
            <div class="card-header rounded-0 border-bottom-0">
              <h3 class="card-title font-weight-normal">{{__('editlecturect')}}</h3>
              <!-- Dùng text: マイ講義コンテンツ編集 cho màn A2216: Edit bài giảng của tôi -->
              <!-- Dùng text: マイ講義コンテンツ追加 cho màn A2217 - Bổ sung bài giảng của tôi -->
              <!-- Dùng text: マイ講義コンテンツ確認 cho màn xác nhận nội dung bài giảng -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <form method="post" class="form-create-edit-lecture"
                action="{{ route('admin.lectures.editLecture', $id) }}" accept-charset="UTF-8">
                @csrf
                <input type="hidden" name="confirm_key" class="confirm_key" id="confirm-key" value="{{ $key }}">
                <input type="hidden" name="id" class="id" id="id_lecture" value="{{ $id }}">
                <input type="hidden" name="thumbnail_url" class="thumbnail_url" id="thumbnail_url">
                <div id="video-iframe" style="display:none;"></div>
                <div class="form-group">
                  <label for="name">{{__('titlelecturect')}}</label>
                  <input type="text" class="form-control" placeholder="" id="name" name="name"
                    value="{{ isset($data['name']) ? $data['name'] : old('name') }}">
                  <div id="name-error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                  <label for="question_url">{{__('question')}}</label>
                  <input type="text" class="form-control" placeholder="" id="question_url" name="question_url"
                    value="{{ isset($data['question_url']) ? $data['question_url'] : old('question_url') }}">
                  <div id="question_url-error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                  <label for="test_url">{{__('test')}}</label>
                  <input type="text" class="form-control" placeholder="" id="test_url" name="test_url"
                    value="{{ isset($data['test_url']) ? $data['test_url'] : old('test_url') }}">
                  <div id="test_url-error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                  <label for="homework_url">{{__('homework')}}</label>
                  <input type="text" class="form-control" placeholder="" id="homework_url" name="homework_url"
                    value="{{ isset($data['homework_url']) ? $data['homework_url'] : old('homework_url') }}">
                  <div id="homework_url-error" class="invalid-feedback"></div>
                </div>
                <hr class="custom-line">
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('video')}}</h6>
                <div class="form-group">
                  <label for="embed-code">{{__('embedcode')}}</label>
                  <div class="form-check form-check-inline ml-1">
                    <input class="form-check-input" type="radio" name="ifrem_code" id="Radiovimeo" value="0" checked>
                    <label class="form-check-label" for="inlineRadio1">Vimeo</label>
                  </div>
                  <div class="form-check form-check-inline mb-2">
                    <input class="form-check-input" type="radio" name="ifrem_code" id="Radioyt" value="1">
                    <label class="form-check-label" for="inlineRadio2">Youtube</label>
                  </div>
                  <input type="text" class="form-control w-250px" placeholder="" id="embed-code" name="embed_code"
                    value="{{ isset($data['embed_code']) ? $data['embed_code'] : old('embed_code') }}">
                  <div id="embed_code-error" class="invalid-feedback"></div>
                </div>
                <hr class="custom-line">

                {{-- Quiz --}}
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('question')}}</h6>
                <div id="all-quizz">
                  @include('admin.lectures.form_data.quiz', [
                  'data' => $data,
                  'confirm' => $confirm,
                  ])
                </div>
                <button type="button" class="btn btn-admin-primary" id="add-quiz">
                  {{__('addquestion')}} <i class="fas fa-plus"></i>
                </button>
                <hr class="custom-line">

                {{-- Explanation --}}
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('comment')}}</h6>
                <div id="all-explanation">
                  @include('admin.lectures.form_data.explanation', [
                  'data' => $data,
                  'confirm' => $confirm,
                  ])
                </div>
                <button type="button" class="btn btn-admin-primary" id="add-explanation">
                  {{__('addcomment')}} <i class="fas fa-plus"></i>
                </button>
                <hr class="custom-line">

                {{-- Headings --}}
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('title')}}</h6>
                <div id="all-headings">
                  @include('admin.lectures.form_data.headings', [
                  'data' => $data,
                  'confirm' => $confirm,
                  ])
                </div>
                <button type="button" class="btn btn-admin-primary" id="add-headings">{{__('addtitle')}} <i
                    class="fas fa-plus"></i></button>
                <hr class="custom-line">

                {{-- Documents --}}
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('document')}}</h6>
                @php
                  $displayMemoDocuments = 'display: none';
                  if (isset($data['documents']) && !empty($data['documents'])) {
                      $dataDocuments = json_decode($data['documents'], true);
                      if (!empty($dataDocuments)) {
                          $displayMemoDocuments = '';
                      }
                  }
                @endphp
                <div class="form-group memo-documents" style="{{ $displayMemoDocuments }}">
                  <label for="explanatory-text">{{__('textnote')}}</label>
                  <textarea class="form-control" rows="4" id="explanatory-text"
                    name="explanatory_text">{{ isset($data['explanatory_text']) ? $data['explanatory_text'] : '' }}</textarea>
                  <div id="explanatory_text-error" class="invalid-feedback"></div>
                </div>
                <div id="all-documents">
                  @include('admin.lectures.form_data.document', [
                  'data' => $data,
                  'confirm' => $confirm,
                  ])
                </div>
                <button type="button" class="btn btn-admin-primary" id="add-documents">{{__('adddocument')}} <i
                    class="fas fa-plus"></i></button>

                <div class="form-group mt-4 mb-1">
                  <button type="button" id="submit-form" class="btn btn-admin-primary px-4">{{__('update')}}</button>
                  <button type="button" id="soft-del-lecture" class="btn btn-danger ml-3 px-4">{{__('delete')}}</button>
                </div>
              </form>

            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- ./col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
@endsection
@section('custom_script')
  <script>
    $(function() {
      $("#soft-del-lecture").on("click", function() {
        var url = "{{ route('admin.lectures.deleteLucture', '') }}" + "/" + $("#id_lecture").val()
        let confirmStr = "マイ講義コンテンツ" + "{{ Config::get('message.common_msg.ask_before_delete') }}";
        if (confirm(confirmStr)) {
          window.location = url;
        }
      })
    })

  </script>
  @include('admin.scripts.lecture.create_lecture_script')
@endsection
