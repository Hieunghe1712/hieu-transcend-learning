@php
$dataHeadings = [];
$indexHeadings = 1;
if (isset($data['headings']) && !empty($data['headings'])) {
    $dataHeadings = json_decode($data['headings'], true);
    $indexHeadings = count($dataHeadings) + 1;
}
@endphp
<input type="hidden" name="number_of_headings" class="number_of_headings" value="{{ $indexHeadings }}">
@if (!empty($dataHeadings))
  @foreach ($dataHeadings as $keyHeadings => $heading)
    @php
      $numberOfHeadings = $keyHeadings + 1;
    @endphp
    <div class="card card-default custom-card form-headings" id="form-headings-{{ $numberOfHeadings }}">
      <div class="card-header">
        <h6 class="card-title font-weight-bold">
          @if ( Config::get('app.locale') == 'jp') 
            見出し{{ $numberOfHeadings }}を追加する
          @elseif(Config::get('app.locale') == 'vi')
            Thêm tiêu đề {{ $numberOfHeadings }}
          @endif
          
        </h6>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus custom-icon-collapse"></i>
          </button>
        </div>
      </div>
      <div class="card-body pt-0">
        <input type="hidden" class="form-control heading-index" value="{{ $numberOfHeadings }}">
        <div class="form-group">
          <label for="heading-title-{{ $numberOfHeadings }}">{{__('title')}}</label>
          <input type="text" class="form-control heading-title heading-title-{{ $numberOfHeadings }}"
            id="heading-title-{{ $numberOfHeadings }}" name="heading_title-{{ $numberOfHeadings }}"
            value="{{ trim($heading['title']) }}" {{ $confirm ? 'disabled' : '' }}>
          <div id="heading_title-{{ $numberOfHeadings }}-error" class="invalid-feedback"></div>
        </div>
        <div class="form-group">
          <label for="heading-time-{{ $numberOfHeadings }}">{{__('time')}}</label>
          <input type="text"
            class="form-control w-100px input-mask-time heading-time heading-time-{{ $numberOfHeadings }}"
            id="heading-time-{{ $numberOfHeadings }}" name="heading_time-{{ $numberOfHeadings }}"
            value="{{ $heading['time'] }}" {{ $confirm ? 'disabled' : '' }}>
          <div id="heading_time-{{ $numberOfHeadings }}-error" class="invalid-feedback"></div>
        </div>
      </div>
    </div>
  @endforeach
@endif
