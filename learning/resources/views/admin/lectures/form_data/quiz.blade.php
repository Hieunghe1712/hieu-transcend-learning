@php
$dataQuiz = [];
$indexQuiz = 1;
if (isset($data['quizzes']) && !empty($data['quizzes'])) {
    $dataQuiz = json_decode($data['quizzes'], true);
    $indexQuiz = count($dataQuiz) + 1;
}
@endphp
<input type="hidden" name="number_of_quiz" class="number_of_quiz" value="{{ $indexQuiz }}">
@if (!empty($dataQuiz))
  @foreach ($dataQuiz as $keyQuiz => $quiz)
    @php
      $numberOfQuiz = $keyQuiz + 1;
    @endphp
    <div class="card card-default custom-card form-quiz" id="form-quiz-{{ $numberOfQuiz }}">
      <div class="card-header">
        <h6 class="card-title font-weight-bold">
            @if ( Config::get('app.locale') == 'jp') 
              クイズ{{ $numberOfQuiz }}を編集する
            @elseif(Config::get('app.locale') == 'vi')
              chỉnh sửa câu hỏi {{ $numberOfQuiz }}
            @endif
        </h6>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus custom-icon-collapse"></i>
          </button>
        </div>
      </div>
      <div class="card-body pt-0">
        <input type="hidden" class="form-control quiz-index" value="{{ $numberOfQuiz }}">
        <div class="form-group">
          <label for="quiz-title-{{ $numberOfQuiz }}">{{__('title')}}</label>
          <input type="text" class="form-control quiz-title title-{{ $numberOfQuiz }}"
            name="title-{{ $numberOfQuiz }}" placeholder="" id="quiz-title-{{ $numberOfQuiz }}"
            value="{{ trim($quiz['title']) }}" {{ $confirm ? 'disabled' : '' }}>
          <div id="title-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
        </div>

        <div class="form-group">
          <label for="question-sentence-{{ $numberOfQuiz }}">{{__('questionsentence')}}</label>
          <textarea class="form-control question-sentence question_sentence-{{ $numberOfQuiz }}"
            name="question_sentence-{{ $numberOfQuiz }}" rows="4" id="question-sentence-{{ $numberOfQuiz }}"
            {{ $confirm ? 'disabled' : '' }}>{{ trim($quiz['question_sentence']) }}</textarea>
          <div id="question_sentence-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
        </div>

        <div class="form-group quiz-choices">
          <label for="quiz-option-id-{{ $numberOfQuiz }}">{{__('option')}}</label><br>
          @foreach ($quizOptions as $option)
            @php
              $randomIdLabel = rand();
            @endphp
            <div class="form-check-inline mr-sm-2 quiz_option_id-{{ $numberOfQuiz }}">
              <input type="radio" value="{{ $option->id }}"
                class="form-check-input quiz-option-id quiz_option_id-{{ $numberOfQuiz }}"
                name="quiz_option_id-{{ $numberOfQuiz }}" id="{{ 'quiz-option-id' . $randomIdLabel }}"
                data-choice="{{ $option->have_choices }}"
                {{ $quiz['quiz_option_id'] == $option->id ? 'checked' : '' }}
                {{ $confirm && $quiz['quiz_option_id'] != $option->id ? 'disabled' : '' }}>
              <label class="form-check-label"
                for="{{ 'quiz-option-id' . $randomIdLabel }}">
                @if ( Config::get('app.locale') == 'jp') 
                  {{ $option->name }}
                @elseif(Config::get('app.locale') == 'vi')
                  {{ $option->name_vi }}
                @endif
              </label>
            </div>
          @endforeach
          <div id="quiz_option_id-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
        </div>

        @php
          $displayQuizValues = 'display:none';
          foreach ($quizOptions as $option) {
              if ($quiz['quiz_option_id'] == $option->id) {
                  if ($option->have_choices == QUIZ_HAVE_CHOICE_1) {
                      $displayQuizValues = '';
                      break;
                  }
              }
          }
        @endphp
        <div class="form-group quiz-values" style="{{ $displayQuizValues }}">
          <label for="quiz-values-{{ $numberOfQuiz }}">{{__('option')}}</label>
          <input type="hidden" class="quiz_values">
          <div id="quiz_values-error" class="invalid-feedback"></div>
          <div class="all-choice">
            @if (!empty($quiz['choices']))
              @foreach ($quiz['choices'] as $keyChoice => $choice)
                @php
                  $indexChoice = $keyChoice + 1;
                @endphp
                <div class="row mb-2 choice-value">
                  <div class="col col-sm-4">
                    <input type="text"
                      class="form-control text-right quiz-value name_quiz_value-{{ $numberOfQuiz }}-1"
                      name="name_quiz_value-{{ $numberOfQuiz }}-{{ $indexChoice }}"
                      data-class="quiz_value-{{ $numberOfQuiz }}-{{ $indexChoice }}"
                      data-index-value="{{ $indexChoice }}" value="{{ $choice['name'] }}"
                      {{ $confirm ? 'disabled' : '' }}>
                    <div id="name_quiz_value-{{ $numberOfQuiz }}-{{ $indexChoice }}-error"
                      class="invalid-feedback">
                    </div>
                  </div>
                  <div class="col-auto text-center">
                    @if (!$confirm)
                      <button type="button" class="btn btn-danger remove-choice">{{__('delete')}}</button>
                    @endif
                  </div>
                </div>
              @endforeach
            @endif
          </div>
          @if (!$confirm)
            <button type="button" class="btn btn-admin-primary px-4 add-choice">{{__('moreoption')}}</button>
          @endif
        </div>

        <div class="form-group">
          <label for="quiz-remark-{{ $numberOfQuiz }}">{{__('ideally')}}</label>
          <textarea class="form-control quiz-remark remark-{{ $numberOfQuiz }}" name="remark-{{ $numberOfQuiz }}" rows="4"
            id="comment" {{ $confirm ? 'disabled' : '' }}>{{ trim($quiz['remark']) }}</textarea>
          <div id="remark-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
        </div>

        <div class="form-group">
          <label for="quiz-appearance-time-{{ $numberOfQuiz }}">{{__('starttime')}}</label>
          <input type="text" name="appearance_time-{{ $numberOfQuiz }}"
            class="form-control input-mask-time w-100px quiz-appearance-time appearance_time-{{ $numberOfQuiz }}"
            id="quiz-appearance-time-{{ $numberOfQuiz }}" value="{{ $quiz['appearance_time'] }}"
            {{ $confirm ? 'disabled' : '' }}>
          <div id="appearance_time-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
        </div>
      </div>
    </div>
  @endforeach
@endif
