@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 講義コンテンツ検索')
@section('content')
  @php
  $confirm = true;
  @endphp
  <!-- Main content -->
  <section class="content pt-3">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card rounded-0">
            <div class="card-header rounded-0 border-bottom-0">
              <h3 class="card-title font-weight-normal">{{__('testcontentlecture')}}</h3>
              <!-- Dùng text: マイ講義コンテンツ編集 cho màn A2216: Edit bài giảng của tôi -->
              <!-- Dùng text: マイ講義コンテンツ追加 cho màn A2217 - Bổ sung bài giảng của tôi -->
              <!-- Dùng text: マイ講義コンテンツ確認 cho màn xác nhận nội dung bài giảng -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <form method="post" action="{{ route('admin.lectures.saveEditLecture', ['id' => $id, 'key' => $key]) }}"
                accept-charset="UTF-8" id="formEditLucturesConfirm">
                @csrf
                <input type="hidden" name="confirm_key" class="confirm_key" id="confirm-key" value="{{ $key }}">
                <div class="form-group">
                  <label for="name">{{__('titlelecturect')}}</label>
                  <input type="text" class="form-control" placeholder="" id="name" name="name"
                    value="{{ isset($data['name']) ? $data['name'] : '' }}" disabled>
                  <div id="name-error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                  <label for="question_url">{{__('question')}}</label>
                  <input type="text" class="form-control" placeholder="" id="question_url" name="question_url"
                    value="{{ isset($data['question_url']) ? $data['question_url'] : '' }}" disabled>
                  <div id="question_url-error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                  <label for="test_url">{{__('test')}}</label>
                  <input type="text" class="form-control" placeholder="" id="test_url" name="test_url"
                    value="{{ isset($data['test_url']) ? $data['test_url'] : '' }}" disabled>
                  <div id="test_url-error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                  <label for="homework_url">{{__('homework')}}</label>
                  <input type="text" class="form-control" placeholder="" id="homework_url" name="homework_url"
                    value="{{ isset($data['homework_url']) ? $data['homework_url'] : '' }}" disabled>
                  <div id="homework_url-error" class="invalid-feedback"></div>
                </div>
                <hr class="custom-line">
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('video')}}</h6>
                <div class="form-group">
                  <label for="embed-code">{{__('embedcode')}}</label>
                  <input type="text" class="form-control w-100px mb-2" id="ifrem-code" name="ifrem_code" value="{{ $data['ifrem_code'] ? 'youtube' : 'vimeo' }}" disabled>
                  <input type="text" class="form-control w-250px" placeholder="" id="embed-code" name="embed_code"
                    value="{{ isset($data['embed_code']) ? $data['embed_code'] : '' }}" disabled>
                  <div id="embed_code-error" class="invalid-feedback"></div>
                </div>
                <hr class="custom-line">

                {{-- Quiz --}}
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('question')}}</h6>
                <div id="all-quizz">
                  @include('admin.lectures.form_data.quiz', [
                  'data' => $data,
                  'confirm' => $confirm,
                  ])
                </div>
                <hr class="custom-line">

                {{-- Explanation --}}
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('comment')}}</h6>
                <div id="all-explanation">
                  @include('admin.lectures.form_data.explanation', [
                  'data' => $data,
                  'confirm' => $confirm,
                  ])
                </div>
                <hr class="custom-line">

                {{-- Headings --}}
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('title')}}</h6>
                <div id="all-headings">
                  @include('admin.lectures.form_data.headings', [
                  'data' => $data,
                  'confirm' => $confirm,
                  ])
                </div>
                <hr class="custom-line">

                {{-- Documents --}}
                <h6 class="text-color-green font-weight-bold py-2">ー {{__('document')}}</h6>
                @php
                  $displayMemoDocuments = 'display: none';
                  if (isset($data['documents']) && !empty($data['documents'])) {
                      $dataDocuments = json_decode($data['documents'], true);
                      if (!empty($dataDocuments)) {
                          $displayMemoDocuments = '';
                      }
                  }
                @endphp
                <div class="form-group memo-documents" style="{{ $displayMemoDocuments }}">
                  <label for="explanatory-text">{{__('textnote')}}</label>
                  <textarea class="form-control" rows="4" id="explanatory-text" name="explanatory-text"
                    disabled>{{ $data['explanatory_text'] }}</textarea>
                </div>
                <div id="all-documents">
                  @include('admin.lectures.form_data.document', [
                  'data' => $data,
                  'confirm' => $confirm,
                  ])
                </div>

                <div class="form-group mt-4 mb-1">
                  <button type="button" id="btn-submit-form-confirm" class="btn btn-admin-primary px-4">{{__('update')}}</button>
                  <a href="{{ route('admin.editLectureForm', ['id' => $id, 'key' => $key]) }}">
                    <button type="button" class="btn btn-secondary ml-3 px-4">{{__('return')}}</button>
                  </a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </section>
@endsection
@section('custom_script')
  <script>
    $(function() {
      $("#btn-submit-form-confirm").attr("disabled", false)
      $("#btn-submit-form-confirm").on("click", function() {
        $(this).attr("disabled", 'disabled')
        $("#formEditLucturesConfirm").submit();
      })
    })

  </script>
@endsection
