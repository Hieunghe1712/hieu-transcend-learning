<div class="card card-default custom-card form-explanation" id="form-explanation-{{ $numberOfExplanation }}">
  <div class="card-header">
    <h6 class="card-title font-weight-bold">
        @if ( Config::get('app.locale') == 'jp') 
          解説{{ $numberOfExplanation }}を追加する
        @elseif(Config::get('app.locale') == 'vi')
          Thêm bình luận {{ $numberOfExplanation }}
        @endif
    </h6>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse">
        <i class="fas fa-minus custom-icon-collapse"></i>
      </button>
    </div>
  </div>
  <div class="card-body pt-0">
    <input type="hidden" class="form-control explanation-index" value="{{ $numberOfExplanation }}">
    <div class="form-group">
      <label for="explanation-title-{{ $numberOfExplanation }}">{{__('titlecomment')}}</label>
      <input type="text" class="form-control explanation-title" id="explanation-title-{{ $numberOfExplanation }}"
        name="explanation_title-{{ $numberOfExplanation }}">
      <div id="explanation_title-{{ $numberOfExplanation }}-error" class="invalid-feedback"></div>
    </div>
    <div class="form-group">
      <label for="commentary">{{__('comment')}}</label>
      <textarea class="form-control explanation-content" rows="4" id="explanation-content-{{ $numberOfExplanation }}"
        name="explanation_content-{{ $numberOfExplanation }}"></textarea>
      <div id="explanation_content-{{ $numberOfExplanation }}-error" class="invalid-feedback"></div>
    </div>
    <div class="form-group">
      <label for="image">{{__('fileimage')}}</label>
      <div class="form-group upload-file-explanation">
        <button type="button" class="btn btn-default px-4 upload-file">{{__('uploadfile')}}</button>
        <input type="file" class="form-control-file explanation-file" style="display: none">
        <span class="ml-2 file-name file-name-{{ $numberOfExplanation }}">{{__('nofile')}}</span>
        <input type="hidden" class="ml-2 file-name-upload file-name-upload-{{ $numberOfExplanation }}"
          name="explanation_file_name-{{ $numberOfExplanation }}">
        <input type="hidden" class="ml-2 dir-file-upload dir-file-upload-{{ $numberOfExplanation }}"
          name="dir_file_upload-{{ $numberOfExplanation }}">
        <input type="hidden" class="ml-2 dir-file-name-upload dir-file-name-upload-{{ $numberOfExplanation }}"
          name="dir_file_name-{{ $numberOfExplanation }}">
        <div id="explanation_file_name-{{ $numberOfExplanation }}-error" class="invalid-feedback"></div>
      </div>

    </div>
  </div>
</div>
