<div class="card card-default custom-card form-quiz" id="form-quiz-{{ $numberOfQuiz }}">
  <div class="card-header">
    <h6 class="card-title font-weight-bold">
      @if ( Config::get('app.locale') == 'jp') 
        クイズ{{ $numberOfQuiz }}を編集する
      @elseif(Config::get('app.locale') == 'vi')
        chỉnh sửa câu hỏi {{ $numberOfQuiz }}
      @endif
    </h6>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse">
        <i class="fas fa-minus custom-icon-collapse"></i>
      </button>
    </div>
  </div>
  <div class="card-body pt-0">
    <input type="hidden" class="form-control quiz-index" value="{{ $numberOfQuiz }}">
    <div class="form-group">
      <label for="quiz-title-{{ $numberOfQuiz }}">{{__('title')}}</label>
      <input type="text" class="form-control quiz-title title-{{ $numberOfQuiz }}" name="title-{{ $numberOfQuiz }}"
        placeholder="" id="quiz-title-{{ $numberOfQuiz }}">
      <div id="title-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
    </div>

    <div class="form-group">
      <label for="question-sentence-{{ $numberOfQuiz }}">{{__('questionsentence')}}</label>
      <textarea class="form-control question-sentence question_sentence-{{ $numberOfQuiz }}"
        name="question_sentence-{{ $numberOfQuiz }}" rows="4"
        id="question-sentence-{{ $numberOfQuiz }}"></textarea>
      <div id="question_sentence-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
    </div>

    <div class="form-group quiz-choices">
      <label for="quiz-option-id-{{ $numberOfQuiz }}">{{__('option')}}</label><br>
      @foreach ($quizOptions as $option)
        @php
          $randomIdLabel = rand();
        @endphp
        <div class="form-check-inline mr-sm-2 quiz_option_id-{{ $numberOfQuiz }}">
          <input type="radio" value="{{ $option->id }}"
            class="form-check-input quiz-option-id quiz_option_id-{{ $numberOfQuiz }}"
            name="quiz_option_id-{{ $numberOfQuiz }}" id="{{ 'quiz-option-id' . $randomIdLabel }}"
            data-choice="{{ $option->have_choices }}">
          <label class="form-check-label" for="{{ 'quiz-option-id' . $randomIdLabel }}">
            @if ( Config::get('app.locale') == 'jp') 
              {{ $option->name }}
            @elseif(Config::get('app.locale') == 'vi')
              {{ $option->name_vi }}
            @endif
          </label>
        </div>
      @endforeach
      <div id="quiz_option_id-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
    </div>

    <div class="form-group quiz-values" style="display:none">
      <label for="quiz-values-{{ $numberOfQuiz }}">{{__('option')}}</label>
      <input type="hidden" class="quiz_values">
      <div id="quiz_values-error" class="invalid-feedback"></div>
      <div class="all-choice">
        <div class="row mb-2 choice-value">
          <div class="col col-sm-4">
            <input type="text" class="form-control text-right quiz-value name_quiz_value-{{ $numberOfQuiz }}-1"
              name="name_quiz_value-{{ $numberOfQuiz }}-1" data-class="quiz_value-{{ $numberOfQuiz }}-1"
              data-index-value="1">
            <div id="name_quiz_value-{{ $numberOfQuiz }}-1-error" class="invalid-feedback"></div>
          </div>
          <div class="col-auto text-center">
            <button type="button" class="btn btn-danger remove-choice">{{__('delete')}}</button>
          </div>
        </div>
      </div>
      <button type="button" class="btn btn-admin-primary px-4 add-choice">{{__('moreoption')}}</button>
    </div>

    <div class="form-group">
      <label for="quiz-remark-{{ $numberOfQuiz }}">{{__('ideally')}}</label>
      <textarea class="form-control quiz-remark remark-{{ $numberOfQuiz }}" name="remark-{{ $numberOfQuiz }}" rows="4"
        id="comment"></textarea>
      <div id="remark-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
    </div>

    <div class="form-group">
      <label for="quiz-appearance-time-{{ $numberOfQuiz }}">{{__('starttime')}}</label>
      <input type="text" name="appearance_time-{{ $numberOfQuiz }}"
        class="form-control input-mask-time w-100px quiz-appearance-time appearance_time-{{ $numberOfQuiz }}"
        id="quiz-appearance-time-{{ $numberOfQuiz }}">
      <div id="appearance_time-{{ $numberOfQuiz }}-error" class="invalid-feedback"></div>
    </div>
  </div>
</div>
