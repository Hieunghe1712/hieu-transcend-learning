<div class="card card-default custom-card form-documents" id="form-documents-{{ $numberOfDocuments }}">
  <div class="card-header">
    <h6 class="card-title font-weight-bold">
      @if ( Config::get('app.locale') == 'jp') 
        資料{{ $numberOfDocuments }}を追加する
      @elseif(Config::get('app.locale') == 'vi')
        Thêm tài liệu {{ $numberOfDocuments }}
      @endif
    </h6>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse">
        <i class="fas fa-minus custom-icon-collapse"></i>
      </button>
    </div>
  </div>
  <div class="card-body pt-0">
    <input type="hidden" class="form-control documents-index" value="{{ $numberOfDocuments }}">
    <div class="form-group">
      <label for="documents-title-{{ $numberOfDocuments }}">{{__('title')}}</label>
      <input type="text" class="form-control documents-title documents-title-{{ $numberOfDocuments }}"
        id="documents-title-{{ $numberOfDocuments }}" name="documents_title-{{ $numberOfDocuments }}">
      <div id="documents_title-{{ $numberOfDocuments }}-error" class="invalid-feedback"></div>
    </div>
    <div class="form-group">
      <label for="file">
        @if ( Config::get('app.locale') == 'jp') 
          資料を添付する（50MBまで）
        @elseif(Config::get('app.locale') == 'vi')
          Đính kèm tài liệu（Tối đa 50MB）
        @endif
      </label>
      <div class="form-group upload-file-documents">
        <button type="button" class="btn btn-default px-4 documents-upload-file">{{__('uploadfile')}}</button>
        <input type="file" class="form-control-file documents-file" style="display: none">
        <span class="ml-2 file-name file-name-{{ $numberOfDocuments }}">{{__('nofile')}}</span>
        <input type="hidden" class="ml-2 file-name-upload file-name-upload-{{ $numberOfDocuments }}"
          name="documents_file_name-{{ $numberOfDocuments }}">
        <input type="hidden" class="ml-2 dir-file-upload dir-file-upload-{{ $numberOfDocuments }}"
          name="dir_file_upload-{{ $numberOfDocuments }}">
        <input type="hidden" class="ml-2 file_created_at file_created_at-{{ $numberOfDocuments }}"
          name="file_created_at-{{ $numberOfDocuments }}">
        <input type="hidden" class="ml-2 dir-file-name-upload dir-file-name-upload-{{ $numberOfDocuments }}"
          name="dir_file_name_upload-{{ $numberOfDocuments }}">
        <div id="documents_file_name-{{ $numberOfDocuments }}-error" class="invalid-feedback"></div>
      </div>
    </div>
  </div>
</div>
