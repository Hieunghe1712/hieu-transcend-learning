@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 講義コンテンツ検索')
@section('content')
<!-- Main content -->
<section class="content pt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card rounded-0">
                    @if (Session::has('success'))
                        <div class="alert alert-success custom-alert">
                            {!! Session::get('success') !!}
                        </div>
                    @endif
                    @if (Session::has('error'))
                            <div class="alert alert-danger custom-alert">
                                {!! Session::get('error') !!}
                            </div>
                        @endif
                    <div class="card-header rounded-0 border-bottom-0">
                        <h3 class="card-title font-weight-normal">{{__('lecturemanager')}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <a href="{{ route('admin.create_lecture_form') }}" class="btn btn-admin-primary px-4 mb-3">{{__('add')}}</a>
                                <form method="get" action="{{ route('admin.my_lecture_list') }}" accept-charset="UTF-8">
                                    <div class="form-group">
                                        <label for="name" class="font-weight-normal">{{__('search')}}</label>
                                        <div class="input-group mb-3">
                                            <input type="text" name="name" class="form-control fs-7" placeholder="{{__('content')}}" id="name" value="{{ app('request')->input('name') }}">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-admin-primary px-3">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive px-0 my-3">
                            <table class="table table-striped text-nowrap table-head-fixed table-valign-middle">
                                <thead>
                                    <tr>
                                        <th class="text-center">{{__('lecturenamect')}}</th>
                                        <th class="text-center">{{__('startstatus')}}</th>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">{{__('countlike')}}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    @if(count($listLectures))
                                        @foreach($listLectures as $lecture)
                                            <tr>
                                                <td class="text-wrap">{{ $lecture->name }}</td>
                                                <td>{{__('public')}}</td>
                                                <td>{{ str_pad($lecture->id, 6, '0', STR_PAD_LEFT) }}</td>
                                                <td>{{ $lecture->liked_number??0 }}</td>
                                                <td><a href="{{ route('admin.editLectureForm', ['id' => $lecture['id']]) }}" class="btn btn-admin-primary px-3">{{__('edit')}}</a></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>        
                        <!--pagination-->
                        {{ $listLectures->links('layouts.pagination.paginator') }}
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- ./col -->
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
{{-- @section('custom_script')
    @include('admin.scripts.lecture.list_lecture_script')
@endsection --}}
