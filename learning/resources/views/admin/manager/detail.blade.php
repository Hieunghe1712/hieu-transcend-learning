@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 運営アカウント詳細')
@section('content')
<!-- Main content -->
<section class="content pt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card rounded-0">
                    <div class="card-header rounded-0 border-bottom-0">
                        <h3 class="card-title font-weight-normal">{{__('detailsmanager')}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pt-1">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-5 col-xl-3">
                                @php
                                $profile_img = 'storage/images/noimage.jpg';
                                @endphp
                                <img src="{{ isset($account['profile_img']) ? asset('storage/images/' . $account['profile_img']) : url($profile_img)  }}" class="avatar" alt="Manager Avatar" id="avatar-manager">
                                <div class="bg-admin-primary mt-3 p-1">{{__('name')}}</div>
                                <div class="mt-1">{{ $account['name'] }}</div>
                                <div class="bg-admin-primary mt-1 p-1">{{__('emailaddress')}}</div>
                                <div class="mt-1">{{ $account['email'] }}</div>
                                <div class="bg-admin-primary mt-1 p-1">{{__('nameunv')}}</div>
                                <div class="mt-1">{{ $account['university'] }}</div>
                                <div class="bg-admin-primary mt-1 p-1">{{__('majors')}}</div>
                                <div class="mt-1">{{ $account['faculty'] }}</div>
                                <div class="bg-admin-primary mt-1 p-1">{{__('permission')}}</div>
                                <div class="mt-1">{{ getPermission((int)$account['permission']) }}</div>
                                <div class="bg-admin-primary mt-1 p-1">{{__('password')}}</div>
                                <div class="mt-1">{{ PASSWORD }}</div>
                                <div class="row mt-3">
                                    <div class="col-6">
                                        <a href="{{ route('admin.editForm', ['id' => $account['id']]) }}" class="btn btn-admin-primary w-100">{{__('submit')}}</a>
                                    </div>
                                    <div class="col-6">
                                        <a href="{{ route('admin.account_manager_list') }}" class="btn btn-admin-primary w-100">{{__('return')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- ./col -->
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
