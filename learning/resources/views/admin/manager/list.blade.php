@extends('layouts.admin.default')
@section('title', 'Transcend-Learningシステム | 運営アカウント管理')
@section('content')
<!-- Main content -->
<section class="content pt-3">
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card rounded-0">
                @if (Session::has('success'))
                    <div class="alert alert-success custom-alert">
                        {!! Session::get('success') !!}
                    </div>
                @endif
                <div class="card-header rounded-0 border-bottom-0">
                    <h3 class="card-title font-weight-normal">{{__('managerstatusac')}}</h3>
                </div>
                    <!-- /.card-header -->
                    <div class="card-body pt-1">
                        <a href="{{ route('admin.createForm') }}" class="btn btn-admin-primary px-4 mb-3">{{__('add')}}</a>
                        <form action="" method="GET" id="form-search-manager">
                            <div class="row">
                                <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="name" class="font-weight-normal">{{__('search')}}</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control manager-name" placeholder="{{__('searchcontent')}}" name="name" value="{{ app('request')->input('name') }}">
                                                <div class="input-group-append">
                                                    <button data-href="{{ route('admin.account_manager_list') }}" type="button" class="btn btn-admin-primary px-3 search-manager">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            @php
                                $arr = app('request')->input();
                                $permission = [];
                                if ($arr){
                                    if (isset($arr['permission'])) {
                                        $permission = json_decode($arr['permission'], true);
                                    }

                                }
                            @endphp
                            <div class="form-check-inline">
                                <label class="check font-weight-normal">{{__('activate')}}
                                    <input type="checkbox"
                                    @if($arr)
                                        @if(!empty($permission))
                                            @if(in_array("1", $permission))
                                                {{"checked"}}
                                            @endif
                                        @endif
                                        @else {{"checked"}}
                                    @endif
                                    value="1" name="permission[]" class="permission">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="check font-weight-normal">{{__('teacher')}}
                                    <input type="checkbox"
                                    @if($arr)
                                        @if(!empty($permission))
                                            @if(in_array("2", $permission))
                                                {{"checked"}}
                                            @endif
                                        @endif
                                        @else {{"checked"}}
                                    @endif
                                    value="2" name="permission[]" class="permission">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </form>
                        <div class="table-responsive px-0 my-3" >
                            <table class="table table-striped text-nowrap table-head-fixed table-valign-middle">
                                <thead>
                                    <tr>
                                        <th class="text-center">{{__('name')}}</th>
                                        <th class="text-center">{{__('nameunv')}}</th>
                                        <th class="text-center">{{__('majors')}}</th>
                                        <th class="text-center">{{__('lastlogin')}}</th>
                                        <th class="text-center">{{__('permisstion')}}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    @if(count($listAccount))
                                        @foreach($listAccount as $account)
                                        <tr>
                                            <td>{{ $account->name }}</td>
                                            <td>{{ $account->university }}</td>
                                            <td>{{ $account->faculty }}</td>
                                            <td>{{ $account->last_login ? date("Y-m-d H:i", strtotime($account->last_login)) : '' }}</td>
                                            <td>
                                                @if($account->permission == 1)
                                                    {{__('activate')}}
                                                @elseif($account->permission == 2)
                                                    {{__('teacher')}}
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.get_account', ['id' => $account['id']]) }}" class="btn btn-admin-primary">{{__('details')}}</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    <!--pagination-->
                    {{ $listAccount->links('layouts.pagination.paginator') }}
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        <!-- ./col -->
    </div>
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
@section('custom_script')
    @include('admin.scripts.manager.list-manager-script')
@endsection
