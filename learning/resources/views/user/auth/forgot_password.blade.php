@extends('layouts.user.auth')
@section('title', 'パスワードを忘れた方はこちら｜関大ウェブ会議ツール開発')
@section('content')
<main id="content">
    <div class="login">
        <p class="mb10">
            登録メールアドレスにパスワードリセット用URLを送信します。
        </p>
        <div class="user_box">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                <strong> 
                    {{ session('status') }}
                </strong>
            </div>
            @endif
            <form action="{{ route('user.postForgotPassword') }}" method="post">
                @csrf
                <dl class="dl_list">
                    <dt>E-mail</dt>
                    <dd>
                        <input id="email" type="text" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" aria-describedby="emailFeedback">
                        @error('email')
                            <div id="emailFeedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </dd>
                </dl>
                <div class="btn_button">
                    <button type="submit" class="user_button_submit">送信</button>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection
