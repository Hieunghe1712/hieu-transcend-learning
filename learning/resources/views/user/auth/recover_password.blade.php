@extends('layouts.user.auth')
@section('title', 'パスワードを忘れた方はこちら｜関大ウェブ会議ツール開発')
@section('content')
<main id="content">
    <div class="login">
        <div class="user_box">
            @if (Session::has('success'))
                @include('layouts.user.flash')
            @endif
            <form action="{{ route('user.postRecoverPassword') }}" method="post">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}">
                <dl class="dl_list">
                    <dt>パスワード</dt>
                    <dd>
                        <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password" aria-describedby="passwordFeedback">
                        @error('password')
                            <div id="passwordFeedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </dd>
                </dl>
                <dl class="dl_list">
                    <dt>パスワード(再入力)</dt>
                    <dd>
                        <input id="password-confirm" type="password" class="@error('password_confirm') is-invalid @enderror" name="password_confirm" area-describedby="passwordConfirmFeedback">
                        @error('password_confirm')
                            <div id="passwordConfirmFeedback" class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </dd>
                </dl>
                <div class="btn_button">
                    <button type="submit" class="user_button_submit">ログイン</button>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection
