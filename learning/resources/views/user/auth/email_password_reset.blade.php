Hi,

A request has been received to change the password for your account.

{{ route('user.password.reset', [ 'token' => $token ]) }}?email={{ $user->email }}

If you did not initiate this request, please contact us immediately at abc@gmail.com

Trancend_learning Team
abc@gmail.com
1-3-3 Uchikanda, Chiyoda
Tokyo, 101-0047, Japan
Sent to: {{ $user->email }}

If you would not like to receive any solicitation from Trancend Learning, contact abc@gmail.com to let us unsubscribe you.
