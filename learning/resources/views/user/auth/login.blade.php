@extends('layouts.user.auth')
@section('title', 'ログイン｜関大ウェブ会議ツール開発')
@section('content')
  <main id="content">
    <div class="login">
      <div class="user_box">
        @if (Session::has('success'))
          @include('layouts.user.flash')
        @endif
        @if (Session::has('error'))
          @include('layouts.user.flash')
        @endif
        <form action="{{ route('user.login') }}" method="post">
          @csrf
          <dl class="dl_list">
            <dt>{{__('emailaddress')}}</dt>
            <dd>
              <input type="text" name="email" id="name" class="email @error('email') is-invalid @enderror"
                value="{{ old('email') }}" aria-describedby="emailFeedback">
              @error('email')
                <div id="emailFeedback" class="invalid-feedback">{{ $message }}</div>
              @enderror
            </dd>
          </dl>
          <dl class="dl_list">
            <dt>{{__('password')}}</dt>
            <dd>
              <input id="password" type="password" class="pwd @error('password') is-invalid @enderror" name="password"
                aria-describedby="passwordFeedback">
              @error('password')
                <div id="passwordFeedback" class="invalid-feedback">{{ $message }}</div>
              @enderror
            </dd>
          </dl>
          <div class="btn_button mb10">
            <button type="submit" class="user_button_submit">{{__('submit')}}</button>
          </div>
          <p class="forgot-password">{{__('forgetpass')}}<a href="{{ route('user.forgotPassword') }}">{{__('here')}}</a></p>
        </form>
      </div>
    </div>
  </main>
@endsection
