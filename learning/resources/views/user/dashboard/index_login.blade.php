@extends('layouts.user.default')
@section('title', 'Transcend-Learning｜関大ウェブ会議ツール開発')
@section('content')
<main id="content" class="index_page">
    <section class="top_mv section_margin">
      <div class="section">
        <h1>Transcend-Learning</h1>
        <p>{{__('documents')}}</p>
      </div>
    </section>

    <section class="section top_version section_margin">
      <h2 class="title_L center">Transcend-Learning</h2>
      <p>
        {{__('depcriptionweb')}}. {{__('documents')}}
      </p>
      <div class="center btn btn-block mt20">
        <a href="{{ route('user.login') }}">{{__('login')}}</a>
      </div>
    </section>

    <div class="section section_margin">
      <ul class="lesson_list lesson_parts">
        @foreach ($listLectures as $lecture)
        <li>
          <a href="{{ route('user.lectures.detail', $lecture->id) }}">
            @php
              $avatar = '/assets/dist/img/no-image-available.png';
              if (isset( $lecture->thumbnail_url) &&  $lecture->thumbnail_url != '') {
                $avatar= $lecture->thumbnail_url;
              }
            @endphp
            <figure class="img img-lecture"><img src="{{ url($avatar) }}" alt=""></figure>
            <div class="detail">
              {{-- <p class="series">シリーズタイトル　1/3</p> --}}
              <strong class="title">{{$lecture->name}}</strong>
              <p class="text">  @php echo nl2br($lecture->explanatory_text); @endphp</p>
              <span class="author">{{$lecture->university}}</span>
              <div class="value">
                <span class="watch">{{ $lecture->view_number }}{{__('view')}}</span>
                <time class="time">{{ $lecture->created_at ? date("Y.m.d h:s", strtotime($lecture->created_at)) : '' }}</time>
              </div>
              <span class="nice">{{ $lecture->liked_number ?? 0 }}</span>
            </div>
          </a>
        </li>
        @endforeach
      </ul>
    </div>

    <section class="section side_block">
      <figure class="side_left">
        <img src="{!! asset('user/dist/images/sample_img_01.png') !!}" alt="Transcend-Learning">
      </figure>
      <div class="side_right">
        <h2 class="title_M">{{__('corporations')}}</h2>
        <p>
          {{__('explain')}}<br>
          {{__('documents')}}
        </p>
        <div class="btn btn-block center mt20"><a href="/corporation_group.html">{{__('detail')}}</a></div>   
      </div>
    </section>

  </main>
@endsection
