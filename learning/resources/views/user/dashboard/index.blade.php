@extends('layouts.user.default')
@section('title', 'Transcend-Learning｜関大ウェブ会議ツール開発')
@section('content')
    <main id="content" class="index_page">
        <section class="bg_color_main section_margin">
            <div class="section">
                <h1 class="title_L">{{__('recentlectures')}}</h1>
                <ul class="lesson_list lesson_parts">
                    @foreach ($topLectures as $lecture)
                        <li>
                            <a href="{{ route('user.lectures.detail', $lecture->id) }}">
                                @php
                                    $avatar = '/assets/dist/img/no-image-available.png';
                                    if (isset($lecture->thumbnail_url) && $lecture->thumbnail_url != '') {
                                        $avatar = $lecture->thumbnail_url;
                                    }
                                @endphp
                                <figure class="img img-lecture"  ><img src="{{ url($avatar) }}" alt=""></figure>
                                <div class="detail">
                                    {{-- <p class="series">シリーズタイトル　1/3</p> --}}
                                    <strong class="title">{{ $lecture->name }}</strong>
                                    <p class="text">@php echo nl2br($lecture->explanatory_text) @endphp</p>
                                    <span class="author">{{ $lecture->university }}</span>
                                    <div class="value">
                                        <span class="watch">{{ $lecture->view_number }}万回視聴</span>
                                        <time
                                            class="time">{{ $lecture->created_at ? date('Y.m.d h:s', strtotime($lecture->created_at)) : '' }}</time>
                                    </div>
                                    <span class="nice">{{ $lecture->liked_number ?? 0 }}</span>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>

        <section class="section">
            <h1 class="title_L center">現在履修可能な講義</h1>
            <ul class="lesson_list lesson_parts">
              @if(count($listLectures))
                @foreach ($listLectures as $list)
                    <li>
                        <a href="{{ route('user.lectures.detail', $list->id) }}">
                            @php
                                $img = '/assets/dist/img/no-image-available.png';
                                if (isset($list->thumbnail_url) && $list->thumbnail_url != '') {
                                    $img = $list->thumbnail_url;
                                }
                            @endphp
                            <figure class="img img-lecture"><img src="{{ url($img) }}" alt=""></figure>
                            <div class="detail">
                              {{-- <p class="series">シリーズタイトル　1/3</p> --}}
                              <strong class="title">{{ $list->name }}</strong>
                              <p class="text">@php echo nl2br($list->explanatory_text) @endphp</p>
                              <span class="author">{{ $list->university }}</span>
                              <div class="value">
                                  <span class="watch">{{ $list->view_number }}万回視聴</span>
                                  <time
                                      class="time">{{ $list->created_at ? date('Y.m.d h:s', strtotime($list->created_at)) : '' }}</time>
                              </div>
                              <span class="nice">{{ $list->liked_number ?? 0 }}</span>
                          </div>
                        </a>
                    </li>
                @endforeach
                @else
                  <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
                @endif
            </ul>
            {{ $listLectures->links('layouts.pagination.user_paginator') }}
        </section>

    </main>
@endsection
