@extends('layouts.user.default')
@section('title', 'マイページ｜関大ウェブ会議ツール開発')
@section('content')
    <!-- Main content -->
    <main id="content">
        <section class="section">
            <h1 class="title_L">プロフィール</h1>
            <section>
                <h2 class="title_M">{{$infoUser['name']}}</h2>
                <div class="profile">
                    <figure class="img">
                        @php
                            $profile_img = '/assets/dist/img/no-image-available.png';
                            if (isset($infoUser['profile_img']) && $infoUser['profile_img'] != '') {
                                if (@file_exists(AVATAR_STUDENT_SAVE . $infoUser['profile_img'])) {
                                    $profile_img = AVATAR_STUDENT_SAVE_JS . $infoUser['profile_img'];
                                } elseif (@file_exists(AVATAR_STUDENT_SAVE_TMP . $infoUser['profile_img'])) {
                                    $profile_img = AVATAR_STUDENT_SAVE_TMP_JS . $infoUser['profile_img'];
                                }
                            }
                        @endphp
                        <img src="{{ url($profile_img) }}" alt="Avatar student" class="avatar">
                    </figure>
                    <div class="detail">
                        <dl>
                            <dt>メールアドレス</dt>
                            <dd>{{$infoUser['email']}}</dd>
                        </dl>
                        <dl>
                            <dt>学校名</dt>
                            <dd>{{$infoUser['university']}}</dd>
                        </dl>
                        <dl>
                            <dt>入学年</dt>
                            <dd>{{$infoUser['school_year']}}</dd>
                        </dl>
                    </div>

                </div>
                <div class="center btn btn-block mt20">
                    <a href="{{ route('user.profile.getEdit', $infoUser['id']) }}">編集する</a>
                  </div>
            </section>
        </section>
    </main>
    <!-- /.content -->
@endsection

