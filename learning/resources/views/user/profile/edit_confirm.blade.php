@extends('layouts.user.default')
@section('title', 'マイページ｜関大ウェブ会議ツール開発')
@section('content')
    <!-- Main content -->
    <main id="content">
        <section class="section">
            <h1 class="title_L">プロフィール変更</h1>
            <div class="form_area">
                @php
                    if (!empty(Session::get('data'))) {
                        $data = Session::get('data');
                    }
                @endphp
                <form action="{{ route('user.profile.saveEditProfile') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="key" value="{{ $key }}">
                    <dl>
                        <input type="hidden" name="id" value="{{ $data['id'] }}">
                        <dt>アイコン</dt>
                        <div class="upload-image-preview h-auto bg-transparent col-md-3">
                            @php
                                $profile_img = '/assets/dist/img/no-image-available.png';
                                if (isset($data['profile_img']) && $data['profile_img'] != '') {
                                    if (@file_exists(AVATAR_STUDENT_SAVE . $data['profile_img'])) {
                                        $profile_img = AVATAR_STUDENT_SAVE_JS . $data['profile_img'];
                                    } elseif (@file_exists(AVATAR_STUDENT_SAVE_TMP . $data['profile_img'])) {
                                        $profile_img = AVATAR_STUDENT_SAVE_TMP_JS . $data['profile_img'];
                                    }
                                }
                            @endphp
                            <img src="{{ url($profile_img) }}" alt="Avatar student" width="40%"
                                style="height: 65%;">
                        </div>
                    </dl>
                    @isset($data['password'])
                        <dl>
                            <dt>新しいパスワード</dt>
                            <dd>
                                <input type="text" class="form-control col-md-3 password" value="{{ $data["password"] }}" disabled>
                            </dd>
                        </dl>
                    @endisset
                    @isset($data['password_confirmation'])
                        <dl>
                            <dt>新しいパスワード(確認)</dt>
                            <dd>
                                <input type="text" class="form-control col-md-3 password" value="{{ $data['password_confirmation'] }}" disabled>
                            </dd>
                        </dl>
                    @endisset
                    <div class="form-inline">
                        <div class="btn_back">
                            <a href="{{ route('user.profile.getEdit', ['key' => $key]) }}">
                                <button type="button">戻る</button>
                            </a>
                        </div>
                        <div class="btn_button">
                            <button type="submit">変更</button>
                        </div>
                    </div>

                </form>
            </div>
        </section>
    </main>
    <!-- /.content -->
@endsection
