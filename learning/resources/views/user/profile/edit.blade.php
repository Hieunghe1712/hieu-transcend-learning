@extends('layouts.user.default')
@section('title', 'マイページ｜関大ウェブ会議ツール開発')
@section('content')
    <!-- Main content -->
    <main id="content">
        <section class="section" >
            <h1 class="title_L">プロフィール変更</h1>
            <div class="form_area" >
                @php
                if (!empty(Session::get('data'))) {
                    $data = Session::get('data');
                }
            
                @endphp
                <form action="{{route('user.profile.postEdit')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <dl>
                        <input type="hidden" name="id" value="{{ $data['id'] }}">
                        <dt>アイコン</dt>
                        <div class="upload-image-preview upload-area col-md-3 mb-3 px-0">
                            @php
                                $profile_img = '/assets/dist/img/no-image-available.png';
                                if (isset($data['profile_img']) && $data['profile_img'] != '') {
                                    if (@file_exists(AVATAR_STUDENT_SAVE . $data['profile_img'])) {
                                        $profile_img = AVATAR_STUDENT_SAVE_JS . $data['profile_img'];
                                    } elseif (@file_exists(AVATAR_STUDENT_SAVE_TMP . $data['profile_img'])) {
                                        $profile_img = AVATAR_STUDENT_SAVE_TMP_JS . $data['profile_img'];
                                    }
                                }
                            @endphp
                            <img src="{{ url($profile_img) }}" alt="Avatar profile" width="40%" style="height: 65%;" id="avatar-profile">
                            <input type="hidden" name="profile_img" id="profile-image" value="{{ isset($data['profile_img']) ? $data['profile_img'] : '' }}">
                        </div>
                        <button type="button" class="btn btn-default px-2 upload-avatar-profile">ファイルを選択</button>
                        <input type="file" class="form-control-file" id="profile-img" style="display:none" value="{{ isset($data['profile_img']) ? $data['profile_img'] : '' }}">
                        <span class="ml-2 file-name">選択されていません</span>
                    </dl>
                    <dl>
                        <dt>新しいパスワード</dt>
                        <dd>
                            <input type="password" class="form-control col-md-3 password {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="" name="password" value="{{ isset($data['password']) ? $data['password'] : old('password') }}" aria-describedby="passwordFeedback">
                            @error('password')
                                <div id="passwordFeedback" class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </dd>
                    </dl>
                    <dl>
                        <dt>新しいパスワード(確認)</dt>
                        <dd>
                            <input type="password" value="{{ isset($data['password_confirmation']) ? $data['password_confirmation'] : old('password_confirmation') }}" name="password_confirmation" class="form-control col-md-3 password {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"  aria-describedby="passwordConfirmFeedback">
                            @error('password_confirmation')
                                <div id="passwordConfirmFeedback" class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </dd>
                    </dl>
                    <div class="btn_button">
                        <button type="submit">変更</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
    <!-- /.content -->
@endsection
@section('custom_script')
    @include('user.scripts.profile.profile_script')
@endsection