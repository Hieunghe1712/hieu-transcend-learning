@extends('layouts.user.default')
@section('title', 'Transcend-Learningシステム | 講義メモ')
@section('content')
<main id="content">
  <section class="section">
    @include('layouts.user.flash')
    <h1 class="title_L">{{__('notelecture')}}</h1>
    <ul class="memo_list lesson_parts lectures">
      @if(count($listLectures))
        @foreach($listLectures as $lecture)
          <li>
              <div class="summary">
                <figure class="img">
                  <a href="{{ route('user.lectures.detail', [ 'id' => $lecture->id ]) }}">
                    <img src="{{ isset($lecture->thumbnail_url) ? $lecture->thumbnail_url : asset('/assets/dist/img/no-image-available.png') }}" alt="Transcend-Learning">
                  </a>
                </figure>
                <div class="detail">
                  {{-- <p class="series">シリーズタイトル　1/3</p> --}}
                  <strong class="title">{{ $lecture->name }}</strong>
                  <div class="value">
                    <time class="time">{{ date("Y/m/d h:i", strtotime($lecture['created_at'])) }}追加</time>
                  </div>
                </div>
              </div>
            <ul class="list">
              @if(count($lecture->memos))
                @foreach($lecture->memos as $memo)
                  <li>
                    <div class="memory">
                      <time><a href="/lectures/detail/{{ $lecture->id }}?time={{ $memo->time_memo }}" class="seek_to_time"
                    data-time="{{ $memo->time_memo }}">{{ $memo->time_memo }}</a></time>
                      <p>{{ $memo->comment }}</p>
                    </div>
                    <i data-id="{{ $memo->id }}" class="fas fa-times del-my-note"></i>
                  </li>
                @endforeach
              @endif
            </ul>
          </li>
        @endforeach
      @else
        <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
      @endif
    </ul>
    <!--pagination-->
    {{ $listLectures->links('layouts.pagination.user_paginator') }}
  </section>
</main>
@endsection
@section('custom_script')
    @include('user.scripts.lecture.note_lecture_script')
@endsection
