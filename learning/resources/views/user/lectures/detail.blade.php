@extends('layouts.user.default')
@section('title', 'Transcend-Learningシステム | マイリスト')
@section('content')
  <main id="content">
      <div class="section lesson_movie">
          <input type="hidden" class="embed_code" id="embed_code" value="{{ $lecture->embed_code }}">
          <input type="hidden" class="lecture_id" id="lecture_id" value="{{ $lecture->id }}">
          <input type="hidden" class="liked_number" id="liked_number" value="{{ $lecture->liked_number }}">
          <input type="hidden" class="appearance_time" id="appearance_time" value="{{ $time_seconds }}">
          <div id="player" class="movie">
              @php
                  echo $lecture->embed_code;
              @endphp
          </div>
          <div class="comment_area">
              <strong class="title">{{ __('notelecture') }}</strong>
              <ul class="list" id="list-comment">
                  @if ($lecture->memos)
                      @foreach ($lecture->memos as $key => $memo)
                          <li>
                              <time>{{ $memo->time_memo }}</time>
                              <p class="content-comment">{{ $memo->comment }}</p>
                          </li>
                      @endforeach
                  @endif

              </ul>
              <div class="send_form">
                  <input type="text" id="memo_text">
                  <div class="send" id="send_memo_lecture">
                      <button></button>
                  </div>
              </div>
          </div>
      </div>


      <section class="section lesson_detail lesson_parts mb30">
          <div class="detail">
              {{-- <p class="series">シリーズタイトル　3/3</p> --}}
              <h1 class="title">{{ trim($lecture->name) }}</h1>
              <p class="text">@php echo nl2br($lecture->explanatory_text) @endphp</p>
              {{-- <div class="option_tags">
        <span>タグ</span><span>タグ</span><span>タグ</span><span>タグ</span><span>タグ</span>
      </div> --}}
              <span class="author">{{ $lecture->university }}</span>
              <div class="value">
                  <span class="watch">{{ formatNumber($lecture->view_number) }}</span>{{ __('view') }}
                  <time class="time">{{ formatDate($lecture->created_at, 'Y/m/d H:i') }}</time>
              </div>
              <div class="detail_btn">
                  @php
                      $classIconMyList = 'far';
                      if ($lecture->in_my_list && !empty($lecture->in_my_list)) {
                          $classIconMyList = 'fas';
                      }
                      $classIconLiked = 'far';
                      if ($lecture->user_liked && !empty($lecture->user_liked)) {
                          $classIconLiked = 'fas';
                      }
                  @endphp
                  <span class="nice_btn"><i class="{{ $classIconLiked }} fa-thumbs-up"></i><span
                          class="current_liked_number">{{ formatNumber($lecture->liked_number) }}</span></span>
                  <span class="mylist_btn"><i class="{{ $classIconMyList }} fa-heart"></i>{{ __('addmylist') }}</span>
              </div>
          </div>
      </section>

      <section class="section mb30">
          <div class="contents_list">
              <h2 class="title_M">{{ __('heading') }}</h2>
              <ul>
                  @if ($lecture->headings)
                      @foreach ($lecture->headings as $headings)
                          <li>
                              <time><a href="javascript:void(0)" class="seek_to_time"
                                      data-time="{{ $headings->heading_time }}">{{ $headings->heading_time }}</a></time>
                              <p>{{ $headings->title }}</p>
                          </li>
                      @endforeach
                  @endif
              </ul>
          </div>
      </section>

      <div class="section section_margin">
          <ul class="lesson_link">
              <li><a
                      href="{{ route('user.lectures.lessonDocument', ['id' => $lecture->id]) }}">{{ __('lessonDocument') }}</a>
              </li>
              <li><a href="{{ isset($lecture->question_url) ? $lecture->question_url : '' }}"
                      class="{{ isset($lecture->question_url) ? '' : 'disabled' }}"
                      target="_blank">{{ __('question') }}</a></li>
              <li><a href="{{ isset($lecture->test_url) ? $lecture->test_url : '' }}"
                      class="{{ isset($lecture->test_url) ? '' : 'disabled' }}" target="_blank">{{ __('test') }}</a>
              </li>
              <li><a href="{{ isset($lecture->homework_url) ? $lecture->homework_url : '' }}"
                      class="{{ isset($lecture->homework_url) ? '' : 'disabled' }}"
                      target="_blank">{{ __('homework') }}</a></li>
          </ul>
      </div>

      @if ($otherLecture->count() > 0)
          <section class="bg_color_main">
              <div class="section">
                  <h2 class="title_M">{{ __('parallellesson') }}</h2>
                  <ul class="lesson_list lesson_parts lectures">
                      @foreach ($otherLecture as $other)
                          <li>
                              <a href="{{ route('user.lectures.detail', $other->id) }}">
                                  <figure class="img"><img
                                          src="{{ isset($other->thumbnail_url) ? $other->thumbnail_url : asset('/assets/dist/img/no-image-available.png') }}"
                                          alt="Transcend-Learning"></figure>
                                  <div class="detail">
                                      {{-- <p class="series">シリーズタイトル　2/3</p> --}}
                                      <strong class="title">{{ $other->name }}</strong>
                                      <p class="text">
                                          @php echo nl2br($lecture->explanatory_text) @endphp
                                      </p>
                                      <span class="author">{{ $other->university }}</span>
                                      <div class="value">
                                          <span class="watch">{{ $other->view_number }}{{ __('view') }}</span>
                                          <time class="time">{{ formatDate($other->created_at, 'Y/m/d H:i') }}</time>
                                      </div>
                                      <span class="nice">{{ formatNumber($other->liked_number) }}</span>
                                  </div>
                              </a>
                          </li>
                      @endforeach
                  </ul>
              </div>
          </section>
      @endif


      <div class="center mt30" style="padding:30px;background-color:#FF0;">
          {{ __('teststatus') }}<br><br>
          <span class="js-modal-open quiz_modal quiz_modalhover"
              data-target="quiz_modal">{{ __('questions') }}</span><br><br>
          <span class="js-modal-open cmthover" data-target="commentary_modal">{{ __('comments') }}</span>
      </div>
      <!-- modal -->
      <div id="quiz_modal" class="c-modal js-modal">
          <div class="c-modal_bg js-modal-close"></div>
          <div class="c-modal_content">
              <a class="js-modal-close c-modal_close" href=""><i class="fas fa-times"></i></a>
              <div class="modal_area modal_area_quiz">
                  <strong class="title_M">クイズタイトル</strong>
                  <p class="mb30">
                      クイズの質問。<br>
                      テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                  </p>
                  <dl>
                      <dt>◆テキスト入力</dt>
                      <dd>
                          <p class="mb10">
                              問題文。<br>
                              テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                          </p>
                          <input type="text">
                      </dd>
                  </dl>
                  <dl>
                      <dt>◆選択肢(ラジオボタン)</dt>
                      <dd>
                          <p class="mb10">
                              問題文。<br>
                              テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                          </p>
                          <div class="select">
                              <label><input type="radio" name="quiz1" checked>選択肢</label>
                              <label><input type="radio" name="quiz1">選択肢</label>
                          </div>
                      </dd>
                  </dl>
                  <dl>
                      <dt>◆選択肢(チェックボックス)</dt>
                      <dd>
                          <p class="mb10">
                              問題文。<br>
                              テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                          </p>
                          <div class="select">
                              <label><input type="checkbox" name="quiz2" checked>選択肢</label>
                              <label><input type="checkbox" name="quiz2">選択肢</label>
                          </div>
                      </dd>
                  </dl>
                  <dl>
                      <dt>◆選択肢(セレクトボックス)</dt>
                      <dd>
                          <p class="mb10">
                              問題文。<br>
                              テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                          </p>
                          <select>
                              <option>選択肢</option>
                              <option>選択肢</option>
                              <option>選択肢</option>
                              <option>選択肢</option>
                          </select>
                      </dd>
                  </dl>

                  <div class="btn_button">
                      <button>回答する</button>
                  </div>
              </div>
          </div>
      </div>
      <!-- modal -->

      <!-- modal -->
      @if ($quizzes)
          @foreach ($quizzes as $quiz)
              <div class="modal fade bd-example-modal-xl" id="quiz_modal_{{ $quiz['sort_number'] }}"
                  data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel"
                  aria-hidden="true"
                  data-answer="@if (in_array($quiz['id'], $answerUser)) {{ 'true' }}@else{{ 'false' }}@endif">
                  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                      <div class="modal-content">
                          <div class="modal-body quiz">
                              <div class="modal_area modal_area_quiz" id="modal_area_quiz">

                                  <strong class="title_M">{{ $quiz['title'] }}</strong>
                                  <p class="mb30">
                                      クイズの質問。<br>
                                      @php echo nl2br($quiz['question_sentence']) @endphp
                                  </p>
                                  <form action="{{ route('user.lectures.saveAnswer') }}" method="post"
                                      id="form-quiz-{{ $quiz['sort_number'] }}">
                                      <input type="hidden" name="lecture_id" id="lecture_id"
                                          value="{{ $lecture->id }}">
                                      <input type="hidden" name="user_id" id="user_id"
                                          value="{{ Auth::user()->id }}">
                                      <input type="hidden" name="quiz_id" id="quiz_id"
                                          value="{{ $quiz['id'] }}">
                                      @if ($quiz['type'] == INPUT)
                                          <dl>
                                              <dt>◆テキスト入力</dt>
                                              <dd>
                                                  <p class="mb10">
                                                      問題文。<br>
                                                      @php echo nl2br($quiz['remark']) @endphp
                                                  </p>
                                                  <input type="text" name="quiz_answer" class="quiz_answer">
                                              </dd>
                                          </dl>
                                      @elseif($quiz['type'] == RADIO)
                                          <dl>
                                              <dt>◆選択肢(ラジオボタン)</dt>
                                              <dd>
                                                  <p class="mb10">
                                                      問題文。<br>
                                                      @php echo nl2br($quiz['remark']) @endphp
                                                  </p>
                                                  @foreach ($quiz['quiz_values'] as $value)
                                                      <label><input type="radio" value="{{ $value['id'] }}"
                                                              name="quiz_answer"
                                                              class="quiz_answer radio">{{ $value['name'] }}</label>
                                                  @endforeach
                                              </dd>
                                          </dl>
                                      @elseif($quiz['type'] == CHECKBOX)
                                          <dl>
                                              <dt>◆選択肢(チェックボックス)</dt>
                                              <dd>
                                                  <p class="mb10">
                                                      問題文。<br>
                                                      @php echo nl2br($quiz['remark']) @endphp
                                                  </p>
                                                  @foreach ($quiz['quiz_values'] as $value)
                                                      <label><input type="checkbox" value="{{ $value['id'] }}"
                                                              name="quiz_answer[]"
                                                              class="quiz_answer">{{ $value['name'] }}</label>
                                                  @endforeach
                                              </dd>
                                          </dl>
                                      @elseif($quiz['type'] == SELECT)
                                          <dl>
                                              <dt>◆選択肢(セレクトボックス)</dt>
                                              <dd>
                                                  <p class="mb10">
                                                      問題文。<br>
                                                      @php echo nl2br($quiz['remark']) @endphp
                                                  </p>
                                                  <select name="quiz_answer" class="quiz_answer select2 narrow wrap">
                                                      <option selected disabled>選択して下さい</option>
                                                      @foreach ($quiz['quiz_values'] as $value)
                                                          <option value="{{ $value['id'] }}">{{ $value['name'] }}
                                                          </option>
                                                      @endforeach
                                                  </select>
                                              </dd>
                                          </dl>
                                      @elseif($quiz['type'] == TEXTAREA)
                                          <dl>
                                              <dt>◆テキストエリア</dt>
                                              <dd>
                                                  <p class="mb10">
                                                      問題文。<br>
                                                      @php echo nl2br($quiz['remark']) @endphp
                                                  </p>
                                                  <textarea name="quiz_answer" class="quiz_answer"></textarea>
                                              </dd>
                                          </dl>
                                      @endif
                                      {{-- <div class="alert alert-danger" style="display:none"></div> --}}
                                      <div class="btn_button">
                                          <button type="button"
                                              id="submit-quiz-answer-{{ $quiz['sort_number'] }}">回答する</button>
                                      </div>
                                  </form>
                              </div>
                          </div>
                          {{-- <div class="modal-footer">
            <button type="button" class="btn btn-primary">Understood</button>
          </div> --}}
                      </div>
                  </div>
              </div>
          @endforeach
      @endif

      <!-- modal -->
      @if ($explanations)
          @foreach ($explanations as $explain)
              <!-- Modal -->
              <div class="modal fade bd-example-modal-xl" id="commentary_modal_{{ $explain->sort_number }}"
                  data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel"
                  aria-hidden="true">
                  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body commentary">
                              <div class="modal_area modal_area_commentary">
                                  <strong class="title_M">{{ $explain->title }}</strong>
                                  <p class="mb20">
                                      @php echo nl2br($explain->content) @endphp
                                  </p>
                                  <div class="side_block">
                                      <figure class="side_left">
                                          <img
                                              src="{{ isset($explain->uploaded_path) ? $explain->uploaded_path : asset('/assets/dist/img/no-image-available.png') }}">
                                      </figure>
                                      <div class="side_right">
                                          <p>
                                              @php echo nl2br($explain->remark) @endphp
                                          </p>
                                      </div>
                                  </div>
                                  <div class="btn_button">
                                      <button type="button" data-dismiss="modal">閉じる</button>
                                  </div>
                              </div>
                          </div>
                          {{-- <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Understood</button>
            </div> --}}
                      </div>
                  </div>
              </div>
          @endforeach
      @endif
      <!-- modal -->

      <!-- modal -->
      <div id="commentary_modal" class="c-modal js-modal">
          <div class="c-modal_bg js-modal-close"></div>
          <div class="c-modal_content">
              <a class="js-modal-close c-modal_close" href=""><i class="fas fa-times"></i></a>
              <div class="modal_area modal_area_commentary">
                  <strong class="title_M">解説タイトル</strong>
                  <p class="mb20">
                      テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                  </p>
                  <div class="side_block">
                      <figure class="side_left">
                          <img src="images/sample_img_02.png">
                      </figure>
                      <div class="side_right">
                          <p>
                              テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                          </p>
                      </div>
                  </div>

                  <div class="btn btn-block center mt50"><a class="js-modal-close" href="">閉じる</a></div>
              </div>
          </div>
      </div>
      <!-- modal -->
  </main>
@endsection
@section('custom_script_ifrem')
    @if ($lecture->ifrem_code == 1)
        @include('user.scripts.lecture.youtube')
    @else
        @include('user.scripts.lecture.vimeo')
    @endif
@endsection
@section('custom_script')
  <script>
    $(function() {
        $(".mylist_btn").on("click", function() {
          var url = "{{ route('user.lectures.addOrRemoveToMyList') }}"
          var formData = new FormData();
          formData.append('lecture_id', $("#lecture_id").val());
          $.ajax({
            type: "POST",
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            headers: {
              'X-CSRF-Token': '{{ csrf_token() }}',
            },
            dataType: 'json',
            success: function(response) {
              alert(response.msg)
              if (response.result == '{{ SUCCESS }}') {
                var icon = $(".mylist_btn").find("i")
                if (icon.hasClass("far")) {
                  icon.removeClass("far")
                  icon.addClass("fas")
                } else {
                  icon.removeClass("fas")
                  icon.addClass("far")
                }
              }
            },
            error: function(response) {
              alert('error')
            }
          })


        })

        $(".nice_btn").on("click", function() {
          var url = "{{ route('user.lectures.likeLecture') }}"
          var formData = new FormData();
          formData.append('lecture_id', $("#lecture_id").val());
          $.ajax({
            type: "POST",
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            headers: {
              'X-CSRF-Token': '{{ csrf_token() }}',
            },
            dataType: 'json',
            success: function(response) {
              if (response.result == '{{ SUCCESS }}') {
                var currentLikedNumber = parseInt($("#liked_number").val())
                if (response.remove == true) {
                  currentLikedNumber = currentLikedNumber - 1;
                } else {
                  currentLikedNumber = currentLikedNumber + 1;
                }
                $("#liked_number").val(currentLikedNumber)
                $(".current_liked_number").html(formatNumber(currentLikedNumber))
                var icon = $(".nice_btn").find("i")
                if (icon.hasClass("far")) {
                  icon.removeClass("far")
                  icon.addClass("fas")
                } else {
                  icon.removeClass("fas")
                  icon.addClass("far")
                }
              }
            },
            error: function(response) {
              alert('error')
            }
          })
        })

        function formatNumber(num) {
          var str = num.toString().replace("$", ""),
            parts = false,
            output = [],
            i = 1,
            formatted = null;
          if (str.indexOf(".") > 0) {
            parts = str.split(".");
            str = parts[0];
          }
          str = str.split("").reverse();
          for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != ",") {
              output.push(str[j]);
              if (i % 3 == 0 && j < (len - 1)) {
                output.push(",");
              }
              i++;
            }
          }
          formatted = output.reverse().join("");
          return (formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
        };
    })
  </script>
@endsection
