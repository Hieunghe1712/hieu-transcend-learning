@extends('layouts.user.default')
@section('title', 'Transcend-Learningシステム | 講義資料')
@section('content')
<main id="content">
  <section class="section">
    @if(count($listDocuments))
      @if($lecture)
        <h1 class="title_L">{{ $lecture->name }} {{__('lessonDocument')}}</h1>
        <p class="mb30">@php echo nl2br($lecture->explanatory_text) @endphp</p>
        <div class="btn right mb30 download-documents">
          <a href="{{ route('user.lectures.downloadAllDocuments', ['id' => $lecture->id, 'download'=>'zip']) }}">{{__('dowloadall')}}</a>
        </div>
      @endif
      <ul class="document_list">
        @if($listDocuments)
          @foreach($listDocuments as $document)
          <li>
            <div>
              <time>{{ date("Y/m/d", strtotime($document['created_at'])) }}</time>
              <p>{{ $document->name }}</p>
            </div>
            @php
              $file_name = substr($document->uploaded_path , strrpos($document->uploaded_path , '/') + 1);
            @endphp
            <a href="{{ route('user.lectures.downloadDocument', ['file_name' => $file_name, 'id' => $document->id ]) }}"><i class="fas fa-download"></i></a>
          </li>
          @endforeach
        @else
          <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
        @endif
      </ul>
    @else
      <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
    @endif  
    <!--pagination-->
    {{ $listDocuments->links('layouts.pagination.user_paginator') }}
  </section>
</main>
@endsection
@section('custom_script')
    @include('user.scripts.lecture.note_lecture_script')
@endsection
