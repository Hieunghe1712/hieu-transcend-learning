@extends('layouts.user.default')
@section('title', 'Transcend-Learningシステム | マイリスト')
@section('content')
<main id="content">
  <section class="section">
    @include('layouts.user.flash')
    <h1 class="title_L">{{__('mylist')}}</h1>
    <ul class="lectures">
      @if(count($listLectures))
        @foreach($listLectures as $lecture)
          <li>
            <div class="row my_list">
              <figure class="img col-3 img-lecture">
                <a href="{{ route('user.lectures.detail', [ 'id' => $lecture->id ]) }}">
                  <img src="{{ isset($lecture->thumbnail_url) ? $lecture->thumbnail_url : asset('/assets/dist/img/no-image-available.png') }}" alt="Transcend-Learning">
                </a>
              </figure>
              <div class="detail col-7">
                {{-- <p class="series">シリーズタイトル　1/3</p> --}}
                <strong class="title">{{ $lecture->name }}</strong>
                <p class="text">@php echo nl2br($lecture->explanatory_text) @endphp</p>
                <span class="author">{{ $lecture->university }}</span>
                <div class="value">
                  <span class="watch">{{ $lecture->view_number }}{{__('view')}}</span>
                  <time class="time">{{ $lecture->created_at ? date("Y.m.d h:s", strtotime($lecture->created_at)) : '' }}</time>
                </div>
                <span class="nice">{{ $lecture->liked_number ?? 0 }}</span>
              </div>
              <i data-id="{{ $lecture->myListId }}" class="fas fa-times del-my-list col-2"></i>
            </div>
          </li>
        @endforeach
      @else
        <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
      @endif
    <ul>
    <!--pagination-->
    {{ $listLectures->links('layouts.pagination.user_paginator') }}
  </section>
</main>
@endsection
@section('custom_script')
    @include('user.scripts.lecture.my_lecture_script')
@endsection
