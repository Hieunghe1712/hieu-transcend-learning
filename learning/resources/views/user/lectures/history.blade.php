@extends('layouts.user.default')
@section('title', 'Transcend-Learningシステム | 視聴履歴')
@section('content')
<main id="content">
  <section class="section">
    <h1 class="title_L">{{__('viewHistory')}}</h1>
    <section class="mb50">
      <ul class="view_list lesson_parts lectures">
        @if(count($listLectures))
          @php
            $previous = '';
          @endphp
          @foreach($listLectures as $lecture)
          @php
            $current = date("Y年m月d日", strtotime($lecture['day']));
          @endphp
          @if($previous != $current)
            <h2 class="title_M">{{ date("Y年m月d日", strtotime($lecture['day'])) }}</h2>
          @endif
            <li>
              <a href="{{ route('user.lectures.detail', [ 'id' => $lecture->id ]) }}" class="row">
                <figure class="img col-md-3">
                    <img src="{{ isset($lecture->thumbnail_url) ? $lecture->thumbnail_url : asset('/assets/dist/img/no-image-available.png') }}" alt="Transcend-Learning">
                </figure>
                <div class="detail col-md-8">
                  {{-- <p class="series">シリーズタイトル　1/3</p> --}}
                  <strong class="title">{{ $lecture->name }}</strong>
                  <p class="text">@php echo nl2br($lecture->explanatory_text) @endphp</p>
                  <span class="author">{{ $lecture->university }}</span>
                  <div class="value">
                    <span class="watch">{{ $lecture->view_number }}{{__('view')}}</span>
                    <time class="time">{{ $lecture->created_at ? date("Y.m.d h:s", strtotime($lecture->created_at)) : '' }}</time>
                  </div>
                  <span class="nice">{{ $lecture->liked_number ?? 0 }}</span>
                </div>
              </a>
            </li>
          @php
            $previous = $current;
          @endphp
          @endforeach
        @else
          <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
        @endif
      <ul>
    </section>
    <!--pagination-->
    {{ $listLectures->links('layouts.pagination.user_paginator') }}
  </section>
</main>
@endsection
