@extends('layouts.user.default')
@section('title', 'Transcend-Learningシステム | 講義検索')
@section('content')
    <main id="content" class="index_page">
        <section class="bg_color_main section_margin">
            <div class="section">
                <h1 class="title_L center">{{__('searchlesson')}}</h1>
                <form method="get" action="{{ route('user.lectures.search') }}" accept-charset="UTF-8">
                    <div class="search_area">
                        <input type="text" data-role="tagsinput" placeholder="{{__('content')}}" id="name" name="name"
                            value="{{ app('request')->input('name') }}">
                        {{-- <div class="law">
            <label><input type="radio" name="search" checked>AND</label>
            <label><input type="radio" name="search">OR</label>
          </div> --}}
                        {{-- <div class="tag_modal_btn">
            <span class="js-modal-open" data-target="tags_modal">タグから調べる</span>
          </div> --}}
                    </div>
                    <div class="btn_button">
                        <button type="submit">検索</button>
                    </div>
                </form>
            </div>
        </section>
        <!-- modal -->
        {{-- <div id="tags_modal" class="c-modal js-modal">
      <div class="c-modal_bg js-modal-close"></div>
      <div class="c-modal_content">
        <a class="js-modal-close c-modal_close" href=""><i class="fas fa-times"></i></a>
        <div class="modal_area modal_area_tag">
          <div class="select_tag">
            <span class="title_M">選択中のタグ</span>
            <input type="text" data-role="tagsinput" value="タグ,検索ワード">
          </div>
          <div class="tags_list">
            <dl>
              <dt><label><input type="checkbox" name="tags">タグ</label></dt>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
            </dl>
            <dl>
              <dt><label><input type="checkbox" name="tags">タグ</label></dt>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
            </dl>
            <dl>
              <dt><label><input type="checkbox" name="tags">タグ</label></dt>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
            </dl>
            <dl>
              <dt><label><input type="checkbox" name="tags">タグ</label></dt>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
              <dd><label><input type="checkbox" name="tags">タグ</label></dd>
            </dl>
          </div>
        </div>
      </div>
    </div> --}}
        <!-- modal -->

        <section class="section">
            <h1 class="title_L center">{{__('availablecourses')}}</h1>
            <ul class="lesson_list lesson_parts lectures">
            @if(count($listLectures))
                @foreach($listLectures as $lecture)
                <li>
                    <a href="{{ route('user.lectures.detail', [ 'id' => $lecture->id ]) }}">
                        @php
                        $avatar = '/assets/dist/img/no-image-available.png';
                        if (isset( $lecture->thumbnail_url) &&  $lecture->thumbnail_url != '') {
                          $avatar= $lecture->thumbnail_url;
                        }
                      @endphp
                        <figure class="img"><img src="{{ url($avatar) }}" alt="Transcend-Learning"></figure>
                        <div class="detail">
                            {{-- <p class="series">シリーズタイトル　1/3</p> --}}
                            <strong class="title">{{ $lecture->name }}</strong>
                            <p class="text">@php echo nl2br($lecture->explanatory_text) @endphp</p>
                            <span class="author">{{ $lecture->university }}</span>
                            <div class="value">
                                <span class="watch">{{ $lecture->view_number }}{{__('view')}}</span>
                                <time class="time">{{ $lecture->created_at ? date("Y.m.d h:s", strtotime($lecture->created_at)) : '' }}</time>
                              </div>
                              <span class="nice">{{ $lecture->liked_number ?? 0 }}</span>
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
            @else
            <tr class="text-center"><td colspan="6">{{ NO_DATA_FOUND }}</td></tr>
          @endif
          {{ $listLectures->links('layouts.pagination.user_paginator') }}
        </section>

    </main>
@endsection
