<script>
   $(function () {
        $('#profile-img').change(function(e) {
            var image = $(this).attr('name');
            getAjaxUploadAvatar(e.target.files, image);
        });

        //upload file
        $(".upload-avatar-profile").on('click', function(e) {
            $('#profile-img').click();
        });

    });

    function getAjaxUploadAvatar (files, image) {
        var formData = new FormData();
        formData.append(image , files[0]);
        var urlUp = "{{ Route('user.profile.uploadAvatar', '') }}" + "/" + image;
        $.ajax({
            type: 'POST',
            url: urlUp,
            data: formData,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            success: function(response) {
                var data = response.result;
                if (data.success == '{{ CODE_AJAX_SUCCESS }}') {
                    if (data.fileName) {
                        $('span.file-name').text(data.fileName);
                        $('#profile-image').val(data.fileName);
                        var src = "{{ AVATAR_STUDENT_SAVE_TMP_JS }}" + data.fileName;
                        $("#avatar-profile").attr("src", src);
                    }
                } else {
                    alert(data.msg);
                }
            },
            error: function(error) {
                alert('{{ Config::get("message.common_msg.upload_avatar_file_type") }}');
            }
        });
    }
</script>
