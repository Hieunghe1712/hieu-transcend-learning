<script>
    var player;

    var iframe = $("#player").find("iframe");
    var src = iframe.attr("src");
    var url = src.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);

    var videoID = (url[2] !== undefined) ? url[2].split(/[^0-9a-z_\-]/i)[0] : url[0];

    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            height: '390',
            width: '640',
            videoId: videoID,
            playerVars: {
                'playsinline': 1
            },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange,
            }
        });

    function onPlayerReady(event) {
        event.target.playVideo();
    }
    var done = false;

    var myTimer; 

    function onPlayerStateChange(event){
        if(event.data == 1) { // playing
            myTimer = setInterval(function(){ 
                var timequiz = player.getCurrentTime();
                
                $(function() { 
                    dataSecond = timequiz;
                    dataSecond = parseInt(dataSecond, 10);
                    var timeParam = $("#appearance_time").val();
                        timeParam = JSON.parse(timeParam);
                    $.each(timeParam, function (sort_number, time) {
                        if(dataSecond == time) {
                            var dataAnswer = $('#quiz_modal_'+sort_number).attr('data-answer').trim();
                            
                            if(dataAnswer == "false") {
                                pauseVideo();
                                $('#quiz_modal_'+sort_number).attr('data-answer', 'true');
                                $('#quiz_modal_'+sort_number).modal({
                                    backdrop: 'static',
                                    keyboard: false,
                                    show: true
                                })
                                $("#submit-quiz-answer-"+sort_number).on("click", function(e) {
                                    e.preventDefault();                        
                                    saveAnswer(sort_number, dataSecond)
                                })

                            }
                        }
                    });
                })
                
            }, 1000);
        }
        else { // not playing
            clearInterval(myTimer);
        }
    }
        function saveAnswer(sort_number, dataSecond) {
            var url = $('#form-quiz-'+sort_number).attr('action');
            var form = $('#form-quiz-'+sort_number);
            var formData = $(form).serialize();
            $.ajax({
                url:url,
                method:"POST",
                dataType:"JSON",
                headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
                },
                data: formData,
                success:function(result)
                {
                    if(result.errors)
                    {
                    // $('.alert-danger').html('');
                    $.each(result.errors, function(key, value){
                        // $('.alert-danger').show();
                        // $('.alert-danger').append('<li>'+value+'</li>');
                        alert(value);
                    });
                    }
                    else
                    {
                        // $('.alert-danger').hide();
                        $('#quiz_modal_'+sort_number).modal('hide')
                        setTimeout(function () {
                        $('#commentary_modal_'+sort_number).modal({
                            show: true
                        })
                        }, 500)

                        playVideo()
                    }
                }
            })
        }

        function stopVideo() {
            player.stopVideo();
        }

        function pauseVideo() {
            player.pauseVideo();
        }

        function playVideo() {
            player.playVideo()
        }
    }

    $(function() {
        $(document).on('click', '.seek_to_time', function() {
            var time = $(this).attr("data-time");
            var convertTime = time.split(':'); // split it at the colons

            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            var secondsSeek = (+convertTime[0]) * 60 * 60 + (+convertTime[1]) * 60 + (+convertTime[2]);
            // player.playVideo();
            player.seekTo(secondsSeek, true);
        })

        $('#memo_text').keypress(function(e) {
            var key = e.which;
            if (key == 13) {
                $('#send_memo_lecture').trigger('click');
            }
        });
        

        $("#send_memo_lecture").on("click", function() {

            var memo = $("#memo_text").val().trim()
            if (memo == "") {
                var errorText = "{{ 'メモ' . Config::get('message.validation_common_msg.input_err') }}"
            } else {
                addMemoText(memo)
            }
        })

        function addMemoText(memo) { 
            var time = player.getCurrentTime();
            var sec_num = parseInt(time, 10); 
            var hours   = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60); 
            if (hours < 10) {
                hours = '0' + hours;
            }
                
            if (minutes < 10) {
                minutes = '0' + minutes;
            }
                
            if (seconds < 10) {
                seconds = '0' + seconds;
            }
                
            seconds = hours + ':' + minutes + ':' + seconds;

            var url = "{{ route('user.lectures.addMemo') }}"
            var formData = new FormData();
            var timeMemo = seconds
            formData.append('comment', memo);
            formData.append('lesson_id', $("#lecture_id").val());
            formData.append('time_memo', timeMemo);
            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                dataType: 'json',
                success: function(response) {
                    // alert(response.msg)
                    if (response.result == '{{ SUCCESS }}') {
                        var data = response.data
                        var html = '<li>'
                        html += "<time>" + data.time_memo + "</time>"
                        html += "<p class='content-comment'>" + data.comment + "</p>"
                        html += "</li>"
                        $("#list-comment").append(html);
                        $("#memo_text").val('')
                    }
                },
                error: function(response) {
                    alert('error')
                }
            })

        }
    })
    
</script>