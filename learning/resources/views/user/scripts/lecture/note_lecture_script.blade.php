<script>
    $('.del-my-note').on('click', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var url = '{{ Route("user.lectures.deleteMemo", "") }}' + '/' + id;
        var confirmStr = "講義ノート" + "{{ Config::get('message.common_msg.ask_before_delete') }}";
        if (confirm(confirmStr)) {
            window.location = url;
        }
        return false;
    });
</script>
