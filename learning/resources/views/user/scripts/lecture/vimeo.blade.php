<script>
    $(function() {
        $('.select2').select2({ width: '100%' });

        var iframe = $("#player").find("iframe");
        var pause = false;

        const player = new Vimeo.Player(iframe);
        var searchParams = new URLSearchParams(window.location.search)
        if (searchParams.has('time')) {
          var timeParam = searchParams.get('time')
          var timeConvert = timeParam.split(':');

          var secondsPlay = (+timeConvert[0]) * 60 * 60 + (+timeConvert[1]) * 60 + (+timeConvert[2]);
          player.setMuted(true)
          player.setCurrentTime(secondsPlay).then(function(seconds) {
            player.play()
          }).catch(function(error) {
            switch (error.name) {
              case 'RangeError':
                alert("Out of range")
                break;
              default:
                // some other error occurred
                break;
            }
          });
        }

        player.on('play', function() {
          pause = false;
        });

        player.on('timeupdate', function(data) {
          var dataSecond = parseInt(data.seconds);
          var timeParam = $("#appearance_time").val()
          timeParam = JSON.parse(timeParam)
          $.each(timeParam, function (sort_number, time) {
            if(dataSecond == time) {
              var dataAnswer = $('#quiz_modal_'+sort_number).attr('data-answer').trim();
              if(dataAnswer == "false" && pause == false) {
                player.pause().then(function() {
                  pause = true;
                  $('#quiz_modal_'+sort_number).attr('data-answer', 'true');
                  $('#quiz_modal_'+sort_number).modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                  })
                  $("#submit-quiz-answer-"+sort_number).on("click", function(e) {
                        e.preventDefault();                        
                        saveAnswer(sort_number, dataSecond)
                  })

                }).catch(function(error) {
                  switch (error.name) {
                    case 'PasswordError':
                    // the video is password-protected and the viewer needs to enter the
                    // password first
                    break;

                    case 'PrivacyError':
                    // the video is private
                    break;

                    default:
                    // some other error occurred
                    break;
                  }
                });
              }
            }
          });
        });


        function saveAnswer(sort_number, dataSecond) {
            var url = $('#form-quiz-'+sort_number).attr('action');
            var form = $('#form-quiz-'+sort_number);
            var formData = $(form).serialize();
            $.ajax({
                url:url,
                method:"POST",
                dataType:"JSON",
                headers: {
                  'X-CSRF-Token': '{{ csrf_token() }}'
                },
                data: formData,
                success:function(result)
                {
                    if(result.errors)
                    {
                      // $('.alert-danger').html('');
                      $.each(result.errors, function(key, value){
                          // $('.alert-danger').show();
                          // $('.alert-danger').append('<li>'+value+'</li>');
                          alert(value);
                      });
                    }
                    else
                    {
                        // $('.alert-danger').hide();
                        $('#quiz_modal_'+sort_number).modal('hide')
                        setTimeout(function () {
                        $('#commentary_modal_'+sort_number).modal({
                            show: true
                          })
                        }, 500)

                        player.setCurrentTime(dataSecond + 0.9)
                    }
                }
            })
        }

        $('#memo_text').keypress(function(e) {
            var key = e.which;
            if (key == 13) {
                $('#send_memo_lecture').trigger('click');
            }
        });

        $("#send_memo_lecture").on("click", function() {

            var memo = $("#memo_text").val().trim()
            if (memo == "") {
                var errorText = "{{ 'メモ' . Config::get('message.validation_common_msg.input_err') }}"
            } else {
                addMemoText(memo)
            }
        })

        function addMemoText(memo) {
            player.getCurrentTime().then(function(seconds) {
                var url = "{{ route('user.lectures.addMemo') }}"
                var formData = new FormData();
                var timeMemo = seconds
                formData.append('comment', memo);
                formData.append('lesson_id', $("#lecture_id").val());
                formData.append('time_memo', timeMemo);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    headers: {
                        'X-CSRF-Token': '{{ csrf_token() }}',
                    },
                    dataType: 'json',
                    success: function(response) {
                        // alert(response.msg)
                        if (response.result == '{{ SUCCESS }}') {
                            var data = response.data
                            var html = '<li>'
                            html += "<time>" + data.time_memo + "</time>"
                            html += "<p class='content-comment'>" + data.comment + "</p>"
                            html += "</li>"
                            $("#list-comment").append(html);
                            $("#memo_text").val('')
                        }
                    },
                    error: function(response) {
                        alert('error')
                    }
                })
            }).catch(function(error) {
                alert(error)
            });

        }

        $(".seek_to_time").on("click", function() {
            var time = $(this).attr("data-time");
            var convertTime = time.split(':'); // split it at the colons

            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            var secondsSeek = (+convertTime[0]) * 60 * 60 + (+convertTime[1]) * 60 + (+convertTime[2]);

            player.setCurrentTime(secondsSeek).then(function(seconds) {}).catch(function(error) {
                switch (error.name) {
                    case 'RangeError':
                        alert("Out of range")
                        break;

                    default:
                        // some other error occurred
                        break;
                }
            });
        })
    });
</script>
