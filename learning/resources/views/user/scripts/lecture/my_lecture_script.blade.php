<script>
    $('.del-my-list').on('click', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var url = '{{ Route("user.lectures.deleteMyList", "") }}' + '/' + id;
        var confirmStr = "好きな講義" + "{{ Config::get('message.common_msg.ask_before_delete') }}";
        if (confirm(confirmStr)) {
            window.location = url;
        }
        return false;
    });
</script>
