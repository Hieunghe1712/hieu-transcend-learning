$(function () {
    'use strict';

    //Date picker
    $('.datepicker').datetimepicker({
        locale: 'ja',
        format: 'L',
        allowInputToggle: true
    });
    
    //Date & time picker
    $('.datetimepicker').datetimepicker({
        locale: 'ja',
        allowInputToggle: true,
        sideBySide: true,
        icons: { time: 'far fa-clock' }
    });

    //increase z-index for open multi modal
    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1050 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex).css('padding-right', '17px');
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
    
    //back to top
    var btn = $('#back-to-top');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, '300');
    });
    $('select').change(function() {
        if ($(this).children('option:first-child').is(':selected')) {
          $(this).addClass('placeholder');
        } else {
         $(this).removeClass('placeholder');
        }
    });
});
