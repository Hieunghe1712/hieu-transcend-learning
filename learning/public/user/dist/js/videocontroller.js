// ある秒数になったら質問を出す 5秒,60秒,80秒
var videoElem = document.getElementById("video");
var prevTime = [0.0, 0.0]; // timeupdateが発火したときのcurrentTime 2番目に最近の値と1番最近の値
var isSeeking = false;
var isMouseDown = false;
document.getElementById("user_agent").innerText=navigator.userAgent;
const isApple = navigator.userAgent.indexOf("iPhone") >= 0 ||
	navigator.userAgent.indexOf("iPad") >= 0 ||
	navigator.userAgent.indexOf("iPod") >= 0 ||
	navigator.userAgent.indexOf("Macintosh") >= 0;

function f_5(e) {
	if (videoElem.currentTime < 5 || videoElem.currentTime >= 6) {
		return;
	}
	videoElem.pause();
	exitFullScreen();

	$("#modal01").fadeIn(300);
	videoElem.removeEventListener("timeupdate", f_5, false);
}

function f_60(e) {
	if (videoElem.currentTime < 60 || videoElem.currentTime >= 61) {
		return;
	}
	videoElem.pause();
	exitFullScreen();

	$("#modal01").fadeIn(300);
	videoElem.removeEventListener("timeupdate", f_60, false);
}

function f_314(e) {
	if (videoElem.currentTime < 314 || videoElem.currentTime >= 315) {
		return;
	}
	videoElem.pause();
	exitFullScreen();

	$("#modal01").fadeIn(300);
	videoElem.removeEventListener("timeupdate", f_314, false);
}

function f_80(e) {
	if (videoElem.currentTime < 80 || videoElem.currentTime >= 81) {
		return;
	}
	videoElem.pause();
	exitFullScreen();

	$("#modal02").fadeIn(300);
	videoElem.removeEventListener("timeupdate", f_80, false);
}

actions = [
	// { time: 5, modalId: "modal01_form", function: f_5 },
//	{ time: 60, modalId: "modal01_form", function: f_60 },
//	{ time: 80, modalId: "modal02_form", function: f_80 },
	{ time: 314, modalId: "modal01_form", function: f_314 },
];

// シークできる時間の最大値
// 回答されていない問題があるときには動画の再生時間もそれ以降には進めないようにするためのもの
function getMaxTime() {
	maxTime = videoElem.duration;
	for (const action of actions) {
		if (!$(`#${action.modalId}`)[0].checkValidity()) {
			maxTime = Math.min(maxTime, action.time);
		}
	}
	return maxTime;
}

times = [];

function pushTime(time) {
	times.push(time);
}

function popValidTime(maxTime) {
	t = maxTime;
	while(times.length > 0) {
		tt = times.pop();
		if (tt <= getMaxTime()) {
			t = tt;
			break;
		}
	}
	return t;
}

// 問題を出すイベントを(再)登録する
function init_actions() {
	seeked_time = videoElem.currentTime;
	for (const action of actions) {
		videoElem.removeEventListener("timeupdate", action.function, false);
		if (action.time > seeked_time) {
			videoElem.addEventListener("timeupdate", action.function, false);
		}
	}
}

videoElem.addEventListener('mousedown', () => {
	// console.log("event 'mousedown'");
	isMouseDown = true;
});

videoElem.addEventListener('mouseup', () => {
	// console.log("event 'mouseup'");
	isMouseDown = false;
});

// timeupdateが発火したときのcurrentTimeを2つ記憶しておく
// 再生しているときにシークバーをいじるとtimeupdateが一瞬で2回発火するので2つ記憶が要る
// https://python5.com/q/rrfmudkg
videoElem.addEventListener('timeupdate', () => {
	// console.log("event 'timeupdate'");
	// console.log(prevTime, videoElem.currentTime);
	if (isSeeking) return;
	prevTime[0] = prevTime[1];
	t = videoElem.currentTime;
	prevTime[1] = t;
	pushTime(t);
});

videoElem.addEventListener('seeking', () => {
	// console.log("event 'seeking'");
	// console.log(isMouseDown, prevTime, videoElem.currentTime);
	isSeeking = true;
});

// 未回答のフォームがあるのに先に進もうとしたら引き止める
videoElem.addEventListener('seeked', (event) => {
	event.stopPropagation();
	event.preventDefault();
	// console.log("event 'seeked'");
	// console.log(isMouseDown, prevTime, videoElem.currentTime);
	maxTime = getMaxTime();
	if (videoElem.currentTime > maxTime) {
		videoElem.pause();
		if (isApple) {
			// Macではクリック押しっぱ中にalertを出すと挙動がおかしくなった
			if (!isMouseDown) {
				alert("未回答の質問フォームがあります");
				// iOSのcurrentTimeがおかしいのを回避する0秒setTimeout
				// https://lab.sonicmoov.com/development/iphone-app-dev/ios-html5-audio-video-currenttime/
				setTimeout(function () {
					videoElem.currentTime = popValidTime(maxTime);
				}, 0);
				prevTime[1] = prevTime[0];
			}
		}
		else {
			alert("未回答の質問フォームがあります");
			videoElem.currentTime = popValidTime(maxTime);
			prevTime[1] = prevTime[0];
		}
		// console.log("currentTime updated");
		// console.log(prevTime, videoElem.currentTime);
	}
	else {
		prevTime[0] = prevTime[1];
		t = videoElem.currentTime;
		prevTime[1] = t;
		pushTime(t);
	}
	// console.log(prevTime, videoElem.currentTime);
	init_actions();
	isSeeking = false;
});

// currentTimeに代入する
function setCurrentTime(time) {
	// console.log("setCurrentTime");
	if (isApple) {
		// iOSのcurrentTimeがおかしいのを回避する0秒setTimeout
		// https://lab.sonicmoov.com/development/iphone-app-dev/ios-html5-audio-video-currenttime/
		setTimeout(function () {
			videoElem.currentTime = time;
		}, 0);
	}
	else {
		videoElem.currentTime = time;
	}
	// currentTimeを代入するとtimeupdateが発火するがprevTimeは更新したくないのでisSeekingをtrueにする
	// すぐseekedも発火するのですぐisSeekingはfalseになる
	isSeeking = true;
}

function exitFullScreen() {
	// フルスクリーンかどうかの判定もする
	// https://blog.tsukumijima.net/article/javascript-fullscreen-decision/
	if ((document.FullscreenElement !== undefined && document.FullscreenElement !== null) ||
	    (document.webkitIsFullScreen) ||
		(document.webkitCurrentFullScreenElement !== undefined && document.webkitCurrentFullScreenElement !== null && document.webkitCurrentFullScreenElement == videoElem) ||
		(document.webkitFullscreenElement !== undefined && document.webkitFullscreenElement !== null) ||
		(document.msFullscreenElement !== undefined && document.msFullscreenElement !== null)) {
			console.log("enter");
		// 環境によって違うので
		// https://nocebo.jp/html5_video_fullscreen/
		if (!!document.exitFullscreen) {
			document.exitFullscreen();
		} else if (!!document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (!!document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (!!document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		} else if (!!document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		} else if (!!document.msExitFullscreen) {
			document.msExitFullscreen();
		} else {
			console.log("undefined");
		}
	} else {
		console.log("not enter");
		console.log(document.FullscreenElement);
		console.log(document.webkitIsFullScreen);
		console.log(document.webkitFullscreenElement);
		console.log(document.msFullscreenElement);
	}
	// if (!!document.exitFullscreen) {
	// 	document.exitFullscreen();
	// } else if (!!document.cancelFullScreen) {
	// 	document.cancelFullScreen();
	// } else if (!!document.mozCancelFullScreen) {
	// 	document.mozCancelFullScreen();
	// } else if (!!document.webkitExitFullscreen) {
	// 	document.webkitExitFullscreen();
	// } else if (!!document.webkitCancelFullScreen) {
	// 	document.webkitCancelFullScreen();
	// } else if (!!document.msExitFullscreen) {
	// 	document.msExitFullscreen();
	// } else {
	// 	console.log("undefined");
	// }
}



$(function() {
	init_actions();
});

// function requestFullScreen() {
// 	if (!!videoElem.requestFullScreen) {
// 		videoElem.requestFullScreen();
// 	} else if (!!videoElem.webkitRequestFullScreen) {
// 		videoElem.webkitRequestFullScreen();
// 	} else if (!!videoElem.webkitEnterFullscreen) {
// 		videoElem.webkitEnterFullscreen();
// 	} else if (!!videoElem.mozRequestFullScreen) {
// 		videoElem.mozRequestFullScreen();
// 	} else if (!!videoElem.msRequestFullscreen) {
// 		videoElem.msRequestFullscreen();
// 	}
// }
