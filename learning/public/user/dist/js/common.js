/* SP_menu */
$(function(){
  $('#header .fa-user').click(function(){
    $('#header .menu').toggleClass('open');
    $(this).toggleClass('active');
  });
});

/* 講義メモAccordion */
$(function(){
    $('.memo_list .detail').each(function(){
        $(this).on('click',function(){
            $(this).toggleClass('open');
            $(this).parents().children(".list").slideToggle();
            return false;
        });
    });
});



function submit_modal(form_id, button_id_to_hide = null) {
	if (!$(`#${form_id}`)[0].reportValidity()) {
		return false;
	}
	if (button_id_to_hide !== null) {
		$(`#${button_id_to_hide}`).fadeOut(0);
		$('.popup-re').fadeIn(0);
	}
	return true;
}

function modal_exit() {
	$('.js-modal').fadeOut(300);
	let e_video = $('#video')[0];
	if (e_video) {
		let remain_seconds = e_video.duration - e_video.currentTime;
		if (remain_seconds >= 1) {
			e_video.play();
		}
	} 
	// $('#video')[0].play();
}

$(function () {
	/* モーダル */
	// ウィンドウを開く
	$('.js-modal-open').each(function () {
		$(this).on('click', function () {
			var target = $(this).data('target');
			var modal = document.getElementById(target);
			$(modal).fadeIn(300);
			return false;
		});
	});
	// ウィンドウを閉じる
	$('.js-modal-close,.popup-btn .close').on('click', function () {
		$('.js-modal').fadeOut(300);
		let e_video = $('#video')[0];
		if (e_video) {
			let remain_seconds = e_video.duration - e_video.currentTime;
			if (remain_seconds >= 1) {
				e_video.play();
			}
		} 
		// $('#video')[0].play();
		return false;
	});

	// 回答を表示  // これをここでやられるとformがsubmitされてしまうので困る
	// $('.popup-an .next').on('click', function () {
	// 	$(this).fadeOut(0);
	// 	$('.popup-re').fadeIn(0);
	// 	return false;
	// });

	/* メニュー */
	// 開く
	$('.menu_open').click(function () {
		$('.header .account .menu').fadeIn(300);
		$("body").addClass("open");
	});

	// 閉じる
	$(document).on('click', function (e) {
		if (!$(e.target).closest('.header .account .menu,.menu_open').length) {
			$('.header .account .menu').fadeOut(300);
		}
	});

  /* レビューポップ */
	$('.review_pop').click(function () {
		$(this).children('.review_pop p').fadeIn(300);
		$("body").addClass("open");
	});
	$(document).on('click', function (e) {
		if (!$(e.target).closest('.review_icon').length) {
			$('.review_pop p').fadeOut(300);
		}
	});

});
