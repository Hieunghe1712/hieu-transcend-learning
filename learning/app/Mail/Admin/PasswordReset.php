<?php
namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Admin;

class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Admin
     */
    protected $admin;

    /**
     * @var string
     */
    protected $token;

    /**
     * PasswordReset constructor.
     * @param  Admin $admin
     * @param  $token
     */
    public function __construct($token, $admin)
    {
        $this->token = $token;
        $this->admin  = $admin;
    }


    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        $subject = '[trancend_learning] password request';
        return $this->view('admin.auth.email_password_reset', [
            'admin'  => $this->admin,
            'token' => $this->token,
        ])->subject($subject);
    }
}
