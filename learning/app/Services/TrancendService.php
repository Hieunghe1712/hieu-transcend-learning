<?php

namespace App\Services;

use Illuminate\Support\Facades\Config;

class TrancendService
{
    /**
     * Upload img
     *
     * @param $request,
     * @param $img
     * @return json $result
     */
    public function uploadAvatar($request, $img, $store)
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        if ($request->isMethod('post')) {
            $data = $request->all();
            $dirTmp = $store;
            if (!is_dir($dirTmp)) {
                mkdir($dirTmp, 0755, true);
            }
            if ($request->file($img) != null) {
                $file = $request->file($img);
                $originalName = $file->getClientOriginalName();
                $ext = strtolower(pathinfo($originalName, PATHINFO_EXTENSION));
                if (isset($request->validator) && $request->validator->fails()) {
                    $result['success'] = CODE_AJAX_ERROR;
                    $result['msg'] = $request->validator->errors()->first();
                } elseif (!in_array($ext, Config::get('const.array_extension_file_avatar'))) {
                    $result['success'] = CODE_AJAX_ERROR;
                    $result['msg'] = Config::get('message.common_msg.upload_avatar_file_type');
                } else {
                    do {
                        $nameFile = date('YmdHis') . "_" . rand(10000, 99999) . "." . $ext; // create random name for file
                    } while (file_exists($dirTmp . $nameFile));
                    if ($nameFile) {
                        $file->move($dirTmp, $nameFile);
                        $result['success'] = CODE_AJAX_SUCCESS;
                        $result['fileName'] = $nameFile;
                    } else {
                        $result['success'] = CODE_AJAX_ERROR;
                        $result['msg'] = sprintf(__(Config::get('message.common_msg.upload_max_file_size')), '2mb');
                    }
                }
            }
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }

        return response()->json(['result' => $result]);
    }

    public function uploadFile($request, $folder, $folderJS, $checkImage)
    {
        $result['success'] = CODE_AJAX_SUCCESS;
        $result['msg'] = '';
        $data = $request->all();
        if (!is_dir($folder)) {
            mkdir($folder, 0755, true);
        }
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $originalName = $file->getClientOriginalName();
            $result['name'] = $originalName;
            $ext = strtolower(pathinfo($originalName, PATHINFO_EXTENSION));

            $check = true;
            if ($checkImage) {
                if (!in_array($ext, Config::get('const.array_extension_file_avatar'))) {
                    $check = false;
                    $result['success'] = CODE_AJAX_ERROR;
                    $result['msg'] = Config::get('message.common_msg.upload_avatar_file_type');
                }
            }

            if ($check) {
                if (isset($request->validator) && $request->validator->fails()) {
                    $result['success'] = CODE_AJAX_ERROR;
                    $result['msg'] = $request->validator->errors()->first();
                } else {
                    do {
                        $nameFile = date('YmdHis') . "_" . rand(10000, 99999) . "." . $ext; // create random name for file
                    } while (file_exists($folder . $nameFile));
                    if ($nameFile) {
                        $file->move($folder, $nameFile);
                        $result['dirFile'] = $folderJS . $nameFile;
                        $result['success'] = CODE_AJAX_SUCCESS;
                        $result['fileName'] = $nameFile;
                        $result['createdAt'] = date("Y-m-d H:i:s");
                    } else {
                        $result['success'] = CODE_AJAX_ERROR;
                        $result['msg'] = sprintf(__(Config::get('message.common_msg.upload_max_file_size')), '2mb');
                    }
                }
            }
        } else {
            $result['success'] = CODE_AJAX_ERROR;
            $result['msg'] = 'Error';
        }
        return $result;
    }
}
