<?php

namespace App\Services\User;

use App\Models\MyList;
use App\Repositories\User\HomeRepository;
use App\Services\TrancendService;

class HomeService extends TrancendService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $homeRepository;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(
        HomeRepository $homeRepository
    ) {
        $this->homeRepository = $homeRepository;
    }

    /**
     * get top Lecture
     *
     * @return Repository
     */
    public function getTopLecture()
    {
        return $this->homeRepository->getTopLecture();
    }

    /**
     * get Lecture
     *
     * @return Repository
     */
    public function getLectures($params = null)
    {
        return $this->homeRepository->getLectures($params);
    }
}
