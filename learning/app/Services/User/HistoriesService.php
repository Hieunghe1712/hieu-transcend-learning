<?php

namespace App\Services\User;

use App\Models\History;
use App\Services\TrancendService;
use App\Repositories\User\HistoriesRepository;

class HistoriesService extends TrancendService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $historiesRepository;

    /**
     * @var History $history
     */
    protected $history;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(
        HistoriesRepository $historiesRepository,
        History $history
    ) {
        $this->historiesRepository = $historiesRepository;
        $this->history = $history;
    }

    /**
     * get history by user id and lecture id
     * @param $idUser
     * @param $idLecture
     *
     * @return Repository
     */
    public function getHistoryByUserIdAndLectureId($idUser, $idLecture, $date)
    {
        return $this->historiesRepository->getHistoryByUserIdAndLectureId($idUser, $idLecture, $date);
    }

    /**
     * Add history lecture
     *
     * @param array $dataPosts
     *
     * @return Repository
     */
    public function addHistoryLecture(array $dataPosts)
    {
        $newHistory = new History();
        $newHistory->fill($dataPosts);

        return $this->historiesRepository->addOrUpdateHistoryLecture($newHistory);
    }

    /**
     * Update history lecture
     *
     * @param array $dataPosts
     *
     * @return Repository
     */
    public function updateHistoryLecture(array $dataPosts, $idHistory)
    {
        $updateHistory = $this->history->find($idHistory);
        $updateHistory->fill($dataPosts);
        return $this->historiesRepository->addOrUpdateHistoryLecture($updateHistory);
    }

}
