<?php

namespace App\Services\User;

use App\Models\Likes;
use App\Services\TrancendService;
use App\Models\MyList;
use App\Models\Views;
use App\Repositories\User\ViewsRepository;

class ViewsService extends TrancendService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $viewsRepository;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(
        ViewsRepository $viewsRepository
    ) {
        $this->viewsRepository = $viewsRepository;
    }

    /**
     * get views by user id and lecture id
     * @param $idUser
     * @param $idLecture
     *
     * @return Repository
     */
    public function getViewsByUserIdAndLectureId($idUser, $idLecture)
    {
        return $this->viewsRepository->getViewsByUserIdAndLectureId($idUser, $idLecture);
    }

    /**
     * Add like lecture
     *
     * @param array $dataPosts
     *
     * @return Repository
     */
    public function addViewLecture(array $dataPosts)
    {
        $newViews = new Views();
        $newViews->fill($dataPosts);

        return $this->viewsRepository->addViewLecture($newViews);
    }
}
