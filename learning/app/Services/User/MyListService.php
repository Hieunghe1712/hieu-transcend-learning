<?php

namespace App\Services\User;

use App\Services\TrancendService;
use App\Models\MyList;
use App\Repositories\User\MyListsRepository;

class MyListService extends TrancendService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $mylistRepository;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(
        MyListsRepository $mylistRepository
    ) {
        $this->mylistRepository = $mylistRepository;
    }

    /**
     * get my list by user id and lecture id
     * @param $idUser
     * @param $idLecture
     *
     * @return Repository
     */
    public function getMyListByUserIdAndLectureId($idUser, $idLecture)
    {
        return $this->mylistRepository->getMyListByUserIdAndLectureId($idUser, $idLecture);
    }

    /**
     * Add to my list
     *
     * @param array $dataPosts
     *
     * @return Repository
     */
    public function addToMyList(array $dataPosts)
    {
        $newMyList = new MyList();
        $newMyList->fill($dataPosts);

        return $this->mylistRepository->addToMyList($newMyList);
    }

    /**
     * Remove to my list
     *
     * @param $idLecture
     *
     * @return Repository
     */
    public function removeToMyList($idMyList)
    {
        $removeMyList = MyList::find($idMyList);

        return $this->mylistRepository->removeToMyList($removeMyList);
    }
}
