<?php

namespace App\Services\User;

use Illuminate\Http\Request;
use App\Repositories\User\LectureRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Services\TrancendService;
use App\Models\Lecture;
use App\Models\Quizze;
use App\Models\MyList;
use App\Models\QuizOption;
use App\Models\AnswerUser;
use App\Models\AnswerValues;

class LectureService extends TrancendService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $lectureRepository;
    protected $lecture;
    protected $quizze;
    protected $myList;
    protected $quizOption;

    /**
     * @var AnswerUser $answerUser
     */
    protected $answerUser;    

    /**
     * @var AnswerValues $answerValues
     */
    protected $answerValues;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(
        LectureRepository $lectureRepository,
        Lecture $lecture,
        Quizze $quizze,
        MyList $myList,
        QuizOption $quizOption,
        AnswerUser $answerUser,
        AnswerValues $answerValues
    ) {
        $this->lectureRepository = $lectureRepository;
        $this->lecture = $lecture;
        $this->quizze = $quizze;
        $this->myList = $myList;
        $this->quizOption = $quizOption;
        $this->answerUser = $answerUser;
        $this->answerValues = $answerValues;
    }

    /**
     * Get my lecture list
     *
     * @param array $params
     * @return Repository
     */
    public function getMyListLectures($params = null)
    {
        return $this->lectureRepository->getMyListLectures($params);
    }

    /**
     * search list of lecture
     *
     * @param array $params
     * @return Repository
     */
    public function searchLecture($params = null)
    {
        return $this->lectureRepository->searchLecture($params);
    }

    /**
     * Get data my list by id
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function findMyListById($id)
    {
        return $this->lectureRepository->getMyListById($id);
    }

    /**
     * Delete my list
     *
     * @param int $id
     * @return bool
     */
    public function deleteMyList($id)
    {
        $result = true;
        $myList = $this->lectureRepository->getMyListById($id);
        if (!$myList->delete()) {
            $result = false;
        }
        return $result;
    }

    /**
     * Get lecture history list
     *
     * @param array $params
     * @return Repository
     */
    public function getLectureHistory($params = null)
    {
        return $this->lectureRepository->getLectureHistory($params);
    }

    /**
    * Get data lecture manager by id
    *
    * @param int $idLecture
    * @return \Illuminate\Http\Response
    */
    public function findLectureById($idLecture)
    {
        return $this->lectureRepository->getLectureById($idLecture);
    }

    /**
    * update Liked NUmber
    *
    * @param int $idLecture
    * @return \Illuminate\Http\Response
    */
    public function updateLikedNumber($idLecture, $remove)
    {
        $lecture = $this->lecture->find($idLecture);
        if ($remove == true) {
            $data['liked_number'] = $lecture->liked_number - 1;
        } else {
            $data['liked_number'] = $lecture->liked_number + 1;
        }
        $lecture->fill($data);
        return $this->lectureRepository->updateLecture($lecture);
    }

    /**
    * update View NUmber
    *
    * @param int $idLecture
    * @return \Illuminate\Http\Response
    */
    public function updateViewNumber($idLecture)
    {
        $lecture = $this->lecture->find($idLecture);
        $data['view_number'] = $lecture->view_number + 1;
        $lecture->fill($data);
        return $this->lectureRepository->updateLecture($lecture);
    }

    /**
    * get random lecture
    *
    * @param $numberRecord
    * @param $idLecture
    * @return \Illuminate\Http\Response
    */
    public function getRandomLecture($numberRecord, $idLecture)
    {
        return $this->lectureRepository->getRandomLecture($numberRecord, $idLecture);
    }

    /** Get lecture note list
     * 
     * @param array $params
     * @return Repository
     */
    public function getLectureNote($params = null)
    {
        return $this->lectureRepository->getLectureNote($params);

    }

    /**
     * Get data note by id
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function findMemoById($id)
    {
        return $this->lectureRepository->getMemoById($id);
    }

    /**
     * Delete note
     *
     * @param int $id
     * @return bool
     */
    public function deleteMemo($id)
    {
        $result = true;
        $note = $this->lectureRepository->getMemoById($id);
        if ($note) {
            $note->deleted_at = Carbon::now();
            if (!$note->save()) {
                $result = false;
            }
        }
        return $result;
    }


    /**
     * get Document by lecture id
     *
     * @param int $id
     * @return bool
     */
    public function getDocuments($id)
    {
        return $this->lectureRepository->getDocuments($id);
    }


    /**
     * get quizzes by lecture id
     *
     * @param int $id
     * @return bool
     */
    public function getQuizByLectureId($id)
    {
        return $this->lectureRepository->getQuizByLectureId($id);
    }

    /**
    * Get data lecture by id
    *
    * @param int $idLecture
    * @return \Illuminate\Http\Response
    */
    public function dataLectureById($idLecture)
    {
        return $this->lectureRepository->dataLectureById($idLecture);
    }  

    /**
     * Get appearance time by lecture id
     *
     * @param int $idLecture
     * @return \Illuminate\Http\Response
     */
    public function getAppearanceTimeByLectureId($id)
    {
        return $this->lectureRepository->getAppearanceTimeByLectureId($id);
    }

    /**
     * Get quizzes by lecture id
     *
     * @param int $idLecture
     * @return \Illuminate\Http\Response
     */
    public function findQuizByLectureId($idLecture)
    {
        $data = [];
        $quizzes = $this->lectureRepository->dataQuizByLectureId($idLecture);
        foreach($quizzes as $quiz) {
            $arr = [];
            $arr['id'] = $quiz->id;
            $arr['sort_number'] = $quiz->sort_number;
            $arr['title'] = $quiz->title;
            $arr['question_sentence'] = $quiz->question_sentence;
            $arr['quiz_option_id'] = $quiz->quiz_option_id;
            $arr['remark'] = $quiz->remark;
            $arr['appearance_time'] = $quiz->appearance_time;
            $arr['type'] = $quiz->type;
            $optionValues = $this->lectureRepository->getOptionValues($quiz->id);
            if(count($optionValues)) {
                foreach($optionValues as $optionValue) {
                    $arr['quiz_values'][] = [
                        'id' => $optionValue->id,
                        'name' => $optionValue->name
                    ];
                }
            } else {
                $arr['quiz_values'] = [];
            }
            $data[] = $arr;
        }
        return $data;
    }

    /**
     * Save answer of quiz
     * @param array $dataPosts
     * @return Repository
     */
    public function saveAnswer (array $dataPosts) 
    {
        $answerUser = $this->answerUser;
        $answerUser->fill($dataPosts);
        $answerUser->user_id = $dataPosts['user_id'];
        $answerUser->lecture_id = $dataPosts['lecture_id'];
        $answerUser->status = 1;
        if($answerUser) {
            $answerValues = $this->answerValues;
            $answerValues->fill($dataPosts);
            $answerValues->quiz_id = (int)$dataPosts['quiz_id'];
            $answerValues->value = json_encode($dataPosts['quiz_answer']);
        }
        return $this->lectureRepository->saveAnswer($answerUser, $answerValues);

    }

    /**
     * Get explanations by lecture id
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getExplainByLectureId($id)
    {
        return $this->lectureRepository->getExplainByLectureId($id);
    }

    /**
     * Get data answer by user id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getAnswerByUserId($id)
    {
        return $this->lectureRepository->getAnswerByUserId($id);
    }

    
    /** Get lecture note list on page
     * 
     * @return Repository
     */
    public function getAllLectureNote()
    {
        return $this->lectureRepository->getAllLectureNote();

    }

    /** Get all my lecture list
     * 
     * @param array $params
     * @return Repository
     */
    public function getAllMyListLecture($params)
    {
        return $this->lectureRepository->getAllMyListLecture($params);

    }
    
}
