<?php

namespace App\Services\User;

use Illuminate\Http\Request;
use App\Repositories\User\LectureRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Services\TrancendService;
use App\Models\Lecture;
use App\Models\Memos;
use App\Models\Quizze;
use App\Models\MyList;
use App\Models\QuizOption;
use App\Repositories\User\MemosRepository;

class MemosService extends TrancendService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $memoRepository;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(
        MemosRepository $memoRepository
    ) {
        $this->memoRepository = $memoRepository;
    }

    /**
     * Get max sort number by lesson id
     *
     * @return mixed
     *
     *
     */
    public function getMaxSortNumberByLessonId($id)
    {
        return $this->memoRepository->getMaxSortNumberByLessonId($id);
    }

     /**
     * Save create memo
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createMemo(array $dataPosts)
    {
        $newMemo = new Memos();
        $newMemo->fill($dataPosts);

        return $this->memoRepository->createMemo($newMemo);
    }
}
