<?php

namespace App\Services\User;

use App\Repositories\User\ProfileRepository;
use App\Services\TrancendService;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class ProfileService extends TrancendService
 {
    /**
     * @var User $user
     */
    protected $user;

    /**
     * @var profileRepository $profileRepository
     */
    protected $profileRepository;


    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(User $user, ProfileRepository $profileRepository)
    {
        $this->user = $user;
        $this->profileRepository = $profileRepository;
    }
     /**
     * Save edit student account
     *
     * @param int $id
     * @param array $params
     * @return Repository
     */
    public function editProfile(array $params) {
        $profile = $this->user->find($params['id']);
        $profile->created_admin_id = $params['id'];
        $profile->profile_img = $params['profile_img'] ?? null;
        if(isset($params['password']) || $params['password'] !=null){
            $profile->password = Hash::make($params['password']);
        }
        $image = $params['profile_img'] ?? null;

        return $this->profileRepository->updateProfile($profile, $image);
    }
}
