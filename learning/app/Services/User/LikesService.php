<?php

namespace App\Services\User;

use App\Models\Likes;
use App\Services\TrancendService;
use App\Models\MyList;
use App\Repositories\User\LikesRepository;
use App\Repositories\User\MyListsRepository;

class LikesService extends TrancendService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $likesRepository;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(
        LikesRepository $likesRepository
    ) {
        $this->likesRepository = $likesRepository;
    }

    /**
     * get like by user id and lecture id
     * @param $idUser
     * @param $idLecture
     *
     * @return Repository
     */
    public function getLikesByUserIdAndLectureId($idUser, $idLecture)
    {
        return $this->likesRepository->getLikesByUserIdAndLectureId($idUser, $idLecture);
    }

    /**
     * Add like lecture
     *
     * @param array $dataPosts
     *
     * @return Repository
     */
    public function addLikeLecture(array $dataPosts)
    {
        $newLikes = new Likes();
        $newLikes->fill($dataPosts);

        return $this->likesRepository->addLikeLecture($newLikes);
    }

    /**
     * Remove like lecture
     *
     * @param $idLecture
     *
     * @return Repository
     */
    public function removeLikeLecture($idLike)
    {
        $removeLikes = Likes::find($idLike);

        return $this->likesRepository->removeLikeLecture($removeLikes);
    }
}
