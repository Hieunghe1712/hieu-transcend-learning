<?php

namespace App\Services\Admin;

use App\Models\QuizValues;
use App\Repositories\Admin\QuizValuesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Services\TrancendService;

class QuizValuesService extends TrancendService
{
    protected $quizValuesRepository;
    protected $quizValue;

    public function __construct(
        QuizValuesRepository $quizValuesRepository,
        QuizValues $quizValue
    ) {
        $this->quizValuesRepository = $quizValuesRepository;
        $this->quizValue = $quizValue;
    }

    /**
     * Save create quiz values
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createQuizValues(array $dataPosts)
    {
        $newQuizValues = new QuizValues();
        $newQuizValues->fill($dataPosts);

        return $this->quizValuesRepository->saveCreateQuizValues($newQuizValues);
    }

    /**
     * Hard delete quiz values
     *
     * @param array $id
     * @return Repository
     */
    public function hardDeleteQuizValues($id)
    {
        $delQuizValues = QuizValues::find($id);

        return $this->quizValuesRepository->hardDeleteQuizValues($delQuizValues);
    }
}
