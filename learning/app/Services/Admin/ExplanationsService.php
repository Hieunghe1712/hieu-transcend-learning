<?php

namespace App\Services\Admin;

use App\Models\Explanations;
use App\Repositories\Admin\ExplanationsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Services\TrancendService;

class ExplanationsService extends TrancendService
{
    protected $explanationsRepository;
    protected $explanations;

    public function __construct(
        ExplanationsRepository $explanationsRepository,
        Explanations $explanations
    ) {
        $this->explanationsRepository = $explanationsRepository;
        $this->explanations = $explanations;
    }

    /**
     * Save create explanations
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createExplanations(array $dataPosts)
    {
        $newExplanations = new Explanations();
        $newExplanations->fill($dataPosts);

        return $this->explanationsRepository->saveCreateExplanations($newExplanations);
    }

    /**
     * Hard delete explanations
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function hardDeleteExplanation($id)
    {
        $delExplanations = Explanations::find($id);

        return $this->explanationsRepository->hardDeleteExplanation($delExplanations);
    }
}
