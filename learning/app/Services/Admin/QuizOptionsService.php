<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use App\Repositories\Admin\QuizOptionsRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Services\TrancendService;

class QuizOptionsService extends TrancendService
{
    protected $quizOptionsRepository;

    public function __construct(
        QuizOptionsRepository $quizOptionsRepository
    ) {
        $this->quizOptionsRepository = $quizOptionsRepository;
    }

    /**
     * Get list quiz options
     *
     * @return Repository
     */
    public function getListQuizOptions()
    {
        return $this->quizOptionsRepository->getListQuizOptions();
    }

    /**
     * Get list quiz options have choice
     *
     * @return Repository
     */
    public function getListIdsQuizOptionsHaveChoice()
    {
        return $this->quizOptionsRepository->getListIdsQuizOptionsHaveChoice();
    }

    /**
     * Get quiz options by id
     *
     * @param int $id
     * @return Repository
     */
    public function getQuizOptionById($id)
    {
        return $this->quizOptionsRepository->getQuizOptionById($id);
    }
}
