<?php

namespace App\Services\Admin;

use App\Models\Documents;
use App\Repositories\Admin\DocumentsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Services\TrancendService;

class DocumentsService extends TrancendService
{
    protected $documentsRepository;
    protected $documents;

    public function __construct(
        DocumentsRepository $documentsRepository,
        Documents $documents
    ) {
        $this->documentsRepository = $documentsRepository;
        $this->documents = $documents;
    }

    /**
     * Save create documents
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createDocuments(array $dataPosts)
    {
        $newDocuments = new Documents();
        $newDocuments->fill($dataPosts);

        return $this->documentsRepository->saveCreateDocuments($newDocuments);
    }

    /**
     * Hard delete documents
     *
     * @param $id
     * @return Repository
     */
    public function hardDeleteDocuments($id)
    {
        $delDocuments = Documents::find($id);

        return $this->documentsRepository->hardDeleteDocuments($delDocuments);
    }
}
