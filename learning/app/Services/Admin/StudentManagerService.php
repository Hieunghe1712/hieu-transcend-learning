<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Services\TrancendService;
use App\Repositories\Admin\AdminStudentRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Models\User;
use Carbon\Carbon;

class StudentManagerService extends TrancendService
{
    /**
     * @var AdminStudentRepository $adminStudentRepository
     */
    protected $adminStudentRepository;

    /**
     * @var User $user
     */
    protected $user;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(
        AdminStudentRepository $adminStudentRepository,
        User $user
    ) {
        $this->adminStudentRepository = $adminStudentRepository;
        $this->user = $user;
    }
    /**
     * Save create user
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createStudent(array $dataPosts)
    {
        $newStudent = $this->user;
        $newStudent->fill($dataPosts);
        $newStudent->name = $dataPosts['name'];
        $newStudent->password = Hash::make($dataPosts['password']);
        $newStudent->created_admin_id = Auth::guard('admin')->user()->id;

        return $this->adminStudentRepository->saveCreateStudent($newStudent);
    }
    /**
     * Get list user
     *
     * @return Repository
     */
    public function getListStudent($params = null) {
        return $this->adminStudentRepository->getListStudents($params);
    }
    /**
     * Get data user by id
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return json
     */
    public function findStudentById($id) {
        return $this->adminStudentRepository->getStudentById($id);
    }
    /**
     * Save create user
     *
     * @param int $idUser
     * @param array $dataPosts
     * @return bool
     */
    public function editStudent (
        int $idUser,
        array $dataPosts
    ) {
        if ($idUser != $dataPosts['id']) {
            return false;
        }
        $editStudent = $this->user->find($idUser);
        $editStudent->fill($dataPosts);
        $editStudent->name = $dataPosts['name'];
        return $this->adminStudentRepository->saveEditStudent($editStudent, $idUser);
    }

    /**
     * Delete user
     *
     * @param int $idUser
     * @return bool
     */
    public function deleteStdent($idUser) {
        $result = true;
        $user = $this->adminStudentRepository->getStudentById($idUser);
        if ($user) {
            $user->deleted_at = Carbon::now();
            $user->deleted_admin_user_id = Auth::guard('admin')->user()->id;
            if (!$user->save()) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Get data user by id
     *
     * @param int $iduser
     * @return \Illuminate\Http\Response
     */
    public function dataStudentById($idUser){
        return $this->adminStudentRepository->getStudentById($idUser);
    }
}
