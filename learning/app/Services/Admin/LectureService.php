<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use App\Repositories\Admin\AdminLectureRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Services\TrancendService;
use App\Models\Lecture;
use App\Models\Quizze;
use App\Models\MyList;
use App\Models\QuizOption;

class LectureService extends TrancendService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $adminLectureRepository;
    protected $lecture;
    protected $quizze;
    protected $myList;
    protected $quizOption;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(
        AdminLectureRepository $adminLectureRepository,
        Lecture $lecture,
        Quizze $quizze,
        MyList $myList,
        QuizOption $quizOption
    ) {
        $this->adminLectureRepository = $adminLectureRepository;
        $this->lecture = $lecture;
        $this->quizze = $quizze;
        $this->myList = $myList;
        $this->quizOption = $quizOption;
    }

    /**
     * Get list of lecture
     *
     * @param array $params
     * @return Repository
     */
    public function list($params = null)
    {
        return $this->adminLectureRepository->list($params);
    }

    /**
     * Save create lecture
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createLecture(array $dataPosts)
    {
        $newLecture = $this->lecture;
        $newLecture->fill($dataPosts);
        $newLecture->liked_number = 0;
        $newLecture->view_number = 0;
        $newLecture->created_admin_id = Auth::guard('admin')->user()->id;

        return $this->adminLectureRepository->saveCreateLecture($newLecture);
    }

    /**
     * Get list lecture manager
     * 
     * @param array $params
     * @return Repository
     */
    public function getMyListLectures($params = null)
    {
        return $this->adminLectureRepository->getMyListLectures($params);
    }

    /**
     * Get data of first quizze
     *
     * @param int $idLecture
     * @return Repository
     */
    public function getFirstQuizzeOfLecture(int $idLecture)
    {
        return $this->adminLectureRepository->findQuizzeByLectureId($idLecture);
    }

    /**
     * Save create lecture
     *
     * @param int $idLecture
     * @param array $dataPosts
     * @return bool
     */
    public function editLecture(int $idLecture, array $dataPosts)
    {
        if ($idLecture != $dataPosts['id']) {
            return false;
        }
        $editLecture = $this->lecture->find($idLecture);
        $editLecture->fill($dataPosts);
        return $this->adminLectureRepository->saveEditLecture($editLecture);
    }

    /**
     * Get data lecture manager by id
     *
     * @param int $idLecture
     * @return \Illuminate\Http\Response
     */
    public function findLectureById($idLecture)
    {
        return $this->adminLectureRepository->getLectureById($idLecture);
    }    

    /**
     * Soft delete lecture
     *
     * @param int $idLecture
     * @return \Illuminate\Http\Response
     */
    public function deleteLecture($idLecture)
    {
        return $this->adminLectureRepository->deleteLecture($idLecture);
    }

    /**
     * get max id
     *
     * @return \Illuminate\Http\Response
     */
    public function getMaxId()
    {
        return $this->adminLectureRepository->getMaxId();
    }

    public function findQuizByLectureId($idLecture)
    {
        return $this->adminLectureRepository->getQuizByLectureId($idLecture);
    }    

    /**
     * Get data explain by lecture id
     *
     * @param int $idLecture
     * @return \Illuminate\Http\Response
     */
    public function findExplainByLectureId($idLecture)
    {
        return $this->adminLectureRepository->getExplainByLectureId($idLecture);
    }    

    /**
     * Get data heading by lecture id
     *
     * @param int $idLecture
     * @return \Illuminate\Http\Response
     */
    public function findHeadingByLectureId($idLecture)
    {
        return $this->adminLectureRepository->getHeadingByLectureId($idLecture);
    }    

    /**
     * Get data options value by lecture id
     *
     * @param int $idLecture
     * @return \Illuminate\Http\Response
     */
    public function findOptionValueByLectureId($idLecture)
    {
        return $this->adminLectureRepository->getOptionValueByLectureId($idLecture);
    }
}
