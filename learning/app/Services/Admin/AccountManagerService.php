<?php

namespace App\Services\Admin;

use Illuminate\Http\Request;
use App\Repositories\Admin\AdminAccountRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Services\TrancendService;
use Carbon\Carbon;

class AccountManagerService extends TrancendService
{
    /**
     * @var \App\Repositories\Repository
     */
    protected $adminAccountRepository;
    protected $account;

    public function __construct (
        AdminAccountRepository $adminAccountRepository,
        Admin $account
    )
    {
        $this->adminAccountRepository = $adminAccountRepository;
        $this->account = $account;
    }

    /**
     * Save create account
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createAccount(array $dataPosts)
    {
        $newAccount = $this->account;
        $newAccount->fill($dataPosts);
        $newAccount->name = $dataPosts['name'];
        $newAccount->password = Hash::make($dataPosts['password']);
        $newAccount->created_admin_id = Auth::guard('admin')->user()->id;

        return $this->adminAccountRepository->saveCreateAccount($newAccount);
    }

    /**
     * Get list account manager
     *
     * @return Repository
     */
    public function getListAccountManager ($params = null) {
        return $this->adminAccountRepository->getListAccountManagers($params);
    }

    /**
     * Get data account by id
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return json
     */
    public function findAccountById($id) {
        return $this->adminAccountRepository->getAccountById($id);
    }

    /**
     * Save create account
     *
     * @param int $idAccount
     * @param array $dataPosts
     * @return bool
     */
    public function editAccount (
        int $idAccount,
        array $dataPosts
    ) {
        if ($idAccount != $dataPosts['id']) {
            return false;
        }
        $editAccount = $this->account->find($idAccount);
        $editAccount->fill($dataPosts);
        $editAccount->name = $dataPosts['name'];
        if ($dataPosts['password'] == PASSWORD) {
            $editAccount->password = $dataPosts['old_password'];
        } else {
            $editAccount->password = Hash::make($dataPosts['password']);
        }

        return $this->adminAccountRepository->saveEditAccount($editAccount, $idAccount);
    }

    /**
     * Delete account manager
     *
     * @param int $idAccount
     * @return bool
     */
    public function deleteAccountManager($idAccount) {
        $result = true;
        $account = $this->adminAccountRepository->getAccountById($idAccount);
        if ($account) {
            $account->deleted_at = Carbon::now();
            $account->deleted_admin_user_id = Auth::guard('admin')->user()->id;
            if (!$account->save()) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Get data account manager by id
     *
     * @param int $idAccount
     * @return \Illuminate\Http\Response
     */
    public function dataAccountById($idAccount){
        return $this->adminAccountRepository->getAccountById($idAccount);
    }
}
