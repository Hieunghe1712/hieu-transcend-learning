<?php

namespace App\Services\Admin;

use App\Models\UploadFiles;
use App\Repositories\Admin\UploadFilesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Services\TrancendService;

class UploadFilesService extends TrancendService
{
    protected $uploadFilesRepository;
    protected $uploadFiles;

    public function __construct(
        UploadFilesRepository $uploadFilesRepository,
        UploadFiles $uploadFiles
    ) {
        $this->uploadFilesRepository = $uploadFilesRepository;
        $this->uploadFiles = $uploadFiles;
    }

    /**
     * Save create upload files
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function saveCreateUploadFiles(array $dataPosts)
    {
        $newUploadFiles = new UploadFiles();
        $newUploadFiles->fill($dataPosts);

        return $this->uploadFilesRepository->saveCreateUploadFiles($newUploadFiles);
    }

    /**
     * Hard delete uploaded files
     *
     * @param $id
     * @return Repository
     */
    public function hardDeleteUploadedFile($id)
    {
        $delUploadFiles = UploadFiles::find($id);

        return $this->uploadFilesRepository->hardDeleteUploadedFile($delUploadFiles);
    }
}
