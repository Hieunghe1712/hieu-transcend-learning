<?php

namespace App\Services\Admin;

use App\Models\Headings;
use App\Repositories\Admin\HeadingsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Services\TrancendService;

class HeadingsService extends TrancendService
{
    protected $headingsRepository;
    protected $headings;

    public function __construct(
        HeadingsRepository $headingsRepository,
        Headings $headings
    ) {
        $this->headingsRepository = $headingsRepository;
        $this->headings = $headings;
    }

    /**
     * Save create headings
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createHeadings(array $dataPosts)
    {
        $newHeadings = new Headings();
        $newHeadings->fill($dataPosts);

        return $this->headingsRepository->saveCreateHeadings($newHeadings);
    }

    /**
     * Hard delete headings
     *
     * @param id
     * @return Repository
     */
    public function hardDeleteHeadings($id)
    {
        $delHeadings = Headings::find($id);

        return $this->headingsRepository->hardDeleteHeadings($delHeadings);
    }
}
