<?php

namespace App\Services\Admin;

use App\Models\Quizze;
use App\Repositories\Admin\QuizRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use App\Services\TrancendService;

class QuizService extends TrancendService
{
    protected $quizRepository;

    public function __construct(
        QuizRepository $quizRepository
    ) {
        $this->quizRepository = $quizRepository;
    }

    /**
     * Save create quiz
     *
     * @param array $dataPosts
     * @return Repository
     */
    public function createQuiz(array $dataPosts)
    {
        $newQuiz = new Quizze();
        $newQuiz->fill($dataPosts);

        return $this->quizRepository->saveCreateQuiz($newQuiz);
    }

    /**
     * Hard delete quiz
     *
     * @param $id
     * @return Repository
     */
    public function hardDeleteQuiz($id)
    {
        $delQuiz = Quizze::find($id);

        return $this->quizRepository->hardDeleteQuiz($delQuiz);
    }
}
