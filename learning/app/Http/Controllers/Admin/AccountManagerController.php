<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Services\Admin\AccountManagerService;
use App\Services\TrancendService;
use App\Http\Requests\Admin\CreateAccountManagerRequest;
use App\Http\Requests\Admin\ImageRequest;

class AccountManagerController extends Controller
{

    /**
     * @var TrancendService $trancendService
     */
    protected $trancendService;

    /**
     * @var AdminAccountService $adminAccountService
     */
    protected $adminAccountService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        TrancendService $trancendService,
        AccountManagerService $adminAccountService
    ) {
        $this->trancendService = $trancendService;
        $this->adminAccountService = $adminAccountService;
        $this->middleware('CheckAdminLogin');
    }

    /**
     * Show view list account management
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $params = $request->input();
        $listAccount = $this->adminAccountService->getListAccountManager($params);
        return view("admin.manager.list", compact("listAccount"));
    }

    /**
     * Show view create account
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function createForm(Request $request)
    {
        if ($key = $request->input('key')) {
            $data = $request->session()->get('confirm_data' . $key);
            return view("admin.manager.create", compact("data"));
        } else {
            return view('admin.manager.create');
        }
    }

    /**
     * Post data for create account management
     *
     * @param CreateAccountManagerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(CreateAccountManagerRequest $request) {
        $request->merge([
            'admin_id' => Auth::guard('admin')->user()->id
        ]);

        $dataPost = $request->all();

        if (isset($request->validator) && $request->validator->fails()) {
            return redirect()->back()->withInput()->with(['data' => $dataPost])->withErrors($request->validator);
        } else {
            (isset($dataPost['key']) && $dataPost['key']) ? $key = $dataPost['key'] : $key = Str::random(32);
            $request->session()->put('confirm_data' . $key, $dataPost);
            return redirect()->route('admin.create_account_confirm', ['key' => $key]);
        }
    }

    /**
     * Show view confirm create account management
     *
     * @param \Illuminate\Http\Request $request
     * @param $key
     * @return \Illuminate\Http\Response
     */
    public function createConfirmAccount(
        Request $request,
        $key
    ) {
        $data = $request->session()->get('confirm_data' . $key);
        if ($data) {
            return view('admin.manager.create_confirm', compact('data', 'key'));
        } else {
            return redirect()->route('admin.account_manager_list');
        }
    }

    /**
     * Save create account management
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveCreateAccount (Request $request) {
        $msg = '';
        $key = $request->input('key');
        $dataPosts = $request->session()->get('confirm_data' . $key);
        $createAccount = $this->adminAccountService->createAccount($dataPosts);
        if ($createAccount) {
            $msg = 'アカウント' . Config::get('message.common_msg.insert_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.create_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');
        return redirect()->route('admin.account_manager_list')->with($result, $msg);
    }

    /**
     * Show detail account management
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getDataAccountById ($id) {
        $account = $this->adminAccountService->findAccountById($id);
        if ($account) {
            return view('admin.manager.detail', compact('account'));
        } else {
            return redirect()->route('admin.account_manager_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
    }

    /**
     * Show edit form account management
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function editAccountForm(
        Request $request,
        $id
    ) {
        $account = $this->adminAccountService->findAccountById($id);
        if (!$account) {
            return redirect()->route('admin.account_manager_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        if ($key = $request->input('key')) {
            $account = $request->session()->get('confirm_data' . $key);
        }
        return view("admin.manager.edit", compact("account", "id"));
    }

    /**
     * Submit Edit Form Account Management
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return route
     */
    public function edit(
        Request $request,
        $id
    ) {
        $request->merge([
            'admin_id' => Auth::guard('admin')->user()->id
        ]);

        $dataPost = $request->all();
        
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect()->back()->with(['account' => $dataPost])->withErrors($request->validator);
        } else {
            (isset($dataPost['key']) && $dataPost['key']) ? $key = $dataPost['key'] : $key = Str::random(32);
            $request->session()->put('confirm_data' . $key, $dataPost);
            return redirect()->route('admin.edit_account_confirm', ['key' => $key, 'id' => $id]);
        }
    }

    /**
     * Show view confirm edit account management
     *
     * @param \Illuminate\Http\Request $request
     * @param $key
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function editConfirmAccount(
        Request $request,
        $key,
        $id
    ) {
        if (!$this->adminAccountService->dataAccountById($id)) {
            return redirect()->route('admin.account_manager_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        $data = $request->session()->get('confirm_data' . $key);
        
        if ($data) {
            return view('admin.manager.edit_confirm', compact('data', 'key', 'id'));
        } else {
            return redirect()->route('admin.account_manager_list', ['id' => $id]);
        }
    }

    /**
     * Save edit account management
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function saveEditAccount(
        Request $request,
        $id
    ) {
        $key = $request->input('key');
        $dataPosts = $request->session()->get('confirm_data' . $key);
        if (!$dataPosts) {
            return redirect()->route('admin.account_manager_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        $editAccount = $this->adminAccountService->editAccount($id, $dataPosts);
        if ($editAccount) {
            $msg = 'アカウント' . Config::get('message.common_msg.update_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.edit_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');

        return redirect()->route('admin.account_manager_list')->with($result, $msg);
    }

    /**
     * Delete account management
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAccount (
        Request $request,
        $id
    ) {
        if (!$this->adminAccountService->dataAccountById($id)) {
            return redirect()->route('admin.account_manager_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        if ($this->adminAccountService->deleteAccountManager($id)) {
            $msg = 'アカウント' . Config::get('message.common_msg.delete_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.delete_error');
            $result = ERROR;
        }
        if (Auth::guard('admin')->check() && Auth::guard('admin')->user()->id == $id) {
            return redirect()->route('admin.logout');
        }
        return redirect()->route('admin.account_manager_list')->with($result, $msg);
    }
    /**
     * Upload file image account
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function upload(ImageRequest $request) {
        $data = $request->all();
        $file = $request->file(['file']);
        if ($file) {
            $originalName = $file->getClientOriginalName();
            $ext = strtolower(pathinfo($originalName, PATHINFO_EXTENSION));
            $nameFile = date('YmdHis') . "_" . rand(10000, 99999) . "." . $ext; // create random name for file
            $file->storeAs('public/images/', $nameFile);
        }
        return response()->json(['data' => $nameFile]);

    }
}
