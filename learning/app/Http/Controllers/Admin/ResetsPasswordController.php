<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Auth;

/**
 * Description of ResetsPasswordController
 *
 * @author ADMIN
 */
class ResetsPasswordController extends Controller {

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     * @var string
     */
    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * Create a new controller instance.
     * @return void
     */
    public function showResetForm(Request $request, $token = null)
    {
        $email = $request->email;
        return view('admin.auth.recover_password', [
            'token' => $token,
            'email' => $email
        ]);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'email' => 'required',
            'token'    => 'required',
            'password' => 'required|min:8|max:128',
            'password_confirm' => 'required|same:password',
        ];
    }

    /**
     * Get the password reset validation error messages.
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [
            'email.required' => 'トークン'. Config::get('message.validation_common_msg.input_err'),
            'token.required' => 'トークン'. Config::get('message.validation_common_msg.input_err'),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.max'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'password.min'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'password_confirm.required' => 'パスワードを認証する'. Config::get('message.validation_common_msg.input_err'),
            'password_confirm.same' => 'パスワードが同じである必要があることを確認する',
         ];
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirm', 'token'
        );
    }

    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);
        $user->save();
        event(new PasswordReset($user));
        return $this->guard()->login($user);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse(Request $request, $response)
    {
        return redirect($this->redirectPath())->with('status', trans($response));
    }

    /**
     * Get the broker to be used during password reset.
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('admins');
    }
}
