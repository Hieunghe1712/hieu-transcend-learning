<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    /**
     * dashboard
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard.index');
    }

    /**
     * Change language
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function changeLanguage(Request $request , $language) {
        $lang = $request->language;
        $language = config('app.locale');
        if ($lang == 'jp') {
            $language = 'jp';
        }
        if ($lang == 'vi') {
            $language = 'vi';
        }
        \Session::put('language', $language);
        return redirect()->back();
    }
}
