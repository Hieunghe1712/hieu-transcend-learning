<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Services\Admin\StudentManagerService;
use App\Http\Requests\Admin\CreateStudentRequest;
use App\Http\Requests\Admin\EditStudentRequest;
use App\Http\Requests\Admin\ImageRequest;

class StudentManagerController extends Controller
{
    /**
     * @var StudentManagerService $studentManagerService
     */
    protected $studentManagerService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        StudentManagerService $studentManagerService
    ) {
        $this->studentManagerService = $studentManagerService;
        $this->middleware('CheckAdminLogin');
    }

    /**
     * Show view list of students
     *
     * @param \Illuminate\Http\Request $request
     * @return Illuminate\Support\Facades\View
     */
    public function index(Request $request)
    {
        $params = $request->input();
        $students = $this->studentManagerService->getListStudent($params);
        return view("admin.students.list", compact("students"));
    }
    /**
     * Show view create student
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function createStudent(Request $request)
    {
        if ($key = $request->input('key')) {
            $data = $request->session()->get('confirm_data' . $key);
            return view("admin.students.create", compact("data"));
        } else {
            return view('admin.students.create');
        }
    }
    /**
     * Show view detail account of student
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getDataStudentById(
        Request $request,
        int $id
    ) {
        $student = $this->studentManagerService->findStudentById($id);
        if ($student) {
            return view('admin.students.detail', compact('student', 'id'));
        } else {
            return redirect()->route('admin.student_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
    }

    /**
     * Post data for create student
     *
     * @param CreateStudentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate(CreateStudentRequest $request) {
        $request->merge([
            'admin_id' => Auth::guard('admin')->user()->id
        ]);
        $dataPost = $request->all();
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect()->back()->with(['data' => $dataPost])->withErrors($request->validator);
        } else {
            (isset($dataPost['key']) && $dataPost['key']) ? $key = $dataPost['key'] : $key = Str::random(32);
            $request->session()->put('confirm_data' . $key, $dataPost);
            return redirect()->route('admin.confirmStudentAccount', ['key' => $key]);
        }
    }

    /**
     * Show view student creation confirmation
     *
     * @param \Illuminate\Http\Request $request
     * @param string $key
     * @return \Illuminate\Http\Response
     */
    public function confirmStudentAccount(
        Request $request,
        $key
    ) {
        $data = $request->session()->get('confirm_data' . $key);
        if ($data) {
            return view('admin.students.create_confirm', compact('data', 'key'));
        } else {
            return redirect()->route('admin.student_list');
        }
    }

    /**
     * Save create data of student
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveCreateStudent(Request $request)
    {
        $msg = '';
        $key = $request->input('key');
        $dataPosts = $request->session()->get('confirm_data' . $key);
        $createStudent = $this->studentManagerService->createStudent($dataPosts);
        if ($createStudent) {
            $msg = 'アカウント' . Config::get('message.common_msg.insert_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.create_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');
        return redirect()->route('admin.student_list')->with($result, $msg);
    }

    /**
     * Save image for student via ajax
     *
     * @param ImageRequest $request
     * @param string $img
     * @return \Illuminate\Http\Response
     */
    public function uploadImageStudent(
        ImageRequest $request
    ) {
        
        // $data = $request->all();
        $file = $request->file(['file']);
        if ($file) {
            $originalName = $file->getClientOriginalName();
            $ext = strtolower(pathinfo($originalName, PATHINFO_EXTENSION));
            $nameFile = date('YmdHis') . "_" . rand(10000, 99999) . "." . $ext;
            $file->storeAs('public/images/', $nameFile);
        }
        return response()->json(['data' => $nameFile]);
    }

    /**
     * Show view edit account of student
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function editStudent(
        Request $request,
        $id
    ) {
        $student = $this->studentManagerService->findStudentById($id);
        if (!$student) {
            return redirect()->route('admin.student_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        if ($key = $request->input('key')) {
            $student = $request->session()->get('confirm_data' . $key);
        }
        return view("admin.students.edit", compact("student", "id"));
    }

    /**
     * Post data for edit student
     *
     * @param EditStudentRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function postEdit(
        EditStudentRequest $request,
        $id
    ) {
        $request->merge([
            'admin_id' => Auth::guard('admin')->user()->id
        ]);
        $dataPost = $request->all();
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect()->back()->with(['student' => $dataPost])->withErrors($request->validator);
        } else {
            (isset($dataPost['key']) && $dataPost['key']) ? $key = $dataPost['key'] : $key = Str::random(32);
            $request->session()->put('confirm_data' . $key, $dataPost);
            return redirect()->route('admin.editConfirmStudent', ['key' => $key, 'id' => $id]);
        }
    }

    /**
     * Show view student edit confirmation
     *
     * @param \Illuminate\Http\Request $request
     * @param string $key
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function editConfirmStudent(
        Request $request,
        $key,
        int $id
    ) {
        if (!$this->studentManagerService->findStudentById($id)) {
            return redirect()->route('admin.student_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        $data = $request->session()->get('confirm_data' . $key);
        if ($data) {
            return view('admin.students.edit_confirm', compact('data', 'key', 'id'));
        } else {
            return redirect()->route('admin.student_detail', ['id' => $id]);
        }
    }

    /**
     * Save edit data of student
     *
     * @param \Illuminate\Http\Request $request
     * @param $idSchool
     * @return \Illuminate\Http\Response
     */
    public function saveEditStudent(
        Request $request,
        int $id
    ) {
        $key = $request->input('key');
        $dataPost = $request->session()->get('confirm_data' . $key);
        if (!$dataPost) {
            return redirect()->route('admin.student_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }

        $editStudent = $this->studentManagerService->editStudent($id, $dataPost);
        if ($editStudent) {
            $msg = 'アカウント' . Config::get('message.common_msg.update_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.edit_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');

        return redirect()->route('admin.student_list')->with($result, $msg);
    }

    /**
     * Delete data of student
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function deleteStudent(
        Request $request,
        int $id
    ) {
        if (!$this->studentManagerService->dataStudentById($id)) {
            return redirect()->route('admin.student_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        $delete = $this->studentManagerService->deleteStdent($id);
        if ($delete) {
            $msg = 'アカウント' . Config::get('message.common_msg.delete_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.delete_error');
            $result = ERROR;
        }
        return redirect()->route('admin.student_list')->with($result, $msg);
    }
}
