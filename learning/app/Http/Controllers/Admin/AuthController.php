<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\AuthLoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Auth\SendsPasswordResetEmails;

class AuthController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    
    /**
     * Show view login
     *
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
        return view("admin.auth.login");
    }

    /**
     * Post data login
     *
     * @param AuthLoginRequest $request
     * @return route
     */
    public function postLogin(AuthLoginRequest $request)
    {
        $dataPosts = $request->all();
        $data = [
            'email' => $dataPosts['email'],
            'password' =>  $dataPosts['password']
        ];

        if (Auth::guard('admin')->attempt($data)) {
            return redirect()->route('admin.account_manager_list');
        } else {
            return redirect()->back()->withInput()->with(ERROR, Config::get('message.common_msg.login_err'));
        }
    }

    /**
     * Logout
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View login
     */
    public function logout()
    {
        Auth::logout();
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

    /**
     * Show view forgot password
     *
     * @return \Illuminate\View\View
     */
    public function forgotPassword () {
        return view("admin.auth.forgot_password");
    }

    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $this->validate($request, [
            'email' => ['required','exists:admins,email'],
        ], [
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.exists' => 'メールアドレスがデータベースに存在しません. 正しく完全な電子メールアドレスを使用して再試行してください',
        ]);
    }

    /**
     * @return PasswordBroker
     */
    protected function broker() 
    {
        return Password::broker('admins');
    }
}
