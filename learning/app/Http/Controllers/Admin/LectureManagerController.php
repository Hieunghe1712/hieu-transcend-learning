<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DocumentsRequest;
use App\Http\Requests\Admin\ExplanationsRequest;
use App\Http\Requests\Admin\FileRequest;
use App\Http\Requests\Admin\HeadingsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Services\Admin\LectureService;
use App\Services\TrancendService;
use App\Http\Requests\Admin\ImageRequest;
use App\Http\Requests\Admin\LecturesRequest;
use App\Http\Requests\Admin\QuizRequest;
use App\Http\Requests\Admin\QuizValuesRequest;
use App\Services\Admin\DocumentsService;
use App\Services\Admin\ExplanationsService;
use App\Services\Admin\HeadingsService;
use App\Services\Admin\QuizOptionsService;
use App\Services\Admin\QuizService;
use App\Services\Admin\QuizValuesService;
use App\Services\Admin\UploadFilesService;
use Exception;
use Illuminate\Support\Facades\DB;

class LectureManagerController extends Controller
{

    /**
     * @var TrancendService $trancendService
     */
    protected $trancendService;

    /**
     * @var LectureService $lectureService
     */
    protected $lectureService;

    /**
     * @var QuizOptionsService $quizOptionsService
     */
    protected $quizOptionsService;

    /**
     * @var LecturesRequest $lecturesRequest
     */
    protected $lecturesRequest;

    /**
     * @var QuizRequest $quizRequest
     */
    protected $quizRequest;

    /**
     * @var QuizValuesRequest $quizValuesRequest
     */
    protected $quizValuesRequest;

    /**
     * @var ExplanationsRequest $explanationsRequest
     */
    protected $explanationsRequest;

    /**
     * @var HeadingsRequest $headingsRequest
     */
    protected $headingsRequest;

    /**
     * @var DocumentsRequest $documentsRequest
     */
    protected $documentsRequest;

    /**
     * @var QuizService $quizService
     */
    protected $quizService;

    /**
     * @var QuizValuesService $quizValuesService
     */
    protected $quizValuesService;

    /**
     * @var UploadFilesService $uploadFilesService
     */
    protected $uploadFilesService;

    /**
     * @var ExplanationsService $explanationsService
     */
    protected $explanationsService;

    /**
     * @var HeadingsService $headingsService
     */
    protected $headingsService;

    /**
     * @var DocumentsService $documentsService
     */
    protected $documentsService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        TrancendService $trancendService,
        LectureService $lectureService,
        QuizOptionsService $quizOptionsService,
        LecturesRequest $lecturesRequest,
        QuizRequest $quizRequest,
        QuizValuesRequest $quizValuesRequest,
        ExplanationsRequest $explanationsRequest,
        HeadingsRequest $headingsRequest,
        DocumentsRequest $documentsRequest,
        QuizService $quizService,
        QuizValuesService $quizValuesService,
        UploadFilesService $uploadFilesService,
        ExplanationsService $explanationsService,
        HeadingsService $headingsService,
        DocumentsService $documentsService
    ) {
        $this->trancendService = $trancendService;
        $this->lectureService = $lectureService;
        $this->quizOptionsService = $quizOptionsService;
        $this->lecturesRequest = $lecturesRequest;
        $this->quizRequest = $quizRequest;
        $this->quizValuesRequest = $quizValuesRequest;
        $this->explanationsRequest = $explanationsRequest;
        $this->headingsRequest = $headingsRequest;
        $this->documentsRequest = $documentsRequest;
        $this->quizService = $quizService;
        $this->quizValuesService = $quizValuesService;
        $this->uploadFilesService = $uploadFilesService;
        $this->explanationsService = $explanationsService;
        $this->headingsService = $headingsService;
        $this->documentsService = $documentsService;
        $this->middleware('CheckAdminLogin');
    }

    /**
     * Show view lecture list
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $params = $request->input();
        $lectures = $this->lectureService->list($params);
        return view("admin.lectures.list", compact("lectures"));
    }

    /**
     * Show view my lecture list
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function myList(Request $request)
    {
        $params = $request->input();
        $params['login_id'] = Auth::guard('admin')->user()->id;
        $listLectures = $this->lectureService->getMyListLectures($params);
        return view("admin.lectures.my_list", compact("listLectures"));
    }

    /**
     * Show view create lecture
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function createForm(Request $request)
    {
        $quizOptions = $this->quizOptionsService->getListQuizOptions();
        $key = "";
        $data=[];
        if ($key = $request->input('key')) {
            $data = $request->session()->get('confirm_data' . $key);
        }
        return view("admin.lectures.create", compact("data", "quizOptions", "key"));
    }

    /**
     * Get data lecture by id
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function editLectureForm(
        Request $request,
        $id
    ){
        $lecture = $this->lectureService->findLectureById($id);
        $quizOptions = $this->quizOptionsService->getListQuizOptions();
        $key = "";
        $data=[];
        if ($lecture) {
            if ($key = $request->input('key')) {
                $data = $request->session()->get('confirm_data' . $key);
            } else {
                $data = $this->getDataLectureEdit($lecture);
            }
            return view('admin.lectures.edit', compact('data', 'quizOptions', 'key', 'id'));
        } else {
            return redirect()->route('admin.my_lecture_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
    }

    /**
     * Get data edit lecture
     *
     * @param $lecture
     * @return route
     */
    public function getDataLectureEdit($lecture)
    {
        $data = [];
        $data['name'] = $lecture->name;
        $data['embed_code'] = $lecture->embed_code;
        $data['ifrem_code'] = $lecture->ifrem_code;
        $data['question_url'] = $lecture->question_url;
        $data['test_url'] = $lecture->test_url;
        $data['homework_url'] = $lecture->homework_url;
        $data['explanatory_text'] = $this->converBrToNl($lecture->explanatory_text);

        //get data quizzes
        $quizzes = [];
        if ($lecture->quizzes) {
            foreach ($lecture->quizzes as $quiz) {
                $arrQuiz = [];
                $arrQuiz['index_quiz'] = $quiz['sort_number'];
                $arrQuiz['title'] = $quiz['title'];
                $arrQuiz['question_sentence'] = $this->converBrToNl($quiz['question_sentence']);
                $arrQuiz['quiz_option_id'] = $quiz['quiz_option_id'];
                $arrQuiz['remark'] = $this->converBrToNl($quiz['remark']);
                $arrQuiz['appearance_time'] = $quiz['appearance_time'];
                $arrQuiz['choices'] = [];
                if ($quiz->quiz_values) {
                    foreach ($quiz->quiz_values as $value) {
                        $arrChoice = [];
                        $arrChoice['name'] = $value['name'];
                        $arrChoice['index_value'] = $value['sort_number'];
                        $arrChoice['index_quiz'] = $quiz['sort_number'];
                        array_push($arrQuiz['choices'], $arrChoice);
                    }
                }
                array_push($quizzes, $arrQuiz);
            }
        }
        $data['quizzes'] = json_encode($quizzes);

        //get data explanation
        $explanations = [];
        if ($lecture->explanations) {
            foreach ($lecture->explanations as $explanation) {
                $arrExplanation = [];
                $arrExplanation['index_explanation'] = $explanation['sort_number'];
                $arrExplanation['title'] = $explanation['title'];
                $arrExplanation['content'] = $this->converBrToNl($explanation['content']);
                $arrExplanation['file_name'] = "";
                $arrExplanation['original_file_name'] = "";
                $arrExplanation['dir_file'] = "";
                if ($explanation->uploaded_files) {
                    $pathFile = pathinfo($explanation->uploaded_files->uploaded_path);
                    $arrExplanation['original_file_name'] = $explanation->uploaded_files->name;
                    $arrExplanation['dir_file'] = $explanation->uploaded_files->uploaded_path;
                    $arrExplanation['file_name'] = $pathFile['basename'];
                }
                array_push($explanations, $arrExplanation);
            }
        }
        $data['explanations'] = json_encode($explanations);

        //get data headings
        $headings = [];
        if ($lecture->headings) {
            foreach ($lecture->headings as $heading) {
                $arrHeading = [];
                $arrHeading['index_headings'] = $heading['sort_number'];
                $arrHeading['title'] = $heading['title'];
                $arrHeading['time'] = $heading['heading_time'];
                array_push($headings, $arrHeading);
            }
        }
        $data['headings'] = json_encode($headings);

        //get data documents
        $documents=[];
        if ($lecture->documents) {
            foreach ($lecture->documents as $document) {
                $arrDocument = [];
                $arrDocument['index_documents'] = $document['sort_number'];
                $arrDocument['title'] = $document['title'];
                $arrDocument['file_name'] = "";
                $arrDocument['original_file_name'] = "";
                $arrDocument['dir_file'] = "";
                if ($document->uploaded_files) {
                    $pathFile = pathinfo($document->uploaded_files->uploaded_path);
                    $arrDocument['original_file_name'] = $document->uploaded_files->name;
                    $arrDocument['dir_file'] = $document->uploaded_files->uploaded_path;
                    $arrDocument['file_name'] = $pathFile['basename'];
                    $arrDocument['created_at'] =  date("Y-m-d H:i:s",strtotime($document->created_at));
                }
                array_push($documents, $arrDocument);
            }
        }
        $data['documents'] = json_encode($documents);

        return $data;
    }


    public function converBrToNl($text)
    {
        $breaks = array("<br />","<br>","<br/>");
        return str_ireplace($breaks, "", $text);
    }
    /**
     * Show view confirm edit lecture
     *
     * @param \Illuminate\Http\Request $request
     * @param $key
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function editConfirmLecture(
        Request $request,
        $id,
        $key
    ){
        if (!$this->lectureService->findLectureById($id)) {
            return redirect()->route('admin.my_lecture_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        $data = $request->session()->get('confirm_data' . $key);
        $quizOptions = $this->quizOptionsService->getListQuizOptions();
        if ($data) {
            return view('admin.lectures.edit_confirm', compact('data', 'key', 'id', 'quizOptions'));
        } else {
            return redirect()->route('admin.my_lecture_list', ['id' => $id]);
        }
    }

    /**
     * Get form quiz
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getFormAddQuiz(Request $request)
    {
        $data = $request->all();
        $numberOfQuiz = $data['num'];
        $quizOptions = $this->quizOptionsService->getListQuizOptions();
        $view = view('admin.lectures.form.quiz', compact('quizOptions', 'numberOfQuiz'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * Get form explanation
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getFormExplanation(Request $request)
    {
        $data = $request->all();
        $numberOfExplanation = $data['num'];
        $view = view('admin.lectures.form.explanation', compact('numberOfExplanation'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * Get form headings
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getFormHeadings(Request $request)
    {
        $data = $request->all();
        $numberOfHeadings = $data['num'];
        $view = view('admin.lectures.form.headings', compact('numberOfHeadings'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * Get form headings
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getFormDocuments(Request $request)
    {
        $data = $request->all();
        $numberOfDocuments = $data['num'];
        $view = view('admin.lectures.form.documents', compact('numberOfDocuments'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * Post data for create lecture
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function createLectures(Request $request)
    {
        $dataPost = $request->all();
        (isset($dataPost['key']) && $dataPost['key']) ? $key = $dataPost['key'] : $key = Str::random(32);
        $errMes = [];
        $result = true;

        $validationDataLeture = $this->validationDataLeture($dataPost);
        $errMes = $validationDataLeture['errMes'];
        $result = $validationDataLeture['result'];

        if ($result == true) {
            $request->session()->put('confirm_data' . $key, $dataPost);
        }


        return response()->json([
            'result' => $result,
            'errMes' => $errMes,
            'key' => $key,
        ]);
    }

    /**
    * Show view confirm create lecture
    *
    * @param $dataPost
    * @return \Illuminate\Http\Response
    */
    public function validationDataLeture($dataPost)
    {
        $errMes = [];
        $result = true;
        $dataLecture = [];
        $dataLecture['name'] = $dataPost['name'];
        $dataLecture['embed_code'] = $dataPost['embed_code'];
        $dataLecture['ifrem_code'] = $dataPost['ifrem_code'];
        $dataLecture['explanatory_text'] = $dataPost['explanatory_text'];
        $dataLecture['question_url'] = $dataPost['question_url'];
        $dataLecture['test_url'] = $dataPost['test_url'];
        $dataLecture['homework_url'] = $dataPost['homework_url'];
        if (isset($dataPost['documents']) && !empty($dataPost['documents'])) {
            $dataDocments = json_decode($dataPost['documents'], true);
            if (!empty($dataDocments)) {
                $lectureValidator = Validator::make($dataLecture, $this->lecturesRequest->rules(), $this->lecturesRequest->messages());
            } else {
                $lectureValidator = Validator::make($dataLecture, $this->lecturesRequest->rules_without_explanatory_text(), $this->lecturesRequest->messages());
            }
        }

        //Lectures validation
        if ($lectureValidator->fails()) {
            $result = false;
            $lectureErr  = $lectureValidator->errors()->get('*');
            foreach ($lectureErr as $nameField => $lectureError) {
                $errMes[$nameField] = $lectureErr[$nameField][0];
            }
        }

        //Quizzes validation
        if (isset($dataPost['quizzes']) && !empty($dataPost['quizzes'])) {
            $validationQuiz = $this->validationQuizzes($dataPost, $errMes);
            if ($validationQuiz['result'] == false) {
                $result = false;
                $errMes = array_merge($errMes, $validationQuiz['errMes']);
            }
        }

        //Explanations validation
        if (isset($dataPost['explanations']) && !empty($dataPost['explanations'])) {
            $dataExplanations = json_decode($dataPost['explanations'], true);
            foreach ($dataExplanations as $dataExplanation) {
                $explanationValidator = Validator::make($dataExplanation, $this->explanationsRequest->rules(), $this->explanationsRequest->messages());
                if ($explanationValidator->fails()) {
                    $result = false;
                    $explanationErrs  = $explanationValidator->errors()->get('*');
                    foreach ($explanationErrs as $nameField => $explanationErr) {
                        $errMes["explanation_" . $nameField. "-".  $dataExplanation['index_explanation']] = $explanationErrs[$nameField][0];
                    }
                }
            }
        }

        //Headings validation
        if (isset($dataPost['headings']) && !empty($dataPost['headings'])) {
            $dataHeadings = json_decode($dataPost['headings'], true);
            $dataTimeHeadings = [];
            foreach ($dataHeadings as $dataHeading) {
                $headingsValidator = Validator::make($dataHeading, $this->headingsRequest->rules(), $this->headingsRequest->messages());
                if ($headingsValidator->fails()) {
                    $result = false;
                    $headingsErrs  = $headingsValidator->errors()->get('*');
                    foreach ($headingsErrs as $nameField => $headingErr) {
                        $errMes["heading_" . $nameField. "-".  $dataHeading['index_headings']] = $headingsErrs[$nameField][0];
                    }
                } else {
                    $durationVideo = intval($dataPost['duration_embed']);
                    $headingTime = $dataHeading['time'];

                    $headingTime = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $headingTime);

                    sscanf($headingTime, "%d:%d:%d", $hours, $minutes, $seconds);

                    $index_headingsSeconds = $hours * 3600 + $minutes * 60 + $seconds;
                    // dd($index_headingsSeconds);
                    // if ($index_headingsSeconds > $durationVideo) {
                    //     $result = false;
                    //     $errMes["heading_time-" . $dataHeading['index_headings']] = '時間'. Config::get('message.validation_common_msg.format_err');
                    // }
                    if (!isset($errMes["heading_time-" . $dataHeading['index_headings']])) {
                        if (!in_array($dataHeading['time'], $dataTimeHeadings)) {
                            array_push($dataTimeHeadings, $dataHeading['time']);
                        } else {
                            if (!empty($dataTimeHeadings)) {
                                $result = false;
                                $errMes["heading_time-" . $dataHeading['index_headings']] = sprintf(__(Config::get('message.validation_common_msg.already_exists')), "時間");
                            }
                        }
                    }
                }
            }
        }

        //Documents validation
        if (isset($dataPost['documents']) && !empty($dataPost['documents'])) {
            $dataDocments = json_decode($dataPost['documents'], true);
            foreach ($dataDocments as $dataDocument) {
                $documentsValidator = Validator::make($dataDocument, $this->documentsRequest->rules(), $this->documentsRequest->messages());
                if ($documentsValidator->fails()) {
                    $result = false;
                    $documentsErr  = $documentsValidator->errors()->get('*');
                    foreach ($documentsErr as $nameField => $documentErr) {
                        $errMes["documents_" . $nameField. "-".  $dataDocument['index_documents']] = $documentsErr[$nameField][0];
                    }
                }
            }
        }

        return [
            'errMes' => $errMes,
            'result' => $result
        ];
    }

    /**
    * Show view confirm create lecture
    *
    * @param \Illuminate\Http\Request $request
    * @param $key
    * @return \Illuminate\Http\Response
    */
    public function createConfirmLectures(
        Request $request,
        $key
    ){
        $quizOptions = $this->quizOptionsService->getListQuizOptions();
        $data = $request->session()->get('confirm_data' . $key);
        if ($data) {
            return view('admin.lectures.create_confirm', compact('data', 'key', 'quizOptions'));
        } else {
            return redirect()->route('admin.my_lecture_list');
        }
    }

    /**
     * Save create lecture
     *
     * @param \Illuminate\Http\Request $request
     * @param $key
     * @return \Illuminate\Http\Response
     */
    public function saveCreateLecture(
        Request $request,
        $key
    ){
        $msg = '';
        $result = SUCCESS;
        $dataPost = $request->session()->get('confirm_data' . $key);
        DB::beginTransaction();
        $error = 0;
        try {
            //Save db lectures
            $dataLecture = [];
            $maxId = $this->lectureService->getMaxId();
            if ($maxId) {
                $maxId = $maxId + 1;
            } else {
                $maxId = 1;
            }
            $no = "L-" . str_pad($maxId, 6, '0', STR_PAD_LEFT);
            $dataLecture['name'] = $dataPost['name'];
            $dataLecture['no'] = $no;
            $dataLecture['embed_code'] = $dataPost['embed_code'];
            $dataLecture['ifrem_code'] = $dataPost['ifrem_code'];
            $dataLecture['question_url'] = $dataPost['question_url'];
            $dataLecture['test_url'] = $dataPost['test_url'];
            $dataLecture['homework_url'] = $dataPost['homework_url'];
            $dataLecture['thumbnail_url'] = $dataPost['thumbnail_url'];
            $dataLecture['explanatory_text'] = null;
            if (isset($dataPost['documents']) && !empty($dataPost['documents'])) {
                $dataLecture['explanatory_text'] = nl2br($dataPost['explanatory_text']);
            }
            $createLecture = $this->lectureService->createLecture($dataLecture);
            if (!($createLecture['result'])) {
                $error++;
            }

            //Save quizzes
            if ($error == 0) {
                if (isset($dataPost['quizzes']) && !empty($dataPost['quizzes'])) {
                    $error = $this->saveDataQuizzes($dataPost, $createLecture);
                }
            }

            //Save explanation
            if ($error == 0) {
                if (isset($dataPost['explanations']) && !empty($dataPost['explanations'])) {
                    $error = $this->saveDataExplanations($dataPost, $createLecture);
                }
            }

            //Save headings
            if ($error == 0) {
                if (isset($dataPost['headings']) && !empty($dataPost['headings'])) {
                    $error = $this->saveDataHeadings($dataPost, $createLecture);
                }
            }

            //Save documents
            if ($error == 0) {
                if (isset($dataPost['documents']) && !empty($dataPost['documents'])) {
                    $error = $this->saveDataDocuments($dataPost, $createLecture);
                }
            }

            //commit DB
            if ($error == 0) {
                DB::commit();
                $msg = 'アカウント' . Config::get('message.common_msg.insert_msg');
                $result = SUCCESS;
            } else {
                DB::rollback();
                $msg = Config::get('message.common_msg.create_error');
                $result = ERROR;
            }
        } catch (Exception $e) {
            DB::rollback();
        }

        $request->session()->forget('confirm_data' . $key);
        return redirect()->route('admin.my_lecture_list')->with($result, $msg);
    }

    /**
    * Edit lecture
    *
    * @param \Illuminate\Http\Request $request
    * @param $id
    * @return route
    */
    public function editLectures(
        Request $request,
        $id
    ){
        $dataPost = $request->all();
        (isset($dataPost['key']) && $dataPost['key']) ? $key = $dataPost['key'] : $key = Str::random(32);
        $errMes = [];
        $result = true;

        $validationDataLeture = $this->validationDataLeture($dataPost);
        $errMes = $validationDataLeture['errMes'];
        $result = $validationDataLeture['result'];

        if ($result == true) {
            $request->session()->put('confirm_data' . $key, $dataPost);
        }

        return response()->json([
            'result' => $result,
            'errMes' => $errMes,
            'key' => $key,
        ]);
    }

    /**
     * Save edit lecture
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @param $key
     * @return \Illuminate\Http\Response
     */
    public function saveEditLecture(
        Request $request,
        $id,
        $key
    ){
        $msg = '';
        $result = SUCCESS;
        $dataPost = $request->session()->get('confirm_data' . $key);
        $lecture = $this->lectureService->findLectureById($id);
        if (!$lecture) {
            return redirect()->route('admin.my_lecture_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        DB::beginTransaction();
        $error = 0;
        try {
            //Save db lectures
            $dataLecture = [];
            $no = "L-" . str_pad($dataPost['id'], 6, '0', STR_PAD_LEFT);
            $dataLecture['id'] = $dataPost['id'];
            $dataLecture['no'] = $no;
            $dataLecture['name'] = $dataPost['name'];
            $dataLecture['embed_code'] = $dataPost['embed_code'];
            $dataLecture['ifrem_code'] = $dataPost['ifrem_code'];
            $dataLecture['question_url'] = $dataPost['question_url'];
            $dataLecture['test_url'] = $dataPost['test_url'];
            $dataLecture['homework_url'] = $dataPost['homework_url'];
            $dataLecture['thumbnail_url'] = $dataPost['thumbnail_url'];
            $dataLecture['explanatory_text'] = null;
            if (isset($dataPost['documents']) && !empty($dataPost['documents'])) {
                $dataLecture['explanatory_text'] = nl2br($dataPost['explanatory_text']);
            }
            $editLeture = $this->lectureService->editLecture($id, $dataLecture);

            if ($editLeture) {
                if (!($editLeture['result'])) {
                    $error++;
                }
            } else {
                $error++;
            }

            //Remove datas
            if ($error == 0) {
                $error = $this->removeDataOfLecture($lecture);
            }
            //Save quizzes
            if ($error == 0) {
                if (isset($dataPost['quizzes']) && !empty($dataPost['quizzes'])) {
                    $error = $this->saveDataQuizzes($dataPost, $editLeture);
                }
            }

            //Save explanation
            if ($error == 0) {
                if (isset($dataPost['explanations']) && !empty($dataPost['explanations'])) {
                    $error = $this->saveDataExplanations($dataPost, $editLeture);
                }
            }

            if ($error == 0) {
                if (isset($dataPost['headings']) && !empty($dataPost['headings'])) {
                    $error = $this->saveDataHeadings($dataPost, $editLeture);
                }
            }

            //Save documents
            if ($error == 0) {
                if (isset($dataPost['documents']) && !empty($dataPost['documents'])) {
                    $error = $this->saveDataDocuments($dataPost, $editLeture);
                }
            }

            //commit DB
            try {
                if ($error == 0) {
                    DB::commit();
                    $msg = 'アカウント' . Config::get('message.common_msg.update_msg');
                    $result = SUCCESS;
                } 
            }
            catch (\Exception $e){
                DB::rollback();
                $msg = Config::get('message.common_msg.edit_error');
                $result = ERROR;
                \Log::error($e);
            }
        } catch (Exception $e) {
            DB::rollback();
            \Log::error($e);
        }

        $request->session()->forget('confirm_data' . $key);
        return redirect()->route('admin.my_lecture_list')->with($result, $msg);
    }

    /**
     * Save data quizzes
     *
     * @param $lecture
     * @return \Illuminate\Http\Response
     */
    public function removeDataOfLecture($lecture)
    {
        $error = 0;
        //Delete data quizzes
        if ($lecture->quizzes) {
            foreach ($lecture->quizzes as $quiz) {
                $delQuiz = $this->quizService->hardDeleteQuiz($quiz->id);
                if (!$delQuiz) {
                    $error++;
                }
                if ($error == 0) {
                    if ($quiz->quiz_values) {
                        foreach ($quiz->quiz_values as $value) {
                            $delQuizValue = $this->quizValuesService->hardDeleteQuizValues($value->id);
                            if (!$delQuizValue) {
                                $error++;
                            }
                        }
                    }
                }
            }
        }

        //Delete data explanations
        if ($error == 0) {
            if ($lecture->explanations) {
                foreach ($lecture->explanations as $explanation) {
                    $delExplantion = $this->explanationsService->hardDeleteExplanation($explanation->id);
                    if (!$delExplantion) {
                        $error++;
                    }
                    if ($explanation->uploaded_files) {
                        $delUploadedFile = $this->uploadFilesService->hardDeleteUploadedFile($explanation->uploaded_files->id);
                        if (!$delUploadedFile) {
                            $error++;
                        }
                    }
                }
            }
        }

        //Delete data headings
        if ($error == 0) {
            if ($lecture->headings) {
                foreach ($lecture->headings as $heading) {
                    $delHeadings = $this->headingsService->hardDeleteHeadings($heading->id);
                    if (!$delHeadings) {
                        $error++;
                    }
                }
            }
        }

        if ($error == 0) {
            if ($lecture->documents) {
                foreach ($lecture->documents as $documents) {
                    $delDocuments = $this->documentsService->hardDeleteDocuments($documents->id);
                    if (!$delDocuments) {
                        $error++;
                    }
                    if ($documents->uploaded_files) {
                        $delUploadedFile = $this->uploadFilesService->hardDeleteUploadedFile($documents->uploaded_files->id);
                        if (!$delUploadedFile) {
                            $error++;
                        }
                    }
                }
            }
        }

        return $error;
    }

    /**
     * Save data quizzes
     *
     * @param $dataPost
     * @param $lecture
     * @return \Illuminate\Http\Response
     */
    public function saveDataQuizzes(
        $dataPost,
        $lecture
    ){
        $error = 0;

        $dataQuizzes = json_decode($dataPost['quizzes'], true);
        foreach ($dataQuizzes as $quiz) {
            $dataQuiz = [];
            $dataQuiz['lecture_id'] = $lecture['lecture']->id;
            $dataQuiz['sort_number'] = $quiz['index_quiz'];
            $dataQuiz['title'] = $quiz['title'];
            $dataQuiz['question_sentence'] = nl2br($quiz['question_sentence']);
            $dataQuiz['quiz_option_id'] = $quiz['quiz_option_id'];
            $dataQuiz['remark'] = nl2br($quiz['remark']);
            $dataQuiz['appearance_time'] = $quiz['appearance_time'];
            $createQuiz = $this->quizService->createQuiz($dataQuiz);
            if (!($createQuiz['result'])) {
                $error++;
            }

            //Save quiz values
            if ($error == 0) {
                $quizOption = $this->quizOptionsService->getQuizOptionById($quiz['quiz_option_id']);
                if ($quizOption->have_choices == QUIZ_HAVE_CHOICE_1) {
                    foreach ($quiz['choices'] as $choice) {
                        $dataQuizValues = [];
                        $dataQuizValues['quiz_id'] = $createQuiz['quiz']->id;
                        $dataQuizValues['name'] = $choice['name'];
                        $dataQuizValues['sort_number'] = $choice['index_value'];
                        $createQuizValues = $this->quizValuesService->createQuizValues($dataQuizValues);
                        if (!($createQuizValues['result'])) {
                            $error++;
                        }
                    }
                }
            }
        }

        return $error;
    }

    /**
     * Save data explanations
     *
     * @param $dataPost
     * @param $lecture
     * @return \Illuminate\Http\Response
     */
    public function saveDataExplanations(
        $dataPost,
        $lecture
    ){
        $error = 0;
        $dataExplanations = json_decode($dataPost['explanations'], true);

        foreach ($dataExplanations as $explanation) {
            if ($explanation['file_name'] && $explanation['original_file_name'] && $explanation['dir_file']) {
                $pathSaveFile = LECTURES_EXPLANTION_SAVE . $explanation['file_name'];
                $pathTmp = LECTURES_EXPLANTION_SAVE_TMP . $explanation['file_name'];
                if (!@file_exists($pathSaveFile)) {
                    if (!(@file_exists($pathTmp)) || !(@rename($pathTmp, $pathSaveFile))) {
                        $error++;
                    }
                }
                //save upload file
                if ($error == 0) {
                    $dataUploadFile = [];
                    $dataUploadFile['name'] = $explanation['original_file_name'];
                    $dataUploadFile['uploaded_path'] = LECTURES_EXPLANTION_SAVE_JS . $explanation['file_name'];
                    $createUploadFiles = $this->uploadFilesService->saveCreateUploadFiles($dataUploadFile);
                    if (!($createUploadFiles['result'])) {
                        $error++;
                    }
                }

                //save explanation
                if ($error == 0) {
                    $dataExplanation = [];
                    $dataExplanation['lecture_id'] =  $lecture['lecture']->id;
                    $dataExplanation['sort_number'] =  $explanation['index_explanation'];
                    $dataExplanation['title'] =  $explanation['title'];
                    $dataExplanation['content'] =  nl2br($explanation['content']);
                    $dataExplanation['file_id'] =  $createUploadFiles['uploadFiles']->id;
                    $createExplantions = $this->explanationsService->createExplanations($dataExplanation);
                    if (!($createExplantions['result'])) {
                        $error++;
                    }
                }
            }
        }
        return $error;
    }

    /**
     * Save data headings
     *
     * @param $dataPost
     * @param $lecture
     * @return \Illuminate\Http\Response
     */
    public function saveDataHeadings(
        $dataPost,
        $lecture
    ){
        $error = 0;
        $dataHeadings = json_decode($dataPost['headings'], true);
        foreach ($dataHeadings as $heading) {
            $dataHeading = [];
            $dataHeading['lecture_id'] = $lecture['lecture']->id;
            $dataHeading['title'] = $heading['title'];
            $dataHeading['heading_time'] = $heading['time'];
            $dataHeading['sort_number'] = $heading['index_headings'];
            $createHeadings = $this->headingsService->createHeadings($dataHeading);
            if (!($createHeadings['result'])) {
                $error++;
            }
        }
        return $error;
    }

    /**
     * Save data documents
     *
     * @param $dataPost
     * @param $lecture
     * @return \Illuminate\Http\Response
     */
    public function saveDataDocuments(
        $dataPost,
        $lecture
    ){
        $error = 0;
        $allFiles = [];
        $dataDocuments = json_decode($dataPost['documents'], true);
        foreach ($dataDocuments as $document) {
            if ($document['file_name'] && $document['original_file_name'] && $document['dir_file']) {
                //save uploadFile
                if ($error == 0) {
                    $dataUploadFile = [];
                    $dataUploadFile['name'] = $document['original_file_name'];
                    $dataUploadFile['uploaded_path'] = LECTURES_DOCUMENT_SAVE_JS .$lecture['lecture']->id . "/" . $document['file_name'];
                    $dataUploadFile['created_at'] = $document['created_at'];
                    $createUploadFiles = $this->uploadFilesService->saveCreateUploadFiles($dataUploadFile);
                    if (!($createUploadFiles['result'])) {
                        $error++;
                    } else {
                        array_push($allFiles, $document['file_name']);
                    }
                }

                //save document
                if ($error == 0) {
                    $dataDocument = [];
                    $dataDocument['lecture_id'] = $lecture['lecture']->id;
                    $dataDocument['title'] = $document['title'];
                    $dataDocument['sort_number'] = $document['index_documents'];
                    $dataDocument['file_id'] = $createUploadFiles['uploadFiles']->id;
                    $dataDocument['created_at'] = $document['created_at'];
                    $createDocuments = $this->documentsService->createDocuments($dataDocument);
                    if (!($createDocuments['result'])) {
                        $error++;
                    }
                }
            }
        }

        if ($error == 0) {
            $folderFile = LECTURES_DOCUMENT_SAVE . $lecture['lecture']->id . "/";
            if (!is_dir($folderFile)) {
                if (!(@mkdir($folderFile, 0755, true))) {
                    $error++;
                }
            }
            foreach ($allFiles as $file) {
                $pathSaveFile = $folderFile . $file;
                $pathTmp = LECTURES_DOCUMENT_SAVE_TMP . $file;
                if ((@file_exists($pathTmp))) {
                    if (!(@rename($pathTmp, $pathSaveFile))) {
                        $error++;
                    }
                }
            }
            $files = glob($folderFile . "*");
            foreach ($files as $file) {
                $fileName = basename($file);
                if (!in_array($fileName, $allFiles)) {
                    if (is_file($file)) {
                        if (!@unlink($file)) {
                            $error++;
                        }
                    }
                }
            }
        }
        return $error;
    }

    /**
     * Validation Quizzes
     *
     * @param $dataPost
     * @return \Illuminate\Http\Response
     */
    public function validationQuizzes($dataPost)
    {
        $result = true;
        $errMes = [];
        $dataQuizzes = json_decode($dataPost['quizzes'], true);
        $dataTime = [];
        if (!empty($dataQuizzes)) {
            foreach ($dataQuizzes as $dataQuiz) {
                $quizValidator = Validator::make($dataQuiz, $this->quizRequest->rules(), $this->quizRequest->messages());
                if ($quizValidator->fails()) {
                    $result = false;
                    $quizErrs  = $quizValidator->errors()->get('*');
                    foreach ($quizErrs as $nameField => $quizErr) {
                        $errMes[$nameField. "-".  $dataQuiz['index_quiz']] = $quizErrs[$nameField][0];
                    }
                } else {
                    $durationVideo = intval($dataPost['duration_embed']);
                    $appearanceTime = $dataQuiz['appearance_time'];

                    $appearanceTime = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $appearanceTime);

                    sscanf($appearanceTime, "%d:%d:%d", $hours, $minutes, $seconds);

                    $appearanceTimeSeconds = $hours * 3600 + $minutes * 60 + $seconds;
                    // if ($appearanceTimeSeconds > $durationVideo) {
                    //     $result = false;
                    //     $errMes["appearance_time-" . $dataQuiz['index_quiz']] = '出現時間'. Config::get('message.validation_common_msg.format_err');
                    // }
                    if (!isset($errMes["appearance_time-" . $dataQuiz['index_quiz']])) {
                        if (!in_array($dataQuiz['appearance_time'], $dataTime)) {
                            array_push($dataTime, $dataQuiz['appearance_time']);
                        } else {
                            if (!empty($dataTime)) {
                                $result = false;
                                $errMes["appearance_time-" . $dataQuiz['index_quiz']] = sprintf(__(Config::get('message.validation_common_msg.already_exists')), "出現時間");
                            }
                        }
                    }
                }
                if ($dataQuiz['quiz_option_id']) {
                    $quizOption = $this->quizOptionsService->getQuizOptionById($dataQuiz['quiz_option_id']);
                    if ($quizOption->have_choices == QUIZ_HAVE_CHOICE_1) {
                        if ($dataQuiz['choices']) {
                            foreach ($dataQuiz['choices'] as $choice) {
                                $quizValuesValidator = Validator::make($choice, $this->quizValuesRequest->rules(), $this->quizValuesRequest->messages());
                                if ($quizValuesValidator->fails()) {
                                    $result = false;
                                    $quizValuesErrs  = $quizValuesValidator->errors()->get('*');
                                    foreach ($quizValuesErrs as $nameField => $quizErr) {
                                        $nameErr = $nameField . "_quiz_value-". $choice['index_quiz']."-".$choice['index_value'];
                                        $errMes[$nameErr] = $quizValuesErrs[$nameField][0];
                                    }
                                }
                            }
                        } else {
                            $errMes['quiz_values'] = '選択肢'. Config::get('message.validation_common_msg.input_err');
                        }
                    }
                }
            }
        }

        return [
            'result' => $result,
            'errMes' => $errMes
        ];
    }

    /**
     * Upload explanation file
     *
     * @param FileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function uploadExplanationFile(FileRequest $request)
    {
        return $this->trancendService->uploadFile($request, LECTURES_EXPLANTION_SAVE_TMP, LECTURES_EXPLANTION_SAVE_TMP_JS, true);
    }
    /**
     * Upload document file
     *
     * @param FileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function uploadDocumentsFile(FileRequest $request)
    {
        return $this->trancendService->uploadFile($request, LECTURES_DOCUMENT_SAVE_TMP, LECTURES_DOCUMENT_SAVE_TMP_JS, false);
    }

    /**
     * Get data lecture by id
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function detail(
        Request $request,
        $id
    ){
        $lecture = $this->lectureService->findLectureById($id);
        $quizzes = $this->lectureService->findQuizByLectureId($id);
        $explanations = $this->lectureService->findExplainByLectureId($id);
        $params = $request->input();
        $params['login_id'] = Auth::guard('admin')->user()->id;
        $myLectures = $this->lectureService->getMyListLectures($params);
        $headings = $this->lectureService->findHeadingByLectureId($id);
        $optionValues = $this->lectureService->findOptionValueByLectureId($id);
        if ($lecture) {
            return view('admin.lectures.detail', compact('lecture', 'quizzes', 'explanations', 'myLectures', 'headings', 'optionValues'));
        } else {
            return redirect()->route('admin.lecture_list')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
    }

    /**
     * Soft delete lecture
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function deleteLucture($id)
    {
        $lecture = $this->lectureService->findLectureById($id);
        if ($lecture) {
            $delete = $this->lectureService->deleteLecture($id);
            if ($delete) {
                $msg = 'マイ講義コンテンツ' . Config::get('message.common_msg.delete_msg');
                $result = SUCCESS;
            } else {
                $msg = Config::get('message.common_msg.delete_error');
                $result = ERROR;
            }
        } else {
            $msg = Config::get('message.common_msg.not_isset_id');
            $result = ERROR;
        }
        return redirect()->route('admin.my_lecture_list')->with($result, $msg);
    }
}
