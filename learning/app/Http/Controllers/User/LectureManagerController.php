<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\User\HistoriesService;
use Illuminate\Http\Request;
use App\Services\User\LectureService;
use App\Services\User\LikesService;
use App\Services\User\MemosService;
use App\Services\User\MyListService;
use App\Services\User\ViewsService;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Response;
use File;
use ZipArchive;
use Carbon\Carbon;
use App\Http\Requests\User\QuizRequest;
class LectureManagerController extends Controller
{

    /**
     * @var LectureService $lectureService
     */
    protected $lectureService;

    /**
     * @var MemosService $memoService
     */
    protected $memoService;

    /**
     * @var MyListService $mylistService
     */
    protected $myListService;

     /**
     * @var LikesService $likesService
     */
    protected $likesService;

     /**
     * @var ViewsService $viewsService
     */
    protected $viewsService;

     /**
     * @var HistoriesService $historiesService
     */
    protected $historiesService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        LectureService $lectureService,
        MemosService $memoService,
        MyListService $myListService,
        LikesService $likesService,
        ViewsService $viewsService,
        HistoriesService $historiesService
    ) {
        $this->lectureService = $lectureService;
        $this->memoService = $memoService;
        $this->myListService = $myListService;
        $this->likesService = $likesService;
        $this->viewsService = $viewsService;
        $this->historiesService = $historiesService;
        // $this->middleware('checkLoginUser');
    }

    /**
     * Show view my lecture list
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function myList(Request $request)
    {
        $params = $request->input();
        $params['login_id'] = Auth::user()->id;
        $listLectures = $this->lectureService->getMyListLectures($params);
        return view("user.lectures.my_list", compact("listLectures"));
    }

    /**
     * search lecture
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $params = $request->input();
        $params['login_id'] = Auth::user()->id;
        $listLectures = $this->lectureService->searchLecture($params);
        return view("user.lectures.search", compact("listLectures"));
    }

    /**
     * Delete my list
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function deleteMyList(Request $request, $id)
    {
        $page = $request->page;
        if (!$this->lectureService->findMyListById($id)) {
            return redirect()->route('user.lectures.myList')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        if ($this->lectureService->deleteMyList($id)) {
            $msg = '私の講義' . Config::get('message.common_msg.delete_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.delete_error');
            $result = ERROR;
        }
        if (Auth::check() && Auth::user()->id == $id) {
            return redirect()->route('user.logout');
        }
        $params = $request->input();
        $params['login_id'] = Auth::user()->id;
        $listLectures = $this->lectureService->getMyListLectures($params);
        $allListLectures = $this->lectureService->getAllMyListLecture($params);
        if(count($allListLectures) % PAGINATE_LIMIT == 0) {
            $lastPage = $listLectures->lastPage();
            $url = route('user.lectures.myList').'?page='.$lastPage;
            return redirect($url)->with($result, $msg);
        }
        return redirect()->back()->with($result, $msg);

    }

    /**
     * Show lecture history list
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function lectureHistory(Request $request)
    {
        $params = $request->input();
        $params['login_id'] = Auth::user()->id;
        $listLectures = $this->lectureService->getLectureHistory($params);
        return view("user.lectures.history", compact("listLectures"));
    }

    /**
     * Get data lecture by id
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request, $id)
    {
        $lecture = $this->lectureService->findLectureById($id);

        if ($lecture) {
            $result = true;
            $lectureUpdateView = $this->lectureService->updateViewNumber($lecture->id);
            if (!$lectureUpdateView) {
                $result = false;
            }

            $userId = Auth::user()->id;

            $viewUser = $this->viewsService->getViewsByUserIdAndLectureId($lecture->id, $userId);
            if (!$viewUser) {
                $dataViewUser['lecture_id'] = $lecture->id;
                $dataViewUser['user_id'] = $userId;
                $createViewUser = $this->viewsService->addViewLecture($dataViewUser);
                if (!$createViewUser) {
                    $result = false;
                }
            }

            $date = Carbon::today()->toDateString();
            $historyUser = $this->historiesService->getHistoryByUserIdAndLectureId($userId, $lecture->id, $date);
            if (!$historyUser) {
                $dataHistoryUser['lecture_id'] = $lecture->id;
                $dataHistoryUser['user_id'] = $userId;
                $createHistoryUser = $this->historiesService->addHistoryLecture($dataHistoryUser);
                if (!$createHistoryUser) {
                    $result = false;
                }
            } else {
                $dataHistoryUser['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
                $updateHistoryUser = $this->historiesService->updateHistoryLecture($dataHistoryUser, $historyUser->id);
                if (!$updateHistoryUser) {
                    $result = false;
                }
            }

            if ($result) {
                $otherLecture = $this->lectureService->getRandomLecture(2, $id);
                $quizzes = $this->lectureService->findQuizByLectureId($id);
                $appearanceTime = $this->lectureService->getAppearanceTimeByLectureId($id);
                // dd($appearanceTime);
                $explanations = $this->lectureService->getExplainByLectureId($id);
                $answerUser = $this->lectureService->getAnswerByUserId(Auth::user()->id);
                $time_seconds = [];
                foreach($appearanceTime as $id => $time) {
                    sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);
                    $time_seconds[$id] = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
                }
                $time_seconds = json_encode($time_seconds);
                return view("user.lectures.detail", compact('lecture', 'otherLecture', 'quizzes', 'time_seconds', 'explanations', 'answerUser'));
            } else {
                return redirect()->route('user.index')->with(ERROR, Config::get('message.common_msg.edit_error'));
            }

        } else {
            return redirect()->route('user.index')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
    }

    /**
     * add Memo
     *
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function addMemo(Request $request)
    {
        $dataPost = $request->all();
        $data['comment']= $dataPost['comment'];
        $data['lesson_id']= $dataPost['lesson_id'];
        $data['user_id']=  Auth::user()->id;
        $data['time_memo']= $dataPost['time_memo'];

        $maxSortnumber = $this->memoService->getMaxSortNumberByLessonId($dataPost['lesson_id']);
        if (!$maxSortnumber) {
            $maxSortnumber = 1;
        } else {
            $maxSortnumber = $maxSortnumber + 1;
        }
        $data['sort_number'] = $maxSortnumber;
        $createMemo = $this->memoService->createMemo($data);
        if ($createMemo) {
            $msg = 'メモ' . Config::get('message.common_msg.insert_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.create_error');
            $result = ERROR;
        }

        return response()->json(['result' => $result, 'msg' => $msg, 'data'=> $data]);
    }

    /**
     * add or remove to my list
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addOrRemoveToMyList(Request $request)
    {
        $dataPost = $request->all();
        $data['lecture_id']= $dataPost['lecture_id'];
        $data['user_id']= Auth::user()->id;
        $remove = false;

        $dataOldMyList = $this->myListService->getMyListByUserIdAndLectureId($data['user_id'], $data['lecture_id']);
        if ($dataOldMyList) {
            $remove = true;
            $dataSave = $this->myListService->removeToMyList($dataOldMyList->id);
        } else {
            $remove = false;
            $dataSave = $this->myListService->addToMyList($data);
        }

        if ($dataSave) {
            if ($remove == false) {
                $msg =  Config::get('message.common_msg.add_to_my_list_success');
            } else {
                $msg =  Config::get('message.common_msg.remove_to_my_list_success');
            }
            $result = SUCCESS;
        } else {
            if ($remove == false) {
                $msg =  Config::get('message.common_msg.add_to_my_list_error');
            } else {
                $msg =  Config::get('message.common_msg.remove_to_my_list_error');
            }
            $result = ERROR;
        }

        return response()->json(['result' => $result, 'msg' => $msg, 'data'=> $data]);
    }

    /**
     * like lecture
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function likeLecture(Request $request)
    {
        $dataPost = $request->all();
        $data['lecture_id']= $dataPost['lecture_id'];
        $data['user_id']= Auth::user()->id;
        $dataOldLike = $this->likesService->getLikesByUserIdAndLectureId($data['user_id'], $data['lecture_id']);
        if ($dataOldLike) {
            $remove = true;
            $dataSave = $this->likesService->removeLikeLecture($dataOldLike->id);
        } else {
            $remove = false;
            $dataSave = $this->likesService->addLikeLecture($data);
        }
        $lectureUpdateLiked = $this->lectureService->updateLikedNumber($data['lecture_id'], $remove);

        if ($dataSave && $lectureUpdateLiked) {
            if ($remove == false) {
                $msg =  Config::get('message.common_msg.add_to_my_list_success');
            } else {
                $msg =  Config::get('message.common_msg.remove_to_my_list_success');
            }
            $result = SUCCESS;
        } else {
            if ($remove == false) {
                $msg =  Config::get('message.common_msg.add_to_my_list_error');
            } else {
                $msg =  Config::get('message.common_msg.remove_to_my_list_error');
            }
            $result = ERROR;
        }

        return response()->json(['result' => $result, 'msg' => $msg, 'data'=> $data, 'remove' => $remove]);
    }

    /**
     * Show lecture note list
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function lectureNote(Request $request)
    {
        $params = $request->input();
        $params['login_id'] = Auth::user()->id;
        $listLectures = $this->lectureService->getLectureNote($params);
        return view("user.lectures.note", compact("listLectures"));
    }

    /**
     * Delete my note
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function deleteMemo(Request $request, $id)
    {
        $page = $request->page;
        if (!$this->lectureService->findMemoById($id)) {
            return redirect()->route('user.lectures.lectureNote')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        if ($this->lectureService->deleteMemo($id)) {

            $msg = '講義ノート' . Config::get('message.common_msg.delete_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.delete_error');
            $result = ERROR;
        }
        if (Auth::check() && Auth::user()->id == $id) {
            return redirect()->route('user.logout');
        }

        $params = $request->input();
        $params['login_id'] = Auth::user()->id;
        $listLectureNote = $this->lectureService->getLectureNote($params);
        $allLectureNote = $this->lectureService->getAllLectureNote();
        if(count($allLectureNote) % PAGINATE_LIMIT == 0) {
            $lastPage = $listLectureNote->lastPage();
            $url = route('user.lectures.lectureNote').'?page='.$lastPage;
            return redirect($url)->with($result, $msg);
        }
        return redirect()->back()->with($result, $msg);
    }

    /**
     * Show lesson document
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function lessonDocument($id)
    {
        $listDocuments = $this->lectureService->getDocuments($id);
        $lecture = $this->lectureService->dataLectureById($id);
        return view("user.lectures.document", compact('listDocuments', 'lecture'));
    }

    /**
     * download document
     *
     * @param $file_name
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function downloadDocument($id, $file_name)
    {
        $filepath = public_path('uploads/lectures/documents/'.$id.'/'.$file_name);
        return Response::download($filepath);
    }

    /**
     * download all documents
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function downloadAllDocuments(Request $request, $id)
    {
        if($request->has('download')) {
            $zip      = new \ZipArchive;
            $fileName = 'attachment.zip';
            if(file_exists(public_path($fileName))) {
                unlink(public_path($fileName));
            }
            if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE) {
                $files = File::files(public_path(LECTURES_DOCUMENT_SAVE_JS . $id));
                foreach ($files as $key => $value) {
                    $relativeName = basename($value);
                    $zip->addFile($value, $relativeName);
                }
            $zip->close();
            }
            return response()->download(public_path($fileName));
        }

    }


    /**
     * save answer of quiz
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function saveAnswer(Request $request)
    {
        if (isset($request->validator) && $request->validator->fails())
        {
            return response()->json(['errors'=>$request->validator->errors()->all()]);
        }
        $dataPosts = $request->all();
        $saveAnswer = $this->lectureService->saveAnswer($dataPosts);
        if ($saveAnswer) {
            return response()->json(['success'=> $saveAnswer]);
        }
    }
}
