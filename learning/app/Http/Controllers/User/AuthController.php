<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\Auth\SendsPasswordResetEmails;
use App\Http\Requests\User\AuthLoginRequest;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{

    use SendsPasswordResetEmails;

    // public function __construct()
    // {
    //     $this->middleware('guest:web')->except('logout');
    // }
    /**
     * Show view login
     *
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
            return view("user.auth.login");
    }
    /**
     * Post data login
     *
     * @param AuthLoginRequest $request
     * @return route
     */
    public function postLogin(Request $request)
    {
        $dataPosts = $request->all();
        $data = [
            'email' => $dataPosts['email'],
            'password' =>  $dataPosts['password']
        ];
        //check data login
        if (Auth::attempt($data)) {
            return redirect()->route('user.index');
        } else {
            return redirect()->back()->withInput()->with(ERROR, Config::get('message.common_msg.login_err'));
        }
    }
    /**
     * Logout
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\View\View login
     */
    public function logout(Request $request)
    {
        Auth::logout();
        Auth::guard('web')->logout();
        return redirect()->route('user.index_login');
    }
    /**
     * Show view forgot password
     *
     * @return \Illuminate\View\View
     */
    public function forgotPassword () {
        return view("user.auth.forgot_password");
    }
    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $this->validate($request, [
            'email' => ['required','check_email_format','exists:users,email'],
        ], [
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.check_email_format' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.exists' => 'メールアドレスがデータベースに存在しません. 正しく完全な電子メールアドレスを使用して再試行してください',
        ]);
    }

    /**
     * @return PasswordBroker
     */
    protected function broker() {
        return Password::broker('users');
    }

}
