<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\User\EditProfileRequest;
use App\Http\Requests\Admin\ImageRequest;
use App\Services\TrancendService;
use App\Services\User\ProfileService;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    /**
     * @var TrancendService $trancendService
     */
    protected $trancendService;

    /**
     * @var profileService $trancendService
     */
    protected $profileService;

    public function __construct(
        TrancendService $trancendService,
        profileService $profileService
    ) {
        $this->trancendService = $trancendService;
        $this->profileService = $profileService;
    }

    /**
     * index profile
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $infoUser = Auth::user();
        return view('user.profile.index', compact('infoUser'));
    }

    /**
     * edit profile
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function editProfile(Request $request) {
        $data = Auth::guard('web')->user();
        if ($key = $request->input('key')) {
            $data = $request->session()->get('confirm_data' . $key);
        } else {
            $data['password'] = null;
        }
        return view('user.profile.edit', compact("data"));
    }

    /**
     * Post data for edit profile
     *
     * @param EditProfileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function postEditProfile(Request $request) {
        $dataPost = $request->all();
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect()->back()->with(['data' => $dataPost])->withErrors($request->validator);
        } else {
            (isset($dataPost['key']) && $dataPost['key']) ? $key = $dataPost['key'] : $key = Str::random(32);
            $request->session()->put('confirm_data' . $key, $dataPost);
            return redirect()->route('user.profile.editConfirm', ['key' => $key]);
        }
    }

     /**
     * Show view profile edit confirmation
     *
     * @param \Illuminate\Http\Request $request
     * @param string $key
     * @return \Illuminate\Http\Response
     */
    public function editConfirmProfile(
        Request $request,
        $key
    ) {
        $data = $request->session()->get('confirm_data' . $key);
        if ($data) {
            return view('user.profile.edit_confirm', compact('data', 'key'));
        } else {
            return redirect()->route('user.profile.index');
        }
    }

    /**
     * Save edit data of student
     *
     * @param \Illuminate\Http\Request $request
     * @param $idSchool
     * @return \Illuminate\Http\Response
     */
    public function saveEditProfile(
        Request $request
    ) {
        $key = $request->input('key');
        $dataPost = $request->session()->get('confirm_data' . $key);
        if (!$dataPost) {
            return redirect()->route('user.profile.index')->with(ERROR, Config::get('message.common_msg.not_isset_id'));
        }
        $editProfile = $this->profileService->editProfile($dataPost);
        if ($editProfile) {
            $msg = 'プロフィール' . Config::get('message.common_msg.update_msg');
            $result = SUCCESS;
        } else {
            $msg = Config::get('message.common_msg.edit_error');
            $result = ERROR;
        }
        $request->session()->forget('confirm_data');

        return redirect()->route('user.profile.index')->with($result, $msg);
    }

    /**
     * Save image for profile via ajax
     *
     * @param ImageRequest $request
     * @param string $img
     * @return \Illuminate\Http\Response
     */
    public function uploadImageProfile(Request $request, $img) {
        return $this->trancendService->uploadAvatar($request, $img, AVATAR_STUDENT_SAVE_TMP);
    }

}
