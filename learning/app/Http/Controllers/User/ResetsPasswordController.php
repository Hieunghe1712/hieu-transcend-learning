<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Auth\Events\PasswordReset;

/**
 * Description of ResetsPasswordController
 *
 * @author ADMIN
 */
class ResetsPasswordController extends Controller {

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     * @var string
     */
    protected $redirectTo = '/index';

    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * Create a new controller instance.
     * @return void
     */
    public function showResetForm(Request $request, $token = null)
    {
        $email = $request->email;
        return view('user.auth.recover_password', [
            'token' => $token,
            'email' => $email
        ]);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'email' => 'required',
            'token'    => 'required',
            'password' => 'required|min:8|max:128|check_password_format',
            'password_confirm' => 'required|same:password|check_password_format',
        ];
    }

    /**
     * Get the password reset validation error messages.
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [
            'email.required' => 'トークン'. Config::get('message.validation_common_msg.input_err'),
            'token.required' => 'トークン'. Config::get('message.validation_common_msg.input_err'),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.max'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'password.min'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'password.check_password_format' => 'パスワード'. Config::get('message.validation_common_msg.password_error'),
            'password_confirm.required' => 'パスワードを認証する'. Config::get('message.validation_common_msg.input_err'),
            'password_confirm.same' => 'パスワードが同じである必要があることを確認する',
            'password_confirm.check_password_format' => 'パスワード(確認)'. Config::get('message.validation_common_msg.password_error'),
        ];
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirm', 'token'
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);
        $user->save();
        event(new PasswordReset($user));
        $this->guard()->login($user);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse(Request $request, $response)
    {
        return redirect($this->redirectPath())->with('status', trans($response));
    }

    /**
     * Get the broker to be used during password reset.
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('users');
    }

}
