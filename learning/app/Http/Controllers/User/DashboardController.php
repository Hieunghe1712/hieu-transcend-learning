<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\User\HomeService;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    /**
     * @var HomeService $homeService
     */
    protected $homeService;

    public function __construct ( HomeService $homeService)
    {
        $this->homeService = $homeService;
        // $this->middleware('checkLoginUser')->except('indexLogin');
    }
    public function index () {
        $topLectures = $this->homeService->getTopLecture();
        $listLectures = $this->homeService->getLectures(Auth::user()->id);
        return view('user.dashboard.index', compact("topLectures","listLectures"));
    }

    /**
     * Show view index pre login
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function indexLogin () {
        $listLectures = $this->homeService->getTopLecture();

        return view("user.dashboard.index_login", compact("listLectures"));
    }

    /**
     * Change language
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function changeLanguage(Request $request , $language) {
        $lang = $request->language;
        $language = config('app.locale');

        if ($lang == 'vi') {
            $language = 'vi';
        }
        if ($lang == 'jp') {
            $language = 'jp';
        }
        \Session::put('language', $language);
        return redirect()->back();
    }
}
