<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class LecturesRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules_without_explanatory_text()
    {
        return [
            'name' => 'required|max:255',
            'embed_code' => 'required',
            'question_url' => 'nullable',
            'test_url' => 'nullable',
            'homework_url' => 'nullable',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'embed_code' => 'required',
            'explanatory_text' => 'nullable',
            'question_url' => 'nullable',
            'test_url' => 'nullable',
            'homework_url' => 'nullable',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            // 'name.required' => '講義コンテンツタイトル'. Config::get('message.validation_common_msg.input_err'),
            // 'name.max'      => '講義コンテンツタイトル' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "255"),
            // 'embed_code.required' => '埋め込みコード'. Config::get('message.validation_common_msg.input_err'),
            // 'explanatory_text.required' => '説明文'. Config::get('message.validation_common_msg.input_err'),
            // 'question_url.url' => '質問'. Config::get('message.validation_common_msg.format_err'),
            // 'test_url.url' => 'テスト'. Config::get('message.validation_common_msg.format_err'),
            // 'homework_url.url' => '宿題'. Config::get('message.validation_common_msg.format_err'),
        ];
    }
}
