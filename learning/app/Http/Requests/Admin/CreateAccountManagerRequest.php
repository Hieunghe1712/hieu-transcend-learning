<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class CreateAccountManagerRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'email' => 'required|max:255|email:rfc,dns',
            'university' => 'required|max:255',
            'faculty' => 'required|max:255',
            'permission' => 'required',
            'password' => 'required|max:128|min:8',

        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'アカウント名'. Config::get('message.validation_common_msg.input_err'),
            'name.max'      => 'アカウント名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "20"),
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.max' => 'メールアドレス' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "255"),
            'email.email' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'university.required' => '大学名'. Config::get('message.validation_common_msg.input_err'),
            'university.max' => '大学名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "255"),
            'faculty.required' => '学部名'. Config::get('message.validation_common_msg.input_err'),
            'faculty.max' => '学部名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "255"),
            'permission.required' => '許可'. Config::get('message.validation_common_msg.input_err'),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.max'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
            'password.min'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.password_length_err')), "8", "128"),
        ];
    }

}
