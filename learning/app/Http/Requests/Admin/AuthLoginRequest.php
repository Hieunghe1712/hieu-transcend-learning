<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;

class AuthLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|exists:admins,email',
            'password' => 'required|max:255',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'E-mail'. Config::get('message.validation_common_msg.input_err'),
            'email' => 'E-mail'. Config::get('message.validation_common_msg.format_err'),
            'email' => 'E-mail'. Config::get('message.validation_common_msg.input_err'),
            'password.required' => 'パスワード'. Config::get('message.validation_common_msg.input_err'),
            'password.max'      => 'パスワード' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "255"),
        ];
    }
}
