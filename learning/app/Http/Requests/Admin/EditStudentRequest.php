<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class EditStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'email' => 'required|max:255|email:rfc,dns|unique:users,email',
            'university' => 'required|max:255',
            'faculty' => 'required|max:255',
            'school_year' => 'required|digits:4|integer',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => '名前'. Config::get('message.validation_common_msg.input_err'),
            'name.max'      => '名前' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "20"),
            'email.required' => 'メールアドレス'. Config::get('message.validation_common_msg.input_err'),
            'email.max' => 'メールアドレス' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "255"),
            'email.email' => 'メールアドレス'. Config::get('message.validation_common_msg.format_err'),
            'email.unique' => 'メールアドレス'. Config::get('message.validation_common_msg.unique_err'),
            'university.required' => '大学名'. Config::get('message.validation_common_msg.input_err'),
            'university.max' => '大学名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "255"),
            'faculty.required' => '学部名'. Config::get('message.validation_common_msg.input_err'),
            'faculty.max' => '学部名' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "255"),
            'school_year.required' => '入学年次'. Config::get('message.validation_common_msg.input_err'),
            'school_year.digits' => '入学年次'. sprintf(__(Config::get('message.validation_common_msg.digits')), "4"),
            'school_year.integer' => Config::get('message.validation_common_msg.number_err'),
        ];
    }
}
