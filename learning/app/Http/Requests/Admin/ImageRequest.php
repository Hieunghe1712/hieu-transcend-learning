<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class ImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'nullable|image|mimes:jpeg,jpg,png|mimetypes:image/jpeg,image/png|max:2048',
        ];
    }
    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file.image' => Config::get('message.common_msg.upload_avatar_file_type'),
            'file.mimes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'file.mimetypes' => Config::get('message.common_msg.upload_avatar_file_type'),
            'file.max' => sprintf(__(Config::get('message.common_msg.upload_max_file_size')), "2mb"),
        ];
    }
}
