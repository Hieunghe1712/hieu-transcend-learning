<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;

class QuizRequest extends FormRequest
{
    public $validator = null;
    /**
     *
     * Check the validation is fails
     * @return validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'quiz_option_id' => 'required',
            // 'appearance_time' => 'required',
            'question_sentence' => 'required',
            'remark' => 'required',
        ];
    }

    /**
     * Get the message for validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'タイトル'. Config::get('message.validation_common_msg.input_err'),
            'title.max'      => 'タイトル' . sprintf(__(Config::get('message.validation_common_msg.max_length_err')), "255"),
            'quiz_option_id.required' => '選択肢の種類'. Config::get('message.validation_common_msg.input_err'),
            // 'appearance_time.required' => '出現時間'. Config::get('message.validation_common_msg.input_err'),
            'question_sentence.required' => '問題文'. Config::get('message.validation_common_msg.input_err'),
            'remark.required' => '備考'. Config::get('message.validation_common_msg.input_err'),
        ];
    }
}
