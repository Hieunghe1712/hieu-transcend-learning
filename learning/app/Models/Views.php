<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Views extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'views';

    protected $fillable = [
        'lecture_id',
        'user_id'
    ];

    public $timestamps = true;

}
