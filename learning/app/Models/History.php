<?php

namespace App\Models;

use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class History extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'histories';

    protected $fillable = [
        'lecture_id',
        'user_id',
        'created_at',
    ];

    public $timestamps = true;

}
