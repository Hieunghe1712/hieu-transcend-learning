<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class AnswerValues extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'answer_values';

    protected $fillable = [
        'answer_user_id',
        'quiz_id',
        'value'
    ];

    public $timestamps = true;

}
