<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class QuizValues extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'quiz_values';

    protected $fillable = [
        'quiz_id',
        'sort_number',
        'name',
    ];

    public $timestamps = true;

}
