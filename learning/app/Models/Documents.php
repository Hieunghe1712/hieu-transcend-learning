<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Documents extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'documents';

    protected $fillable = [
        'lecture_id',
        'sort_number',
        'title',
        'file_id',
        'created_at',
    ];

    public function uploaded_files()
    {
        return $this->hasOne(UploadFiles::class, 'id', 'file_id');
    }

    public $timestamps = true;

}
