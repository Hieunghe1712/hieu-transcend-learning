<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Headings extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'headings';

    protected $fillable = [
        'lecture_id',
        'sort_number',
        'title',
        'heading_time',
    ];

    public $timestamps = true;

}
