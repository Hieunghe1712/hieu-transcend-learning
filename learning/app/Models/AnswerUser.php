<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class AnswerUser extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'answer_user';

    protected $fillable = [
        'user_id',
        'lecture_id',
        'status'
    ];

    public $timestamps = true;

}
