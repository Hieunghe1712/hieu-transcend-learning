<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class Lecture extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'lectures';

    protected $fillable = [
        'name',
        'embed_code',
        'ifrem_code',
        'no',
        'liked_number',
        'view_number',
        'explanatory_text',
        'question_url',
        'test_url',
        'homework_url',
        'thumbnail_url',
    ];

    public function quizzes()
    {
        return $this->hasMany(Quizze::class, 'lecture_id', 'id')->orderBy("sort_number", "ASC");
    }

    public function explanations()
    {
        return $this->hasMany(Explanations::class, 'lecture_id', 'id')->orderBy("sort_number", "ASC");
    }

    public function headings()
    {
        return $this->hasMany(Headings::class, 'lecture_id', 'id')->orderBy("sort_number", "ASC");
    }

    public function documents()
    {
        return $this->hasMany(Documents::class, 'lecture_id', 'id')->orderBy("sort_number", "ASC");
    }

    public function memos()
    {
        return $this->hasMany(Memos::class, 'lesson_id', 'id')
                ->where('user_id', Auth::user()->id)
                ->whereNull('deleted_at')
                ->orderBy("sort_number", "ASC");
    }

    public function in_my_list()
    {
        return $this->hasOne(MyList::class, 'lecture_id', 'id')->where(['user_id' => Auth::user()->id]);
    }

    public function user_liked()
    {
        return $this->hasOne(Likes::class, 'lecture_id', 'id')->where(['user_id' => Auth::user()->id]);
    }

    public $timestamps = true;

}
