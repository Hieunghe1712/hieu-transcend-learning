<?php

namespace App\Models;

use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Quizze extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'quizzes';

    protected $fillable = [
        'lecture_id',
        'sort_number',
        'title',
        'question_sentence',
        'quiz_option_id',
        'remark',
        'appearance_time'
    ];

    public function quiz_values()
    {
        return $this->hasMany(QuizValues::class, 'quiz_id', 'id')->orderBy("sort_number", "ASC");
    }

    public $timestamps = true;

}
