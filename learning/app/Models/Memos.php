<?php

namespace App\Models;

use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Memos extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'memos';

    protected $fillable = [
        'lesson_id',
        'comment',
        'time_memo',
        'sort_number',
        'user_id',
    ];

    public $timestamps = true;

}
