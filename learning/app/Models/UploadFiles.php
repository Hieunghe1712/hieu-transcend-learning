<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class UploadFiles extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'upload_files';

    protected $fillable = [
        'name',
        'uploaded_path',
        'created_at',
    ];

    public $timestamps = true;

}
