<?php

namespace App\Models;

use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class QuizOption extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'quiz_options';

    protected $fillable = [
        'name',
        'sort_number'
    ];

    public $timestamps = true;

}
