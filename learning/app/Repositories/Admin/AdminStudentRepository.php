<?php

namespace App\Repositories\Admin;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\User;

class AdminStudentRepository
{
    /**
     * @var User $user
     */
    protected $user;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Create user manager
     *
     * @param User $user
     * @return bool
     */
    public function saveCreateStudent(User $user) {
        return $user->save();
    }
    /**
     * Edit User manager
     *
     * @param Admin $user
     * @param int $idUser
     * @return bool
     */
    public function saveEditStudent (User $user, $idUser) {
        return $user->save();
    }
    /**
     * Get list of students
     *
     * @param array $params
     * @return mixed
     */
    public function getListStudents($params = null)
    {
        $lists = $this->user
                ->select(
                    'users.id',
                    'users.name',
                    'users.email',
                    'users.university',
                    'users.faculty',
                    'users.last_login',
                    'users.school_year'
                )
                ->whereNull('deleted_at');

        if (isset($params['name'])) {
            $lists = $lists->where('users.name', 'LIKE', "%{$params['name']}%");
        }

        return $lists->paginate(PAGINATE_LIMIT);
    }
    /**
     * Get data user by id
     *
     * @param int $id
     * @return mixed
     * 
     */
    public function getStudentById(int $id) {
        return $this->user->where('users.id', $id)->first();
    }
}
