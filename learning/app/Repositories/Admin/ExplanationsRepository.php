<?php

namespace App\Repositories\Admin;

use App\Models\Explanations;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class ExplanationsRepository
{
    /**
     * @var Explanations $explanations
     */
    protected $explanations;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(Explanations $explanations)
    {
        $this->explanations = $explanations;
    }

    /**
     * Create explanations
     *
     * @param entity
     * @return bool
     */
    public function saveCreateExplanations(Explanations $explanations)
    {
        $result = true;
        if (!$explanations->save()) {
            $result = false;
        }
        return [
            'result' => $result,
            'explanations' => $explanations,
        ];
    }

    /**
     * Hard delete explanations
     *
     * @param entity
     * @return bool
     */
    public function hardDeleteExplanation(Explanations $explanations)
    {
        return $explanations->delete();
    }


}
