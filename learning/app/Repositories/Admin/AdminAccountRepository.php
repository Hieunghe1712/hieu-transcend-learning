<?php

namespace App\Repositories\Admin; 
  
use App\Models\Admin;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AdminAccountRepository
{
    /**
     * @var Account $account
     */
    protected $account;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(Admin $account)
    {
        $this->account = $account;
    }

    /**
     * Create account manager
     *
     * @param Admin $account
     * @return bool
     */
    public function saveCreateAccount (Admin $account) {
        return $account->save();
    }

    /**
     * Edit account manager
     *
     * @param Admin $account
     * @param int $idAccount
     * @return bool
     */
    public function saveEditAccount (Admin $account, $idAccount) {
        return $account->save();
    }

    /**
     * Get list account managers
     *
     * @return mixed
     * @param array $params
     * 
     */
    public function getListAccountManagers ($params = null) {
        $lists = $this->account
            ->select(
                'admins.id',
                'admins.name',
                'admins.university',
                'admins.faculty',
                'admins.last_login',
                'admins.permission'
            )->whereNull('deleted_at');
        if($params){
            if (isset($params['name'])) {
                $lists = $lists->where('admins.name', 'LIKE', "%{$params['name']}%");
            }
            if(isset($params['permission'])) {
                $permission = json_decode($params['permission'], true);
                if(count($permission) == 1 && $permission[0] == 1) {
                    $lists = $lists->where('admins.permission', 1);
                } else if(count($permission) == 1 && $permission[0] == 2) {
                    $lists = $lists->where('admins.permission', 2);
                }
            }
        }
        return $lists->paginate(PAGINATE_LIMIT);
    }

    /**
     * Get data account by id
     *
     * @param int $id
     * @return mixed
     * 
     */
    public function getAccountById(int $id) {
        return $this->account->where('admins.id', $id)->first();
    }
}
