<?php

namespace App\Repositories\Admin;

use App\Models\QuizValues;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class QuizValuesRepository
{
    /**
     * @var QuizValues $quizValues
     */
    protected $quizValues;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(QuizValues $quizValues)
    {
        $this->quizValues = $quizValues;
    }

    /**
     * Create quiz values
     *
     * @param entity
     * @return bool
     */
    public function saveCreateQuizValues(QuizValues $quizValues)
    {
        $result = true;
        if (!$quizValues->save()) {
            $result = false;
        }
        return [
            'result' => $result,
            'quizValues' => $quizValues,
        ];
    }

    /**
     * Hard delete quiz values
     *
     * @param entity
     * @return bool
     */
    public function hardDeleteQuizValues(QuizValues $quizValues)
    {
        return $quizValues->delete();
    }
}
