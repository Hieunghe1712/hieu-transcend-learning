<?php

namespace App\Repositories\Admin;

use App\Models\Quizze;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class QuizRepository
{
    /**
     * @var Quizze $quiz
     */
    protected $quiz;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(Quizze $quiz)
    {
        $this->quiz = $quiz;
    }

    /**
     * Create quiz
     *
     * @param entity
     * @return bool
     */
    public function saveCreateQuiz(Quizze $quiz)
    {
        $result = true;
        if (!$quiz->save()) {
            $result = false;
        }
        return [
            'result' => $result,
            'quiz' => $quiz,
        ];
    }

    /**
     * Hard delete quiz
     *
     * @param entity
     * @return bool
     */
    public function hardDeleteQuiz(Quizze $quiz)
    {
        return $quiz->delete();
    }
}
