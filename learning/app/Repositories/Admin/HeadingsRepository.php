<?php

namespace App\Repositories\Admin;

use App\Models\Headings;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class HeadingsRepository
{
    /**
     * @var Headings $headings
     */
    protected $headings;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(Headings $headings)
    {
        $this->headings = $headings;
    }

    /**
     * Create headings
     *
     * @param entity
     * @return bool
     */
    public function saveCreateHeadings(Headings $headings)
    {
        $result = true;
        if (!$headings->save()) {
            $result = false;
        }
        return [
            'result' => $result,
            'headings' => $headings,
        ];
    }

    /**
     * Hard delete headings
     *
     * @param entity
     * @return bool
     */
    public function hardDeleteHeadings(Headings $headings)
    {
        return $headings->delete();
    }
}
