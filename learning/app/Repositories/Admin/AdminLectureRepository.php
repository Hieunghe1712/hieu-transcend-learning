<?php

namespace App\Repositories\Admin;

use App\Models\Lecture;
use App\Models\Quizze;
use App\Models\MyList;
use App\Models\QuizOption;
use App\Models\Explanations;
use App\Models\Headings;
use App\Models\QuizValues;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AdminLectureRepository
{
    /**
     * @var Lecture $lecture
     */
    protected $lecture;

    /**
     * @var Quizze $quizze
     */
    protected $quizze;

    /**
     * @var MyList $myList
     */
    protected $myList;

    /**
     * @var QuizOption $quizOption
     */
    protected $quizOption;

    /**
     * @var Explanations $explanations
     */
    protected $explanations;

    /**
     * @var Headings $headings
     */
    protected $headings;

    /**
     * @var QuizValues $quizValues
     */
    protected $quizValues;
    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(
        Lecture $lecture,
        Quizze $quizze,
        MyList $myList,
        QuizOption $quizOption,
        Explanations $explanations,
        Headings $headings,
        QuizValues $quizValues
    )
    {
        $this->lecture = $lecture;
        $this->quizze = $quizze;
        $this->myList = $myList;
        $this->quizOption = $quizOption;
        $this->explanations = $explanations;
        $this->headings = $headings;
        $this->quizValues = $quizValues;
    }

    /**
     * Get list of lectures
     *
     * @param array $params
     * @return mixed
     */
    public function list($params = null)
    {
        $lists = $this->lecture
                ->join('admins', 'admins.id', '=', 'lectures.created_admin_id')
                ->select(
                    'lectures.id',
                    'lectures.name',
                    'admins.name as lecturerName',
                    'lectures.created_admin_id',
                    'lectures.liked_number',
                    'lectures.view_number',
                    'lectures.created_at',
                )
                ->whereNull('lectures.deleted_at');

        if (isset($params['name'])) {
            $lists = $lists->where('lectures.name', 'LIKE', "%{$params['name']}%");
        }

        return $lists->paginate(PAGINATE_LIMIT);
    }

    /**
     * Create lecture
     *
     * @param entity
     * @return bool
     */
    public function saveCreateLecture(Lecture $lecture)
    {
        $result = true;
        if (!$lecture->save()) {
            $result = false;
        }
        return [
            'result' => $result,
            'lecture' => $lecture,
        ];
    }

    /**
     * Edit lecture
     *
     * @param entity
     * @return bool
     */
    public function saveEditLecture(Lecture $lecture)
    {
        $result = true;
        if (!$lecture->save()) {
            $result = false;
        }
        return [
            'result' => $result,
            'lecture' => $lecture,
        ];
    }

    /**
     * Get my list lecture
     *
     * @param $param
     * @return mixed
     *
     */
    public function getMyListLectures($params = null)
    {
        $lists = $this->lecture
                ->select(
                    'lectures.id',
                    'lectures.name',
                    'lectures.liked_number'
                )
                ->where('lectures.created_admin_id', $params['login_id'])
                ->whereNull('lectures.deleted_at');
        if (isset($params['name'])) {
            $lists = $lists->where('lectures.name', 'LIKE', "%{$params['name']}%");
        }
        return $lists->paginate(PAGINATE_LIMIT);
    }

    /**
     * Get data lecture by id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getLectureById($id)
    {
        return $this->lecture
                ->join('admins', 'admins.id', '=', 'lectures.created_admin_id')
                ->with('quizzes', 'quizzes.quiz_values', 'explanations', 'explanations.uploaded_files', 'headings', 'documents', 'documents.uploaded_files')
                ->select(
                    'lectures.*',
                    'admins.name as lecturerName'
                )->where('lectures.id', $id)->first();
    }


    /**
     * Get data quiz by lecture id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getQuizByLectureId($id) {
        return $this->quizze
                ->join('lectures', 'lectures.id', '=', 'quizzes.lecture_id')
                ->select(
                    'quizzes.*'
                )->where('lectures.id', $id)->get();
    }

    /**
     * Get data explain by lecture id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getExplainByLectureId($id) {
        return $this->explanations
                ->join('lectures', 'lectures.id', '=', 'explanations.lecture_id')
                ->join('upload_files', 'upload_files.id', '=', 'explanations.file_id')
                ->select(
                    'upload_files.uploaded_path',
                    'explanations.*'
                )->where('lectures.id', $id)->get();
    }

    /**
     * Get data heading by lecture id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getHeadingByLectureId($id) {
        return $this->headings
                ->join('lectures', 'lectures.id', '=', 'headings.lecture_id')
                ->select(
                    'headings.*'
                )->where('lectures.id', $id)->get();
    }

    /**
     * Get data options value by lecture id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getOptionValueByLectureId($id) {
        return $this->quizValues
                ->join('quizzes', 'quizzes.id', '=', 'quiz_values.quiz_id')
                ->join('lectures', 'lectures.id', '=', 'quizzes.lecture_id')
                ->join('quiz_options', 'quiz_options.id', '=', 'quizzes.quiz_option_id')
                ->select(
                    'quiz_values.*',
                    'quiz_options.id as optionId',
                    'quiz_options.name as optionName'
                )->where('lectures.id', $id)->get();
    }

    /**
     * Get quizze by id of lecture
     *
     * @param int $idLecture
     * @return mixed
     */
    public function findQuizzeByLectureId(int $idLecture)
    {
        return $this->quizze->where([
                    'quizzes.lecture_id' => $idLecture
                ])->first();
    }

    /**
     * Soft delete lecture
     *
     * @param int $idLecture
     * @return mixed
     */
    public function deleteLecture(int $idLecture)
    {
        $result = true;
        DB::beginTransaction();
        try {
            $res = $this->lecture
                    ->where('lectures.id', $idLecture)
                    ->update([
                        'deleted_at' => date('Y-m-d H:i:s'),
                    ]);
            if (!$res) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * get max id
     *
     * @return \Illuminate\Http\Response
     */
    public function getMaxId()
    {
        return $this->lecture->max('id');
    }
}
