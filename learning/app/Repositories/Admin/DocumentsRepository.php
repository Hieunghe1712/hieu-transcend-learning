<?php

namespace App\Repositories\Admin;

use App\Models\Documents;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class DocumentsRepository
{
    /**
     * @var Documents $documents
     */
    protected $documents;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(Documents $documents)
    {
        $this->documents = $documents;
    }

    /**
     * Create documents
     *
     * @param entity
     * @return bool
     */
    public function saveCreateDocuments (Documents $documents)
    {
        $result = true;
        if (!$documents->save()) {
            $result = false;
        }
        return [
            'result' => $result,
            'documents' => $documents,
        ];
    }

    /**
     * Hard delete documents
     *
     * @param entity
     * @return bool
     */
    public function hardDeleteDocuments (Documents $documents)
    {
        return $documents->delete();
    }
}
