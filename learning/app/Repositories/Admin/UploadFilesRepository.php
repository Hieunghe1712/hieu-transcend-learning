<?php

namespace App\Repositories\Admin;

use App\Models\UploadFiles;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class UploadFilesRepository
{
    /**
     * @var UploadFiles $uploadFiles
     */
    protected $uploadFiles;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(UploadFiles $uploadFiles)
    {
        $this->uploadFiles = $uploadFiles;
    }

    /**
     * Create upload file
     *
     * @param entity
     * @return bool
     */
    public function saveCreateUploadFiles(UploadFiles $uploadFiles)
    {
        $result = true;
        if (!$uploadFiles->save()) {
            $result = false;
        }
        return [
            'result' => $result,
            'uploadFiles' => $uploadFiles,
        ];
    }

    /**
     * Hard delete uploaded files
     *
     * @param entity
     * @return bool
     */
    public function hardDeleteUploadedFile(UploadFiles $uploadFiles)
    {
        return $uploadFiles->delete();
    }
}
