<?php

namespace App\Repositories\Admin;

use App\Models\QuizOption;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;

class QuizOptionsRepository
{
    /**
     * @var QuizOption $quizOption
     */
    protected $user;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(QuizOption $quizOption)
    {
        $this->quizOption = $quizOption;
    }

    /**
     * Get list quiz options
     *
     * @param array $params
     * @return mixed
     */
    public function getListQuizOptions()
    {
        $query = $this->quizOption
        ->select('id', 'name', 'name_vi', 'sort_number', 'have_choices')
        ->whereNull('deleted_at')
        ->orderBy("sort_number", "ASC");
        return $query->get();
    }

    /**
     * Get list quiz options
     *
     * @param array $params
     * @return mixed
     */
    public function getListIdsQuizOptionsHaveChoice()
    {
        $query = $this->quizOption
        ->whereNull('deleted_at')
        ->where('have_choices', QUIZ_HAVE_CHOICE_1)
        ->orderBy("sort_number", "ASC")
        ->pluck("id");
        return $query->toArray();
    }

    /**
     * Get quiz options by id
     *
     * @param entity
     * @return mixed
     */
    public function getQuizOptionById($id)
    {
        $query = $this->quizOption
        ->select('id', 'name', 'sort_number', 'have_choices')
        ->where('id', $id)
        ->orderBy("sort_number", "ASC");
        return $query->first();
    }
}
