<?php
namespace App\Repositories\Auth;

use Illuminate\Support\Carbon;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\DatabaseTokenRepository as BaseRepository;

class DatabaseTokenRepository extends BaseRepository
{

    /**
     * Create a new token record.
     * @param  \Illuminate\Contracts\Auth\CanResetPassword $user
     * @return string
     */
    public function create(CanResetPasswordContract $user)
    {
        $email = $user->getEmailForPasswordReset();

        $this->deleteExisting($user);

        // We will create a new, random token for the user so that we can e-mail them
        // a safe link to the password reset form. Then we will insert a record in
        // the database so that we can verify the token within the actual reset.
        $token = $this->createNewToken();

        $this->getTable()->where('email', $user->getEmailForPasswordReset())
            ->update($this->getPayload($email, $token));

        return $token;
    }

    protected function getPayload($email, $token)
    {
        return [
            'password_reset_token'=> $this->hasher->make($token),
            'password_reset_request_at' => new Carbon
        ];
    }


    protected function deleteExisting(CanResetPasswordContract $user)
    {
        return $this->getTable()->where('email', $user->getEmailForPasswordReset())->update([
            'password_reset_token'      => null,
            'reset_token_created_at' => null,
        ]);
    }

    public function deleteExpired()
    {
        $expiredAt = Carbon::now()->subSeconds($this->expires);

        $this->getTable()->where('password_reset_request_at', '<', $expiredAt)->update([
            'password_reset_token'      => null,
            'password_reset_request_at' => null,
        ]);
    }

    public function exists(CanResetPasswordContract $user, $token)
    {
        $record = (array) $this->getTable()->where(
            'email', $user->getEmailForPasswordReset()
        )->first();

        return $record &&
               ! $this->tokenExpired($record['password_reset_request_at']) &&
                 $this->hasher->check($token, $record['password_reset_token']);
    }

}
