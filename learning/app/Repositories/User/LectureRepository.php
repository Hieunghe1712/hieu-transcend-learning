<?php

namespace App\Repositories\User;

use App\Models\Lecture;
use App\Models\Quizze;
use App\Models\MyList;
use App\Models\QuizOption;
use App\Models\Explanations;
use App\Models\Headings;
use App\Models\QuizValues;
use App\Models\History;
use App\Models\Memos;
use App\Models\AnswerUser;
use App\Models\AnswerValues;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class LectureRepository
{
    /**
     * @var Lecture $lecture
     */
    protected $lecture;

    /**
     * @var Quizze $quizze
     */
    protected $quizze;

    /**
     * @var MyList $myList
     */
    protected $myList;

    /**
     * @var QuizOption $quizOption
     */
    protected $quizOption;

    /**
     * @var Explanations $explanations
     */
    protected $explanations;

    /**
     * @var Headings $headings
     */
    protected $headings;

    /**
     * @var QuizValues $quizValues
     */
    protected $quizValues;

    /**
     * @var History $history
     */
    protected $history;

    /**
     * @var Memos $memos
     */
    protected $memos;    

    /**
     * @var AnswerUser $answerUser
     */
    protected $answerUser;    

    /**
     * @var AnswerValues $answerValues
     */
    protected $answerValues;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(
        Lecture $lecture,
        Quizze $quizze,
        MyList $myList,
        QuizOption $quizOption,
        Explanations $explanations,
        Headings $headings,
        QuizValues $quizValues,
        History $history,
        Memos $memos,
        AnswerUser $answerUser,
        AnswerValues $answerValues
    )
    {
        $this->lecture = $lecture;
        $this->quizze = $quizze;
        $this->myList = $myList;
        $this->quizOption = $quizOption;
        $this->explanations = $explanations;
        $this->headings = $headings;
        $this->quizValues = $quizValues;
        $this->history = $history;
        $this->memos = $memos;
        $this->answerUser = $answerUser;
        $this->answerValues = $answerValues;
    }

    /**
     * Get my lecture list
     *
     * @return mixed
     * @param array params
     *
     */
    public function getMyListLectures($params = null)
    {
        $lists = $this->myList
                ->join('lectures', 'lectures.id', 'my_lists.lecture_id')
                ->join('admins', 'admins.id', '=', 'lectures.created_admin_id')
                ->select(
                    'my_lists.id as myListId',
                    'lectures.*',
                    'admins.university'
                )
                ->where('my_lists.user_id', $params['login_id'])
                ->whereNull('my_lists.deleted_at')
                ->orderBy("my_lists.id", "DESC");
        return $lists->paginate(PAGINATE_LIMIT);
    }

    /**
     * Get data my list by id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getMyListById(int $id)
    {
        return $this->myList->where('my_lists.id', $id)->first();
    }

     /**
     * Get data search
     *
     * @param array params
     * @return mixed
     *
     */
    public function searchLecture($params = null)
    {
        $lists = $this->lecture
        ->join('admins', 'admins.id', '=', 'lectures.created_admin_id')
        ->select(
            'lectures.id',
            'lectures.name',
            'admins.name as lecturerName',
            'lectures.created_admin_id',
            'lectures.liked_number',
            'lectures.view_number',
            'lectures.created_at',
        )
        ->whereNull('lectures.deleted_at');

        if (isset($params['name'])) {
            $lists = $lists->where('lectures.name', 'LIKE', "%{$params['name']}%");
        }

        return $lists->paginate(PAGINATE_LIMIT);
    } 

    /**
     * Get lecture history list
     *
     * @return mixed
     * @param array params
     *
     */
    public function getLectureHistory($params = null)
    {
        $lists = $this->history
                ->orderBy('histories.created_at', 'DESC')
                ->join('lectures', 'lectures.id', 'histories.lecture_id')
                ->join('admins', 'admins.id', '=', 'lectures.created_admin_id')
                ->select(
                    'lectures.*',
                    'admins.university',
                    'histories.created_at as day'
                )
                ->where('histories.user_id', $params['login_id']);
        return $lists->paginate(PAGINATE_LIMIT);
    }

    /**
     * Get data lecture by id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getLectureById($id)
    {
        return $this->lecture
                ->join('admins', 'admins.id', 'lectures.created_admin_id')
                ->select('lectures.*', 'admins.university')
                ->with('quizzes', 'quizzes.quiz_values', 'explanations', 'explanations.uploaded_files', 'headings', 'documents', 'documents.uploaded_files', 'memos', 'in_my_list')
                ->where('lectures.id', $id)->first();
    }

    /**
     * Update lecture
     *
     * @param Lecture $lecture
     * @return mixed
     *
     */
    public function updateLecture(Lecture $lecture)
    {
        $result = true;
        DB::beginTransaction();
        try {
            if (!$lecture->save()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * get random lecture
     *
     * @param int $numberRecord
     * @param int $idLecture
     * @return mixed
     *
     */
    public function getRandomLecture($numberRecord, $idLecture)
    {
        $lectrues =  $this->lecture
                ->join('admins', 'admins.id', '=', 'lectures.created_admin_id')
                ->select(
                    'lectures.*',
                    'admins.university'
                )
                ->where('lectures.id', '!=', $idLecture)
                ->whereNull('lectures.deleted_at')
                ->get();
        if ($lectrues->count() > $numberRecord) {
            $lectrues = $lectrues->random($numberRecord);
        }
        return $lectrues;
    }

    /**
     * Get lecture note list
     *
     * @return mixed
     * @param array params
     *
     */
    public function getLectureNote($params = null)
    {
        $lists = $this->lecture
                ->with('memos')
                ->has('memos');
        return $lists->paginate(PAGINATE_LIMIT);
    }

    /**
     * Get data note by id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getMemoById(int $id)
    {
        return $this->memos->where('memos.id', $id)->first();
    }

    /**
     * Get document list
     *
     * @return mixed
     * @param int $id
     *
     */
    public function getDocuments($id)
    {
        $lists = $this->lecture
                ->with('documents')
                ->join('documents', 'documents.lecture_id', 'lectures.id')
                ->join('upload_files', 'upload_files.id', 'documents.file_id')
                ->select(
                    'lectures.id',
                    'upload_files.name',
                    'upload_files.uploaded_path',
                    'documents.created_at'
                )
                ->where('lectures.id', $id);
        return $lists->paginate(PAGINATE_LIMIT);
    }


    /**
     * Get quizzes by lecture id
     *
     * @return mixed
     * @param int $id
     *
     */
    public function getQuizByLectureId($id)
    {
        $lists = $this->lecture
                ->with('quizzes')
                ->select(
                    'lectures.id',
                    'lectures.name',
                    'lectures.created_at'
                )->get();
        return $lists;
    }

    /**
     * Get data lecture by id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function dataLectureById($id)
    {
        return $this->lecture->where('lectures.id', $id)->first();
    }

    /**
     * Get quizz appearance time by lecture id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getAppearanceTimeByLectureId($id) {
        $appearanceTime = $this->lecture
                ->join('quizzes', 'quizzes.lecture_id', '=', 'lectures.id')
                ->where('lectures.id', $id)
                ->pluck('quizzes.appearance_time', 'quizzes.sort_number');
        return $appearanceTime->toArray();
    }

    /**
     * Get data quiz by lecture id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function dataQuizByLectureId($id) {
        return $this->quizze
                ->join('lectures', 'lectures.id', '=', 'quizzes.lecture_id')
                ->join('quiz_options', 'quiz_options.id', '=', 'quizzes.quiz_option_id')
                ->select(
                    'quiz_options.type',
                    'quizzes.*'
                )->where('lectures.id', $id)->get();
    }

    /**
     * Get data options value by lecture id
     *
     * @param int $quizId
     * @return mixed
     *
     */
    public function getOptionValues($quizId) {
        return $this->quizValues->where('quiz_values.quiz_id', $quizId)
        ->orderBy('quiz_values.sort_number', 'ASC')
        ->get();
    }

    /**
     * save answer of quiz in lecture detail
     *
     * @param AnswerUser $answerUser
     * @param AnswerValues $answerValues
     * @return bool
     */
    public function saveAnswer (
        AnswerUser $answerUser,
        AnswerValues $answerValues
    ) {
        $return = true;
        DB::beginTransaction();
        try{
            if ($answerUser->save()) {
                $answerValues->answer_user_id = $answerUser->id;
                $answerValues->save();
            } else {
                $return = false;
            }
            $return ? DB::commit() : DB::rollback();
            return $return;
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return false;
        }
    }

    /**
     * Get data explanations by lecture id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getExplainByLectureId($id)
    {
        return $this->explanations
                ->join('upload_files', 'upload_files.id', '=', 'explanations.file_id')
                ->select(
                    'explanations.*',
                    'upload_files.uploaded_path'
                )
                ->where('explanations.lecture_id', $id)->get();
    }    


    /**
     * Get data answer by user id
     *
     * @param int $id
     * @return mixed
     *
     */
    public function getAnswerByUserId($id)
    {
        return $this->quizze
                ->join('answer_values', 'answer_values.quiz_id', '=', 'quizzes.id')
                ->join('answer_user', 'answer_user.id', 'answer_values.answer_user_id')
                ->where('answer_user.user_id', $id)
                ->pluck('answer_values.quiz_id')->toArray();

    }

    /**
     * Get all lecture note list
     *
     * @return mixed
     *
     */
    public function getAllLectureNote()
    {
        $lists = $this->lecture
                ->with('memos')
                ->has('memos');
        return $lists->get();
    }

    /**
     * Get my lecture list on page
     *
     * @return mixed
     * @param array params
     *
     */
    public function getAllMyListLecture($params = null)
    {
        $lists = $this->myList
                ->join('lectures', 'lectures.id', 'my_lists.lecture_id')
                ->join('admins', 'admins.id', '=', 'lectures.created_admin_id')
                ->select(
                    'my_lists.id as myListId',
                    'lectures.*',
                    'admins.university'
                )
                ->where('my_lists.user_id', $params['login_id'])
                ->whereNull('my_lists.deleted_at');
        return $lists->get();
    }
    
}
