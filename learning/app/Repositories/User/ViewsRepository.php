<?php

namespace App\Repositories\User;

use App\Models\Views;
use Illuminate\Support\Facades\DB;
use Exception;

class ViewsRepository
{
    /**
     * @var Views $views
     */
    protected $views;


    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(
        Views $views
    ) {
        $this->views = $views;
    }

    /**
     * get views by user id and lecture id
     * @param $idUser
     * @param $idLecture
     *
     * @return mixed
     */
    public function getViewsByUserIdAndLectureId($idUser, $idLecture)
    {
        $views = $this->views->where(['lecture_id' => $idLecture, 'user_id' => $idUser])->first();
        return $views;
    }

    /**
     * Add user view lecture
     *
     * @return mixed
     */
    public function addViewLecture(Views $views)
    {
        $result = true;
        DB::beginTransaction();
        try{
            if (!$views->save()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

}
