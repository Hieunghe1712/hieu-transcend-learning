<?php

namespace App\Repositories\User;

use App\Models\Memos;
use Illuminate\Support\Facades\DB;
use Exception;


class MemosRepository
{
    /**
     * @var Memos $memo
     */
    protected $memo;


    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(
        Memos $memo
    ) {
        $this->memo = $memo;

    }

    /**
     * Get max sort number by lesson id
     * @param $id
     *
     * @return mixed
     */
    public function getMaxSortNumberByLessonId($id)
    {
        $max = $this->memo->where('id', $id)->max('sort_number');
        return $max;
    }

    /**
     * create memo
     *
     * @return mixed
     */
    public function createMemo(Memos $memo)
    {
        $result = true;
        DB::beginTransaction();
        try{
            if (!$memo->save()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }
}
