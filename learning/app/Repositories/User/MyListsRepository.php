<?php

namespace App\Repositories\User;

use App\Models\MyList;
use Illuminate\Support\Facades\DB;
use Exception;

class MyListsRepository
{
    /**
     * @var MyList $myList
     */
    protected $myList;


    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(
        MyList $myList
    ) {
        $this->myList = $myList;
    }

    /**
     * Get max sort number by lesson id
     * @param $idUser
     * @param $idLecture
     *
     * @return mixed
     */
    public function getMyListByUserIdAndLectureId($idUser, $idLecture)
    {
        $myList = $this->myList->where(['lecture_id' => $idLecture, 'user_id' => $idUser])->first();
        return $myList;
    }

    /**
     * Add to my List
     *
     * @return mixed
     */
    public function addToMyList(MyList $myList)
    {
        $result = true;
        DB::beginTransaction();
        try{
            if (!$myList->save()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Remove to my List
     *
     * @return mixed
     */
    public function removeToMyList(MyList $myList)
    {
        $result = true;
        DB::beginTransaction();
        try{
            if (!$myList->delete()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }
}
