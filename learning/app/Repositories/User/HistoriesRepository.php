<?php

namespace App\Repositories\User;

use App\Models\History;
use Illuminate\Support\Facades\DB;
use Exception;

class HistoriesRepository
{
    /**
     * @var History $history
     */
    protected $history;


    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(
        History $history
    ) {
        $this->history = $history;
    }

    /**
     * get history by user id and lecture id
     * @param $idUser
     * @param $idLecture
     *
     * @return mixed
     */
    public function getHistoryByUserIdAndLectureId($idUser, $idLecture, $date)
    {
        $history = $this->history
            ->where('histories.user_id', $idUser)
            ->where('histories.lecture_id', $idLecture)
            ->whereDate('created_at', $date)
            ->first();
        return $history;
    }

    /**
     * Add user history lecture
     *
     * @return mixed
     */
    public function addOrUpdateHistoryLecture(History $history)
    {
        $result = true;
        DB::beginTransaction();
        try{
            if (!$history->save()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

}
