<?php

namespace App\Repositories\User;

use App\Models\Lecture;
use Illuminate\Support\Facades\DB;
use Exception;

class HomeRepository
{
    /**
     * @var lecture $lecture
     */
    protected $lecture;


    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(
        Lecture $lecture
    ) {
        $this->lecture = $lecture;
    }

    /**
     * Get lecture before login
     *
     * @return mixed
     */
    public function getTopLecture()
    {
        $lecture = $this->lecture
                        ->join('admins', 'admins.id', '=', 'lectures.created_admin_id')
                        ->select('lectures.*', 'admins.university')
                        ->whereNull('lectures.deleted_at')
                        ->orderBy('created_at','desc')
                        ->take(3)->get();
        return $lecture;
    }
    /**
     * Get lecture
     *
     * @return mixed
     *
     *
     */
    public function getLectures($params = null)
    {
        $lists = $this->lecture
                ->join('admins', 'admins.id', '=', 'lectures.created_admin_id')
                ->select('lectures.*', 'admins.university')
                ->whereNull('lectures.deleted_at');

        return $lists->paginate(PAGINATE_LIMIT);
    }

}
