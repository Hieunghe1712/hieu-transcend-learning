<?php

namespace App\Repositories\User;

use App\Models\Likes;
use Illuminate\Support\Facades\DB;
use Exception;

class LikesRepository
{
    /**
     * @var Likes $likes
     */
    protected $likes;


    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(
        Likes $likes
    ) {
        $this->likes = $likes;
    }

    /**
     * Get max sort number by lesson id
     * @param $idUser
     * @param $idLecture
     *
     * @return mixed
     */
    public function getLikesByUserIdAndLectureId($idUser, $idLecture)
    {
        $likes = $this->likes->where(['lecture_id' => $idLecture, 'user_id' => $idUser])->first();
        return $likes;
    }

    /**
     * Add to my List
     *
     * @return mixed
     */
    public function addLikeLecture(Likes $like)
    {
        $result = true;
        DB::beginTransaction();
        try{
            if (!$like->save()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

    /**
     * Remove to my List
     *
     * @return mixed
     */
    public function removeLikeLecture(Likes $like)
    {
        $result = true;
        DB::beginTransaction();
        try{
            if (!$like->delete()) {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }
}
