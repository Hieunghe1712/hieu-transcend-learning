<?php

namespace App\Repositories\User;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\User;

class ProfileRepository
{
    /**
     * @var User $user
     */
    protected $user;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Create or update a student account
     *
     * @param User $user
     * @param $image
     * @return boolean
     */
    public function updateProfile(
        User $user,
        $image
    ) {
        $result = true;
        DB::beginTransaction();
        try{
            if ($user->save()) {
                if (!empty($image)) {
                    $dirTmp = AVATAR_STUDENT_SAVE_TMP . $image;
                    $dirSave = AVATAR_STUDENT_SAVE . $image;
                    if (file_exists($dirTmp)) {
                        @rename($dirTmp, $dirSave);
                    }
                }
            } else {
                $result = false;
            }
            $result ? DB::commit() : DB::rollback();
            return $result;
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }

}
