<?php

/**
 * コントローラ名を取得する
 *
 * @return String
 */
function controllerName()
{
    return substr(class_basename(Route::currentRouteAction()),
        0, (strpos(class_basename(Route::currentRouteAction()), '@') -0)
    );
}

/**
 * メソッド名を取得する
 *
 * @return String
 */
function methodName()
{
    return substr(strstr(class_basename(Route::currentRouteAction()), '@'),
        1, (strpos(class_basename(Route::currentRouteAction()), '@') -0)
    );
}


/**
 * 文字列を省略する
 *
 * @param  String $mbStrimle
 * @param  Int $start
 * @param  Int $length
 * @param  String $trimMarker
 * @param  String|Boolean $encoding
 * @return String
 */
function mbStrimlen($mbStrimle, $start = 0, $length = 20, $trimMarker = '...', $encoding = false)
{
    $encoding = $encoding ? $encoding : mb_internal_encoding();
    $mbStrimle = mb_substr($mbStrimle, $start, mb_strlen($mbStrimle), $encoding);
    if (mb_strlen($mbStrimle, $encoding) > $length) {
        $markerlen = mb_strlen($trimMarker, $encoding);
        $mbStrimle = mb_substr($mbStrimle, 0, $length - $markerlen, $encoding) . $trimMarker;
    }

    return $mbStrimle;
}

/**
 * 権限を文字列に変換する
 *
 * @param  Int $int
 * @return String
 */
function getPermission(Int $int)
{
    $val = '';
    switch ($int) {
        case 1:
            $val = __('運営');
            break;
        case 2:
            $val = __('講師');
            break;
    }

    return $val;
}

/**
 * Format datetime
 * @param $date
 * @param $format
 * @return $date
 */
if (!function_exists('formatDate')) {
    function formatDate($date, $format = 'Y/m/d H:i:s')
    {
        if (empty($date)) {
            return '';
        }
        try {
            if (!empty($date->year)) {
                $date = $date->format($format);
            } else {
                $dateCv = new \DateTime($date);
                $date = $dateCv->format($format);
            }
        } catch (\Exception $e) {
            return '';
        }
        return $date;
    }
}


/**
 * Format number
 * @param $number
 * @return $number
 */
if (!function_exists('formatNumber')) {
    function formatNumber($number)
    {
        return number_format((float)$number, 0);
    }
}
