<?php

define('PAGINATE_LIMIT', 15);
define('SUCCESS', 'success');
define('ERROR', 'error');
define('LIMIT_20', 20);

define('NO_ERROR', 0);
define('UNDELETED', 0);
define('DELETED', 1);
define('CODE_AJAX_ERROR', 0);
define('CODE_AJAX_SUCCESS', 1);
define('CODE_AJAX_ERROR_DATA', 2);

define('AVATAR_MANAGER_SAVE_TMP', $_SERVER['DOCUMENT_ROOT'] . '/uploads/managers/tmp/');
define('AVATAR_MANAGER_SAVE', $_SERVER['DOCUMENT_ROOT'] . '/uploads/managers/');
define('AVATAR_MANAGER_SAVE_TMP_JS', '/uploads/managers/tmp/');
define('AVATAR_MANAGER_SAVE_JS', '/uploads/managers/');

define('AVATAR_STUDENT_SAVE_TMP', $_SERVER['DOCUMENT_ROOT'] . '/uploads/students/tmp/');
define('AVATAR_STUDENT_SAVE', $_SERVER['DOCUMENT_ROOT'] . '/uploads/students/');
define('AVATAR_STUDENT_SAVE_TMP_JS', '/uploads/students/tmp/');
define('AVATAR_STUDENT_SAVE_JS', '/uploads/students/');

define('PASSWORD', '********');
define('NO_DATA_FOUND', 'データが見つかりません。');

define("QUIZ_HAVE_CHOICE_0", 0);
define("QUIZ_HAVE_CHOICE_1", 1);
define('LECTURES_EXPLANTION_SAVE_TMP', $_SERVER['DOCUMENT_ROOT'] . '/uploads/lectures/explanations/tmp/');
define('LECTURES_EXPLANTION_SAVE', $_SERVER['DOCUMENT_ROOT'] . '/uploads/lectures/explanations/');
define('LECTURES_EXPLANTION_SAVE_TMP_JS', '/uploads/lectures/explanations/tmp/');
define('LECTURES_EXPLANTION_SAVE_JS', '/uploads/lectures/explanations/');

define('LECTURES_DOCUMENT_SAVE_TMP', $_SERVER['DOCUMENT_ROOT'] . '/uploads/lectures/documents/tmp/');
define('LECTURES_DOCUMENT_SAVE', $_SERVER['DOCUMENT_ROOT'] . '/uploads/lectures/documents/');
define('LECTURES_DOCUMENT_SAVE_TMP_JS', '/uploads/lectures/documents/tmp/');
define('LECTURES_DOCUMENT_SAVE_JS', '/uploads/lectures/documents/');

define('LECTURES_DOCUMENT_DOWNLOAD', 'downloads/lectures/documents/');


define('INPUT', 'input');
define('RADIO', 'radio');
define('CHECKBOX', 'checkbox');
define('SELECT', 'select');
define('TEXTAREA', 'textarea');

return [
	'array_extension_file_avatar' => ['jpg', 'jpeg', 'png'],
];
