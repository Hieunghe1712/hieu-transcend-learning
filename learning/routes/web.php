<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\AccountManagerController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\StudentManagerController;
use App\Http\Controllers\Admin\LectureManagerController;
use App\Http\Controllers\Admin\ResetsPasswordController;

use App\Http\Controllers\User\AuthController as UserAuthController;
use App\Http\Controllers\User\DashboardController as UserDashboardController;
use App\Http\Controllers\User\LectureManagerController as UserLectureManagerController;
use App\Http\Controllers\User\ProfileController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('locale')->group(function() {
    Route::get('/admin-change-language/{language}', [DashboardController::class, 'changeLanguage'])->name('change-changeLanguageadmin');
    //route admin
    Route::prefix('admin')->group(function () {
        Route::get('/login', [AuthController::class, 'getLogin'])->name('admin.login');
        Route::post('/login', [AuthController::class, 'postLogin']);
        Route::get('/logout', [AuthController::class, 'logout'])->name('admin.logout');
        Route::get('/forgot-password', [AuthController::class, 'forgotPassword'])->name('admin.forgotPassword');
        Route::post('/forgot-password', [AuthController::class, 'sendResetLinkEmail'])->name('admin.postForgotPassword');
        Route::get('/recover-password/{token}', [ResetsPasswordController::class, 'showResetForm'])->name('password.reset');
        Route::post('/recover-password', [ResetsPasswordController::class, 'reset'])->name('admin.postRecoverPassword');

        Route::middleware('CheckAdminLogin')->group(function () {
            Route::get('/', [DashboardController::class, 'index'])->name('admin.index');
            Route::prefix('accounts')->group(function () {
                Route::get('/', [AccountManagerController::class, 'index'])->name('admin.account_manager_list');
                Route::get('/create', [AccountManagerController::class, 'createForm'])->name('admin.createForm');
                Route::post('/create', [AccountManagerController::class, 'create'])->name('admin.createAccountManager');
                Route::post('/upload-avatar', [AccountManagerController::class, 'upload'])->name('admin.manager.uploadAvatar');
                Route::get('/{id}', [AccountManagerController::class, 'getDataAccountById'])->name('admin.get_account');
                Route::get('/create-confirm/{key}', [AccountManagerController::class, 'createConfirmAccount'])->name('admin.create_account_confirm');
                Route::post('/process-save-create', [AccountManagerController::class, 'saveCreateAccount'])->name('admin.saveCreateAccount');
                Route::get('/edit/{id}', [AccountManagerController::class, 'editAccountForm'])->name('admin.editForm');
                Route::post('/edit/{id}', [AccountManagerController::class, 'edit'])->name('admin.editAccountManager');
                Route::get('/edit-confirm/{key}/{id}', [AccountManagerController::class, 'editConfirmAccount'])->name('admin.edit_account_confirm');
                Route::post('/process-save-edit/{id}', [AccountManagerController::class, 'saveEditAccount'])->name('admin.saveEditAccount');
                Route::get('delete/{id}', [AccountManagerController::class, 'deleteAccount'])->name('admin.deleteAccount');
            });

            Route::prefix('students')->group(function () {
                Route::get('/', [StudentManagerController::class, 'index'])->name('admin.student_list');
                Route::get('/create', [StudentManagerController::class, 'createStudent'])->name('admin.create_student');
                Route::post('/create', [StudentManagerController::class, 'postCreate'])->name('admin.postCreateStudent');
                Route::get('/create-confirm/{key}', [StudentManagerController::class, 'confirmStudentAccount'])->name('admin.confirmStudentAccount');
                Route::post('/process-save-create', [StudentManagerController::class, 'saveCreateStudent'])->name('admin.saveCreateStudent');
                Route::post('/upload-avatar', [StudentManagerController::class, 'uploadImageStudent'])->name('admin.student.uploadAvatar');
                Route::get('/{id}', [StudentManagerController::class, 'getDataStudentById'])->name('admin.student_detail');
                Route::get('/edit/{id}', [StudentManagerController::class, 'editStudent'])->name('admin.editStudent');
                Route::post('/edit/{id}', [StudentManagerController::class, 'postEdit'])->name('admin.postEditStudent');
                Route::get('/edit-confirm/{key}/{id}', [StudentManagerController::class, 'editConfirmStudent'])->name('admin.editConfirmStudent');
                Route::post('/process-save-edit/{id}', [StudentManagerController::class, 'saveEditStudent'])->name('admin.saveEditStudent');
                Route::get('/delete/{id}', [StudentManagerController::class, 'deleteStudent'])->name('admin.deleteStudent');
            });

            Route::prefix('lectures')->group(function () {
                Route::get('/list', [LectureManagerController::class, 'list'])->name('admin.lecture_list');
                Route::get('/my-list', [LectureManagerController::class, 'myList'])->name('admin.my_lecture_list');
                Route::get('/my-list/create', [LectureManagerController::class, 'createForm'])->name('admin.create_lecture_form');
                Route::post('/my-list/create', [LectureManagerController::class, 'create'])->name('admin.create_lecture');
                Route::get('/my-list/create-confirm/{key}', [LectureManagerController::class, 'createConfirmLectures'])->name('admin.lectures.createConfirmLectures');
                Route::post('/my-list/process-save-create/{key}', [LectureManagerController::class, 'saveCreateLecture'])->name('admin.lectures.saveCreateLecture');
                Route::get('/my-list/edit/{id}', [LectureManagerController::class, 'editLectureForm'])->name('admin.editLectureForm');
                Route::post('/my-list/edit/{id}', [LectureManagerController::class, 'editLectures'])->name('admin.lectures.editLecture');
                Route::get('/my-list/edit-confirm/{id?}/{key?}', [LectureManagerController::class, 'editConfirmLecture'])->name('admin.lectures.editConfirmLecture');
                Route::post('/my-list/process-save-edit/{id}/{key}', [LectureManagerController::class, 'saveEditLecture'])->name('admin.lectures.saveEditLecture');
                Route::get('/my-list/edit-confirm/{key}/{id}', [LectureManagerController::class, 'editConfirmLecture'])->name('admin.edit_lecture_confirm');
                Route::post('/my-list/process-save-edit/{id}', [LectureManagerController::class, 'saveEditLecture'])->name('admin.saveEditLecture');
                Route::post('/get-form-quizz', [LectureManagerController::class, 'getFormAddQuiz'])->name('admin.lectures.getFormAddQuiz');
                Route::post('/get-form-explanation', [LectureManagerController::class, 'getFormExplanation'])->name('admin.lectures.getFormExplanation');
                Route::post('/get-form-headings', [LectureManagerController::class, 'getFormHeadings'])->name('admin.lectures.getFormHeadings');
                Route::post('/get-form-documents', [LectureManagerController::class, 'getFormDocuments'])->name('admin.lectures.getFormDocuments');
                Route::post('/my-list/create-lectures', [LectureManagerController::class, 'createLectures'])->name('admin.lectures.createLectures');
                Route::post('/my-list/upload-explanation-file', [LectureManagerController::class, 'uploadExplanationFile'])->name('admin.lectures.uploadExplanationFile');
                Route::post('/my-list/upload-document-file', [LectureManagerController::class, 'uploadDocumentsFile'])->name('admin.lectures.uploadDocumentsFile');
                Route::get('/{id}', [LectureManagerController::class, 'detail'])->name('admin.lecture_detail');
                Route::get('/my-list/lectures/delete/{id?}', [LectureManagerController::class, 'deleteLucture'])->name('admin.lectures.deleteLucture');
            });
        });
    });

    //route user
    Route::get('/change-language/{language}', [UserDashboardController::class, 'changeLanguage'])->name('change-language');
    Route::get('/login', [UserAuthController::class, 'getLogin'])->name('user.login');
    Route::post('/login', [UserAuthController::class, 'postLogin']);
    Route::get('/logout', [UserAuthController::class, 'logout'])->name('user.logout');
    Route::get('/forgot-password', [AuthController::class, 'forgotPassword'])->name('user.forgotPassword');
    Route::post('/forgot-password', [AuthController::class, 'sendResetLinkEmail'])->name('user.postForgotPassword');

    Route::get('/', [UserDashboardController::class, 'indexLogin'])->name('user.index_login');

    Route::middleware('CheckLoginUser')->group(function () {

        Route::get('/index', [UserDashboardController::class, 'index'])->name('user.index');
        
        Route::prefix('lectures')->group(function () {
            Route::get('/my-list', [UserLectureManagerController::class, 'myList'])->name('user.lectures.myList');
            Route::get('/search', [UserLectureManagerController::class, 'search'])->name('user.lectures.search');
            Route::get('/my-list/delete/{id}', [UserLectureManagerController::class, 'deleteMyList'])->name('user.lectures.deleteMyList');
            Route::get('/history', [UserLectureManagerController::class, 'lectureHistory'])->name('user.lectures.lectureHistory');
            Route::get('/detail/{id}', [UserLectureManagerController::class, 'detail'])->name('user.lectures.detail');
            Route::post('/add-memo', [UserLectureManagerController::class, 'addMemo'])->name('user.lectures.addMemo');
            Route::post('/add-remove-my-list', [UserLectureManagerController::class, 'addOrRemoveToMyList'])->name('user.lectures.addOrRemoveToMyList');
            Route::post('/like-lecture', [UserLectureManagerController::class, 'likeLecture'])->name('user.lectures.likeLecture');
            Route::get('/note', [UserLectureManagerController::class, 'lectureNote'])->name('user.lectures.lectureNote');
            Route::get('/note/delete/{id}', [UserLectureManagerController::class, 'deleteMemo'])->name('user.lectures.deleteMemo');
            Route::get('/document/{id}', [UserLectureManagerController::class, 'lessonDocument'])->name('user.lectures.lessonDocument');
            Route::get('document/download/{id}/{file_name}', [UserLectureManagerController::class, 'downloadDocument'])->name('user.lectures.downloadDocument');
            Route::get('/download-all-documents/{id}', [UserLectureManagerController::class, 'downloadAllDocuments'])->name('user.lectures.downloadAllDocuments');

            Route::post('/process-save-answer', [UserLectureManagerController::class, 'saveAnswer'])->name('user.lectures.saveAnswer');
        });

        // Router Profle
        Route::prefix('profile')->group(function () {
            Route::get('/', [ProfileController::class, 'index'])->name('user.profile.index');
            Route::get('/edit', [ProfileController::class, 'editProfile'])->name('user.profile.getEdit');
            Route::get('/edit-confirm/{key}', [ProfileController::class, 'editConfirmProfile'])->name('user.profile.editConfirm');
            Route::post('/process-save-edit', [ProfileController::class, 'saveEditProfile'])->name('user.profile.saveEditProfile');
            Route::post('/upload-avatar/{img}', [ProfileController::class, 'uploadImageProfile'])->name('user.profile.uploadAvatar');
            Route::post('/edit', [ProfileController::class, 'postEditProfile'])->name('user.profile.postEdit');
        });
    });
});

